package com.versatilemobitech.oyku.Activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;

import com.versatilemobitech.oyku.R;
import com.versatilemobitech.oyku.interfaces.EActivity;
import com.versatilemobitech.oyku.utilities.Utility;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ChangePasswordActivity extends AppCompatActivity implements EActivity {

    @BindView(R.id.imv_back_arrow)
    ImageView imv_back_arrow;


    @BindView(R.id.etCurrentPassword)
    EditText etCurrentPassword;

    @BindView(R.id.tvSave)
    TextView tvSave;

    @BindView(R.id.ivShowPassword)
    ImageView ivShowPassword;

    @BindView(R.id.ivHidePassword)
    ImageView ivHidePassword;

    @BindView(R.id.ivNewShowPassword)
    ImageView ivNewShowPassword;

    @BindView(R.id.ivNewHidePassword)
    ImageView ivNewHidePassword;

    @BindView(R.id.etOldPassword)
    EditText etOldPassword;

    @BindView(R.id.etNewPassword)
    EditText etNewPassword;

    @BindView(R.id.coordinatorLayout)
    CoordinatorLayout coordinatorLayout;

    @BindView(R.id.ivCurrentShowPwd)
    ImageView ivCurrentShowPwd;

    private boolean isCurrentPwdShown = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
        ButterKnife.bind(this);
        Toolbar toolbar = findViewById(R.id.toolbar_title);
        TextView tv_title = toolbar.findViewById(R.id.tv_title);
        tv_title.setText("Change Password");

        toolbar.setNavigationIcon(R.drawable.ic_back_arrow);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        setOnClickListeners();

    }

    private void setOnClickListeners() {

        ivCurrentShowPwd.setOnClickListener(v -> {
            if (isCurrentPwdShown){
                isCurrentPwdShown = false;
                ivCurrentShowPwd.setImageDrawable(this.getResources().getDrawable(R.drawable.ic_eye_hide));
                etCurrentPassword.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
            } else {
                isCurrentPwdShown = true;
                ivCurrentShowPwd.setImageDrawable(this.getResources().getDrawable(R.drawable.ic_eye_visible));
                etCurrentPassword.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD);
            }
        });

        ivShowPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                etOldPassword.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);

                ivHidePassword.setVisibility(View.VISIBLE);
                ivShowPassword.setVisibility(View.GONE);

                etOldPassword.setSelection(etOldPassword.length());
            }
        });

        ivHidePassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                etOldPassword.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                ivHidePassword.setVisibility(View.GONE);
                ivShowPassword.setVisibility(View.VISIBLE);
                etOldPassword.setSelection(etOldPassword.length());
            }
        });



        ivNewShowPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                etNewPassword.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);

                ivNewHidePassword.setVisibility(View.VISIBLE);
                ivNewShowPassword.setVisibility(View.GONE);

                etNewPassword.setSelection(etNewPassword.length());
            }
        });

        ivNewHidePassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                etNewPassword.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                ivNewHidePassword.setVisibility(View.GONE);
                ivNewShowPassword.setVisibility(View.VISIBLE);
                etNewPassword.setSelection(etNewPassword.length());
            }
        });


        tvSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String currentPassword = etCurrentPassword.getText().toString().trim();
                String oldPassword = etOldPassword.getText().toString().trim();
                String newPassword = etNewPassword.getText().toString().trim();

                if (currentPassword.isEmpty())
                {
                    showSnackBar("Please enter Current Password",2);
                }

                else if (oldPassword.isEmpty())
                {
                    showSnackBar("Please enter old password",2);
                }
                else  if (newPassword.isEmpty())
                {
                    showSnackBar("Please enter new password",2);
                }
                else
                {
                   showSnackBar("Password changed successfully",1);
                    Toast.makeText(ChangePasswordActivity.this, "Work in progress", Toast.LENGTH_SHORT).show();
                    setResult(2);
                    finish();
                }

            }
        });



    }

    @OnClick(R.id.imv_back_arrow)
    public void back_nav() {
        startActivity(new Intent());
    }

    @Override
    public void changeTitle(String title) {

    }

    @Override
    public void showSnackBar(String snackBarText, int type) {

        Utility.showSnackBar(this,coordinatorLayout,snackBarText,type);

    }

    @Override
    public Activity activity() {
        return this;
    }
}
