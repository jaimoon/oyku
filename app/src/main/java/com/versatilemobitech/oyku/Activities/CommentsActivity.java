package com.versatilemobitech.oyku.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import butterknife.BindView;
import butterknife.ButterKnife;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.versatilemobitech.oyku.Fragments.notification.NotificationAdpater;
import com.versatilemobitech.oyku.R;
import com.versatilemobitech.oyku.customviews.CommentsAdpater;

public class CommentsActivity extends AppCompatActivity {

    ImageView iv_toolbar_menu;
    EditText tvSubmit;
    View comment_submit;
    @BindView(R.id.imv_back_arrow)
    ImageView mimv_back_arrow;

    @BindView(R.id.recycler_comments_list)
    RecyclerView recycler_comments_list;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        setContentView(R.layout.activity_comments);



        ButterKnife.bind(this);
        Toolbar toolbar = findViewById(R.id.toolbar_title);
        TextView tv_title = toolbar.findViewById(R.id.tv_title);
        tv_title.setText("Comments");

        ImageView imgView= toolbar.findViewById(R.id.iv_toolbar_menu);
        mimv_back_arrow=toolbar.findViewById(R.id.imv_back_arrow);
        imgView.setVisibility(View.GONE);

        toolbar.setNavigationIcon(R.drawable.back_arrow);
        mimv_back_arrow.setVisibility(View.GONE);
        toolbar.setNavigationIcon(R.drawable.ic_back_arrow);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });


        iv_toolbar_menu = findViewById(R.id.iv_toolbar_menu);
        iv_toolbar_menu.setVisibility(View.GONE);
        comment_submit = findViewById(R.id.comment_submit);
        tvSubmit = findViewById(R.id.tvSubmit);

        comment_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();
               /* Intent intent = new Intent(CommentsActivity.this, DashboardActivity.class);
                startActivity(intent);*/
            }
        });


        showCommants();

    }

    private void showCommants() {
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false);
        recycler_comments_list.setLayoutManager(layoutManager);
        recycler_comments_list.setAdapter(new CommentsAdpater(this));
    }
}
