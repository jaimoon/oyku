package com.versatilemobitech.oyku.Activities;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.app.ActivityCompat;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import android.Manifest;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.versatilemobitech.oyku.R;
import com.versatilemobitech.oyku.interfaces.EActivity;
import com.versatilemobitech.oyku.utilities.Constants;
import com.versatilemobitech.oyku.utilities.Utility;

import java.util.Calendar;

public class CreateEventActivity extends AppCompatActivity implements EActivity {


    public static final int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 123;
    private static final int MY_CAMERA_PERMISSION_CODE = 100;
    private static final String IMAGE_DIRECTORY = "/demonuts";
    private int GALLERY = 1, CAMERA = 2;
    public static final int PICK_IMAGE_REQUEST = 1;
    String userChoosenTask;
    Uri imageUri;
    //    @BindView(R.id.imv_backarrow_ce)
//    ImageView mimv_backarrow_ce;
    @BindView(R.id.imv_back_arrow)
    ImageView mimv_back_arrow;

    @BindView(R.id.edt_start_time)
    EditText medt_start_time;

    @BindView(R.id.edt_end_time)
    EditText medt_end_time;

    @BindView(R.id.edt_start_date)
    EditText medt_start_date;

    @BindView(R.id.edt_end_date)
    EditText medt_end_date;

    @BindView(R.id.tvSubmit)
    TextView tvSubmit;

    @BindView(R.id.tv_uploadfile)
    TextView tv_uploadfile;

    @BindView(R.id.coordinatorLayout)
    CoordinatorLayout coordinatorLayout;

    @BindView(R.id.etEventName)
    EditText etEventName;

    @BindView(R.id.edt_event_descptn)
    EditText edt_event_descptn;

    @BindView(R.id.iv_toolbar_menu)
    ImageView iv_toolbar_menu;

    boolean isNewEvent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_event);

        isNewEvent = getIntent().getBooleanExtra(Constants.IS_NEW, false);

        ButterKnife.bind(this);
        Toolbar toolbar = findViewById(R.id.toolbar_title);
        TextView tv_title = toolbar.findViewById(R.id.tv_title);
        if (isNewEvent) {
            tv_title.setText("Create Event");
        } else {
            tv_title.setText("Edit an Event");
        }
        iv_toolbar_menu = findViewById(R.id.iv_toolbar_menu);
        iv_toolbar_menu.setVisibility(View.INVISIBLE);
        mimv_back_arrow.setVisibility(View.GONE);
        toolbar.setNavigationIcon(R.drawable.ic_back_arrow);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        setOnClickLiseners();

        tv_uploadfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectImage();
            }
        });

    }

    private void selectImage() {

        try {
            if (ActivityCompat.checkSelfPermission(this,
                    Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,
                                Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        MY_CAMERA_PERMISSION_CODE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        final CharSequence[] options = {"Take Photo", "Choose from Gallery", "Cancel"};

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Choose Picture");
        builder.setCancelable(false);
        builder.setItems(options, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int item) {
                switch (item) {
                    case 0:
                        takePhotoFromCamera();
                        break;
                    case 1:
                        choosePhotoFromGallary();
                        break;
                    case 2:
                        builder.setCancelable(true);
                        dialog.dismiss();
//                        startActivity(new Intent(CreateEventActivity.this,CreateEventActivity.class));
                        break;
                }
            }
        });
        builder.show();
    }

    private void takePhotoFromCamera() {
        Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, CAMERA);

    }

    private void choosePhotoFromGallary() {
        Intent galleryIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(galleryIntent, GALLERY);

        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("image/*");
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                           int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (userChoosenTask.equals("Take Photo"))
                        takePhotoFromCamera();

                    else if (userChoosenTask.equals("Choose from Library"))
                        choosePhotoFromGallary();
                } else {
                    //code for deny
                }
                break;
        }
    }


    private void setOnClickLiseners() {

        tvSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String eventName = etEventName.getText().toString().trim();
                String eventDescription = edt_event_descptn.getText().toString().trim();

                if (eventName.isEmpty()) {
                    showSnackBar("Please Enter event name", 2);
                } else if (eventDescription.isEmpty()) {
                    showSnackBar("Please Enter event description", 2);
                } else {
                    Toast.makeText(CreateEventActivity.this, "Work in progress", Toast.LENGTH_SHORT).show();
                    showSnackBar("Event Created", 1);


                    //finish();
                    Intent intent = new Intent(CreateEventActivity.this, PostEventActivity.class);

                    startActivityForResult(intent,234);
                }

            }
        });


    }

//    @OnClick(R.id.imv_backarrow_ce)
//    public void back_nav() {
//        finish();
////        startActivity(new Intent());
//    }

    @OnClick(R.id.edt_start_date)
    public void selectstartdate() {

        displaydatepicker(medt_start_date);
    }

    @OnClick(R.id.edt_end_date)
    public void selectenddate() {

        displaydatepicker(medt_end_date);
    }

    @OnClick(R.id.edt_start_time)
    public void selectstarttime() {



        displaytimepicker(medt_start_time);
    }


    @OnClick(R.id.edt_end_time)
    public void selectendtime() {

        displaytimepicker(medt_end_time);
    }

    @OnClick(R.id.tvSubmit)
    public void select_btn() {
        startActivity(new Intent(CreateEventActivity.this, PostEventActivity.class));
        finish();
    }

    private void displaytimepicker(EditText editText) {

        Calendar mcurrentTime = Calendar.getInstance();
        int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
        int minute = mcurrentTime.get(Calendar.MINUTE);
        TimePickerDialog mTimePicker;
        mTimePicker = new TimePickerDialog(CreateEventActivity.this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
               // editText.setText(selectedHour + ":" + selectedMinute);
                editText.setText(pad(selectedHour) + ":" + pad(selectedMinute));
            }
        }, hour, minute, true);//Yes 24 hour time
        mTimePicker.setTitle("Select Time");
        mTimePicker.show();
    }

    public String pad(int input)
    {

        String str = "";

        if (input > 10) {

            str = Integer.toString(input);
        } else {
            str = "0" + Integer.toString(input);

        }
        return str;
    }

    private void displaydatepicker(EditText editText) {

        Utility.hideKeyboardFrom(this,editText);
        Calendar calendar = Calendar.getInstance();
        int day = 0, month = 0, year = 0;

        day = calendar.get(Calendar.DAY_OF_MONTH);
        month = (calendar.get(Calendar.MONTH)) /*+ 1*/;
        year = calendar.get(Calendar.YEAR);


        DatePickerDialog datePickerDialog = new DatePickerDialog(CreateEventActivity.this,
                R.style.DatePickerDialogTheme, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {

                StringBuilder stringBuilder = new StringBuilder();
                int month = i1 + 1;
                int day = i2;
                String monthstr = "";
                String daystr = "";

                if (month < 10) {
                    monthstr = "0" + month;
                } else {

                    monthstr = String.valueOf(month);
                }

                if (day < 10) {

                    daystr = "0" + day;

                } else {

                    daystr = String.valueOf(day);
                }

                stringBuilder.append(daystr).append("/").append(monthstr).append("/").append(i);
                String str_dob = stringBuilder.toString();
                editText.setText(str_dob);
            }
        }, year, month, day);

        // datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
        datePickerDialog.show();

    }

    @Override
    public void changeTitle(String title) {

    }

    @Override
    public void showSnackBar(String snackBarText, int type) {

        Utility.showSnackBar(this, coordinatorLayout, snackBarText, type);
    }

    @Override
    public Activity activity() {
        return this;
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode ==234) {
            if (resultCode != RESULT_CANCELED) {
                finish();
            }
        }
    }
}
