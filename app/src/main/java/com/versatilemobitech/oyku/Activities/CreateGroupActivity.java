package com.versatilemobitech.oyku.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.versatilemobitech.oyku.Adapters.CreateGroup_Adapter;
import com.versatilemobitech.oyku.Model.CreateGroup_Model;
import com.versatilemobitech.oyku.R;

public class CreateGroupActivity extends AppCompatActivity {

    private CreateGroup_Model createEvent_model;

    @BindView(R.id.btn_create_group)
    Button mbtn_create_group;

    @BindView(R.id.imv_back_arrow)
    ImageView mimv_back_arrow;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_group);

        ButterKnife.bind(this);

        Toolbar toolbar = findViewById(R.id.toolbar_title);
        TextView tv_title = toolbar.findViewById(R.id.tv_title);
        tv_title.setText("Create Group");

        RecyclerView recyclerView = findViewById(R.id.recycler_view_crte_grp);
        CreateGroup_Adapter createGroup_adapter = new CreateGroup_Adapter(this, createEvent_model);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(createGroup_adapter);

    }

    @OnClick(R.id.imv_back_arrow)
    public void back_nav() {
        startActivity(new Intent(CreateGroupActivity.this, Students_AddgroupActivity.class));
    }

    @OnClick(R.id.btn_create_group)
    public void btn_create_group() {

        Dialog dialog_fp = new Dialog(this, R.style.customdialog_theme);
        dialog_fp.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog_fp.setContentView(R.layout.create_group_dialog);
        dialog_fp.getWindow().getAttributes().windowAnimations = R.style.customdialog_theme;
        WindowManager.LayoutParams lp_fp = new WindowManager.LayoutParams();
        Window window_fp = dialog_fp.getWindow();
        lp_fp.copyFrom(window_fp.getAttributes());
        lp_fp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp_fp.height = WindowManager.LayoutParams.MATCH_PARENT;
        dialog_fp.setCancelable(false);
        dialog_fp.setCanceledOnTouchOutside(false);
        dialog_fp.getWindow().setLayout(lp_fp.width, lp_fp.height);
        dialog_fp.show();

        Button dialogButton = dialog_fp.findViewById(R.id.btn_submit_groupname);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog_fp.dismiss();
                finish();
                //Toast.makeText(getApplicationContext(), "Submitted", Toast.LENGTH_SHORT).show();
            }
        });

        EditText edt_email = dialog_fp.findViewById(R.id.edt_create_group);
        edt_email.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

    }


}
