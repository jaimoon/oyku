package com.versatilemobitech.oyku.Activities;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.google.android.material.bottomnavigation.BottomNavigationItemView;
import com.google.android.material.bottomnavigation.BottomNavigationMenuView;
import com.google.android.material.bottomnavigation.BottomNavigationPresenter;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationView;
import com.versatilemobitech.oyku.Fragments.Events_Fragment;
import com.versatilemobitech.oyku.Fragments.Home_Fragment;
import com.versatilemobitech.oyku.Fragments.Profile_Fragment;
import com.versatilemobitech.oyku.Fragments.notification.Notification_Fragment;
import com.versatilemobitech.oyku.R;
import com.versatilemobitech.oyku.utilities.Utility;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class DashboardActivity extends AppCompatActivity {

    private DrawerLayout drawerLayout;
    private ActionBarDrawerToggle drawerToggle;
    private BottomNavigationView bottomNavigationView;
    private Toolbar toolbar;
    private Menu menu;
    private boolean mIsHomeFragment = false;

    @BindView(R.id.view_nav_home_1)
    View view_nav_home_1;

    @BindView(R.id.view_nav_ntfcn_1)
    View view_nav_ntfcn_1;

    @BindView(R.id.view_nav_events_1)
    View view_nav_events_1;

    @BindView(R.id.view_nav_classes_1)
    View view_nav_classes_1;

    @BindView(R.id.view_nav_invt_prts_1)
    View view_nav_invt_prts_1;

    @BindView(R.id.view_nav_profile_1)
    View view_nav_profile_1;

    @BindView(R.id.view_nav_hlp_sup_1)
    View view_nav_hlp_sup_1;

    @BindView(R.id.view_nav_prv_plcy_1)
    View view_nav_prv_plcy_1;

    @BindView(R.id.view_nav_terms_cnds_1)
    View view_nav_terms_cnds_1;

    @BindView(R.id.view_nav_logout_1)
    View view_nav_logout_1;

    @BindView(R.id.menu_nav_home)
    TextView mmenu_nav_home;

    @BindView(R.id.menu_nav_notification)
    TextView mmenu_nav_notification;

    @BindView(R.id.menu_nav_events)
    TextView mmenu_nav_events;

    @BindView(R.id.menu_nav_classes)
    TextView mmenu_nav_classes;

    @BindView(R.id.menu_nav_invite_prts)
    TextView mmenu_nav_invite_prts;

    @BindView(R.id.menu_nav_profile)
    TextView mmenu_nav_profile;

    @BindView(R.id.menu_nav_help_sup)
    TextView mmenu_nav_help_sup;

    @BindView(R.id.menu_nav_privacy_policy)
    TextView mmenu_nav_privacy_policy;

    @BindView(R.id.menu_nav_terms_conds)
    TextView mmenu_nav_terms_conds;

    @BindView(R.id.menu_nav_logout)
    TextView mmenu_nav_logout;


    ImageView imageView;
    String menuInfo;

    ColorStateList csl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        setContentView(R.layout.activity_dashboard);

        toolbar = findViewById(R.id.toolbar_title);
        setSupportActionBar(toolbar);

        TextView tv_title = toolbar.findViewById(R.id.tv_title);
        tv_title.setVisibility(View.VISIBLE);
        tv_title.setText("Öykü");
        SearchView searchView=toolbar.findViewById(R.id.searchView);
        searchView.setQueryHint("Search");
        searchView.setMaxWidth(Integer.MAX_VALUE);
        EditText searchEditText = (EditText) searchView.findViewById(androidx.appcompat.R.id.search_src_text);
        searchEditText.setTextColor(getResources().getColor(R.color.black));
        searchEditText.setHintTextColor(getResources().getColor(R.color.color_dark_grey));

        Utility.setCloseSearchIcon(searchView);



        drawerLayout = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        bottomNavigationView = findViewById(R.id.bottom_navigation);

        drawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawerLayout.addDrawerListener(drawerToggle);
        drawerToggle.syncState();
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Drawable leftDrawable = AppCompatResources.getDrawable(this, R.drawable.ic_menu);
        getSupportActionBar().setHomeAsUpIndicator(leftDrawable);
        removePaddingFromNavigationItem();
        Bottomnavigationview();
        ButterKnife.bind(this);
        loadFragment(new Home_Fragment(), true);
//        getBottomIconsChange(menuInfo);
        toolbar.setVisibility(View.VISIBLE);

        int colorInt = getResources().getColor(R.color.colorPrimaryDark);
        csl = ColorStateList.valueOf(colorInt);
        //Utility.hideKeyboardFrom(this,edt_search);
    }




    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.menu = menu;
        return super.onCreateOptionsMenu(menu);
    }

    public void removePaddingFromNavigationItem() {

        try {
            BottomNavigationMenuView menuView = (BottomNavigationMenuView) bottomNavigationView.getChildAt(0);

            for (int i = 0; i < menuView.getChildCount(); i++) {
                BottomNavigationItemView item = (BottomNavigationItemView) menuView.getChildAt(i);
                View activeLabel = item.findViewById(R.id.largeLabel);
                if (activeLabel instanceof TextView) {
                    activeLabel.setPadding(0, 0, 0, 0);
                }
            }
        }catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        Log.i("TEST","MEnu");
        return super.onOptionsItemSelected(item);

    }

    private void Bottomnavigationview() {
        bottomNavigationView.setOnNavigationItemSelectedListener(item -> {





            switch (item.getItemId()) {
                case R.id.btmnav_home:

                    loadFragment(new Home_Fragment(), true);

                    toolbar.setVisibility(View.VISIBLE);
                    //for color change in side menu
                    home_navColorchange();
                    //Utility.hideKeyboardFrom(this,edt_search);
                    break;

                case R.id.btmnav_notification:

                    loadFragment(new Notification_Fragment(), false);
                    toolbar.setVisibility(View.GONE);
                    //Utility.hideKeyboardFrom(this,edt_search);
                    break;

                case R.id.btmnav_post:
                    mIsHomeFragment = false;
                    startActivity(new Intent(DashboardActivity.this, NewPostActivity.class));
                    //finish();
                    break;

                case R.id.btmnav_events:

                    loadFragment(new Events_Fragment(), false);
                    toolbar.setVisibility(View.GONE);
                    break;

                case R.id.btmnav_profile:

                    loadFragment(new Profile_Fragment(), false);
                    toolbar.setVisibility(View.GONE);
                    break;

                default:
                    break;
            }
           // Utility.hideKeyboardFrom(this,edt_search);

            return true;
        });
    }

    private void home_navColorchange() {

        mmenu_nav_home.setTextColor(Color.parseColor("#4E096C"));
        view_nav_home_1.setVisibility(View.VISIBLE);

        mmenu_nav_notification.setTextColor(Color.parseColor("#707070"));
        view_nav_ntfcn_1.setVisibility(View.GONE);

        mmenu_nav_events.setTextColor(Color.parseColor("#707070"));
        view_nav_events_1.setVisibility(View.GONE);

        mmenu_nav_classes.setTextColor(Color.parseColor("#707070"));
        view_nav_classes_1.setVisibility(View.GONE);

        mmenu_nav_invite_prts.setTextColor(Color.parseColor("#707070"));
        view_nav_invt_prts_1.setVisibility(View.GONE);

        mmenu_nav_profile.setTextColor(Color.parseColor("#707070"));
        view_nav_profile_1.setVisibility(View.GONE);

        mmenu_nav_help_sup.setTextColor(Color.parseColor("#707070"));
        view_nav_hlp_sup_1.setVisibility(View.GONE);

        mmenu_nav_terms_conds.setTextColor(Color.parseColor("#707070"));
        view_nav_terms_cnds_1.setVisibility(View.GONE);

        mmenu_nav_privacy_policy.setTextColor(Color.parseColor("#707070"));
        view_nav_prv_plcy_1.setVisibility(View.GONE);

        mmenu_nav_logout.setTextColor(Color.parseColor("#707070"));
        view_nav_logout_1.setVisibility(View.GONE);

        mmenu_nav_home.setText(R.string.menu_nav_home_active);

    }

//    private void onBackPass() {
//
//        if (bottomNavigationView.getSelectedItemId() == R.id.btmnav_home)
//        {
//            super.onBackPressed();
//            finish();
//        }
//        else
//        {
//            bottomNavigationView.setSelectedItemId(R.id.btmnav_home);
//        }
//    }

//    public static void setHomeItem(DashboardActivity activity) {
//        BottomNavigationView bottomNavigationView = (BottomNavigationView)
//                activity.findViewById(R.id.bottom_navigation);
//        bottomNavigationView.setSelectedItemId(R.id.btmnav_home);
//    }

    @OnClick(R.id.menu_nav_home)
    public void home_nav() {

        mmenu_nav_home.setTextColor(Color.parseColor("#4E096C"));
        view_nav_home_1.setVisibility(View.VISIBLE);

        mmenu_nav_notification.setTextColor(Color.parseColor("#707070"));
        view_nav_ntfcn_1.setVisibility(View.GONE);

        mmenu_nav_events.setTextColor(Color.parseColor("#707070"));
        view_nav_events_1.setVisibility(View.GONE);

        mmenu_nav_classes.setTextColor(Color.parseColor("#707070"));
        view_nav_classes_1.setVisibility(View.GONE);

        mmenu_nav_invite_prts.setTextColor(Color.parseColor("#707070"));
        view_nav_invt_prts_1.setVisibility(View.GONE);

        mmenu_nav_profile.setTextColor(Color.parseColor("#707070"));
        view_nav_profile_1.setVisibility(View.GONE);

        mmenu_nav_help_sup.setTextColor(Color.parseColor("#707070"));
        view_nav_hlp_sup_1.setVisibility(View.GONE);

        mmenu_nav_terms_conds.setTextColor(Color.parseColor("#707070"));
        view_nav_terms_cnds_1.setVisibility(View.GONE);

        mmenu_nav_privacy_policy.setTextColor(Color.parseColor("#707070"));
        view_nav_prv_plcy_1.setVisibility(View.GONE);

        mmenu_nav_logout.setTextColor(Color.parseColor("#707070"));
        view_nav_logout_1.setVisibility(View.GONE);

        mmenu_nav_home.setText(R.string.menu_nav_home_active);
        toolbar.setVisibility(View.VISIBLE);
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        }
        toolbar.setVisibility(View.VISIBLE);
        Intent intent=new Intent(DashboardActivity.this, DashboardActivity.class);
        //intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
//        loadFragment(new Home_Fragment());
    }

    @OnClick(R.id.menu_nav_notification)
    public void notification_nav() {


        mmenu_nav_home.setTextColor(Color.parseColor("#707070"));
        view_nav_home_1.setVisibility(View.GONE);

        mmenu_nav_notification.setTextColor(Color.parseColor("#4E096C"));
        view_nav_ntfcn_1.setVisibility(View.VISIBLE);

        mmenu_nav_events.setTextColor(Color.parseColor("#707070"));
        view_nav_events_1.setVisibility(View.GONE);

        mmenu_nav_classes.setTextColor(Color.parseColor("#707070"));
        view_nav_classes_1.setVisibility(View.GONE);

        mmenu_nav_invite_prts.setTextColor(Color.parseColor("#707070"));
        view_nav_invt_prts_1.setVisibility(View.GONE);

        mmenu_nav_profile.setTextColor(Color.parseColor("#707070"));
        view_nav_profile_1.setVisibility(View.GONE);

        mmenu_nav_help_sup.setTextColor(Color.parseColor("#707070"));
        view_nav_hlp_sup_1.setVisibility(View.GONE);

        mmenu_nav_terms_conds.setTextColor(Color.parseColor("#707070"));
        view_nav_terms_cnds_1.setVisibility(View.GONE);

        mmenu_nav_privacy_policy.setTextColor(Color.parseColor("#707070"));
        view_nav_prv_plcy_1.setVisibility(View.GONE);

        mmenu_nav_logout.setTextColor(Color.parseColor("#707070"));
        view_nav_logout_1.setVisibility(View.GONE);

        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
        toolbar.setVisibility(View.GONE);
        //loadFragment(new Notification_Fragment(), false);
        getBottomIconsChange("notification");
        bottomNavigationView.setSelectedItemId(R.id.btmnav_notification);
    }

    @OnClick(R.id.menu_nav_events)
    public void events_nav() {

        mmenu_nav_home.setTextColor(Color.parseColor("#707070"));
        view_nav_home_1.setVisibility(View.GONE);

        mmenu_nav_notification.setTextColor(Color.parseColor("#707070"));
        view_nav_ntfcn_1.setVisibility(View.GONE);

        mmenu_nav_events.setTextColor(Color.parseColor("#4E096C"));
        view_nav_events_1.setVisibility(View.VISIBLE);

        mmenu_nav_classes.setTextColor(Color.parseColor("#707070"));
        view_nav_classes_1.setVisibility(View.GONE);

        mmenu_nav_invite_prts.setTextColor(Color.parseColor("#707070"));
        view_nav_invt_prts_1.setVisibility(View.GONE);

        mmenu_nav_profile.setTextColor(Color.parseColor("#707070"));
        view_nav_profile_1.setVisibility(View.GONE);

        mmenu_nav_help_sup.setTextColor(Color.parseColor("#707070"));
        view_nav_hlp_sup_1.setVisibility(View.GONE);

        mmenu_nav_terms_conds.setTextColor(Color.parseColor("#707070"));
        view_nav_terms_cnds_1.setVisibility(View.GONE);

        mmenu_nav_privacy_policy.setTextColor(Color.parseColor("#707070"));
        view_nav_prv_plcy_1.setVisibility(View.GONE);

        mmenu_nav_logout.setTextColor(Color.parseColor("#707070"));
        view_nav_logout_1.setVisibility(View.GONE);

        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
        toolbar.setVisibility(View.GONE);

        //loadFragment(new Events_Fragment(), false);
        getBottomIconsChange("event");
        bottomNavigationView.setSelectedItemId(R.id.btmnav_events);
    }

    @OnClick(R.id.menu_nav_classes)
    public void classes_nav() {

        mmenu_nav_home.setTextColor(Color.parseColor("#707070"));
        view_nav_home_1.setVisibility(View.GONE);

        mmenu_nav_notification.setTextColor(Color.parseColor("#707070"));
        view_nav_ntfcn_1.setVisibility(View.GONE);

        mmenu_nav_events.setTextColor(Color.parseColor("#707070"));
        view_nav_events_1.setVisibility(View.GONE);

        mmenu_nav_classes.setTextColor(Color.parseColor("#4E096C"));
        view_nav_classes_1.setVisibility(View.VISIBLE);

        mmenu_nav_invite_prts.setTextColor(Color.parseColor("#707070"));
        view_nav_invt_prts_1.setVisibility(View.GONE);

        mmenu_nav_profile.setTextColor(Color.parseColor("#707070"));
        view_nav_profile_1.setVisibility(View.GONE);

        mmenu_nav_help_sup.setTextColor(Color.parseColor("#707070"));
        view_nav_hlp_sup_1.setVisibility(View.GONE);

        mmenu_nav_terms_conds.setTextColor(Color.parseColor("#707070"));
        view_nav_terms_cnds_1.setVisibility(View.GONE);

        mmenu_nav_privacy_policy.setTextColor(Color.parseColor("#707070"));
        view_nav_prv_plcy_1.setVisibility(View.GONE);

        mmenu_nav_logout.setTextColor(Color.parseColor("#707070"));
        view_nav_logout_1.setVisibility(View.GONE);

        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
        toolbar.setVisibility(View.VISIBLE);
//        startActivity(new Intent(DashboardActivity.this, ViewClassActivity.class));
        Intent intent = new Intent(DashboardActivity.this, ViewClassActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    @OnClick(R.id.menu_nav_invite_prts)
    public void invite_parents() {

        mmenu_nav_home.setTextColor(Color.parseColor("#707070"));
        view_nav_home_1.setVisibility(View.GONE);

        mmenu_nav_notification.setTextColor(Color.parseColor("#707070"));
        view_nav_ntfcn_1.setVisibility(View.GONE);

        mmenu_nav_events.setTextColor(Color.parseColor("#707070"));
        view_nav_events_1.setVisibility(View.GONE);

        mmenu_nav_classes.setTextColor(Color.parseColor("#707070"));
        view_nav_classes_1.setVisibility(View.GONE);

        mmenu_nav_invite_prts.setTextColor(Color.parseColor("#4E096C"));
        view_nav_invt_prts_1.setVisibility(View.VISIBLE);

        mmenu_nav_profile.setTextColor(Color.parseColor("#707070"));
        view_nav_profile_1.setVisibility(View.GONE);

        mmenu_nav_help_sup.setTextColor(Color.parseColor("#707070"));
        view_nav_hlp_sup_1.setVisibility(View.GONE);

        mmenu_nav_terms_conds.setTextColor(Color.parseColor("#707070"));
        view_nav_terms_cnds_1.setVisibility(View.GONE);

        mmenu_nav_privacy_policy.setTextColor(Color.parseColor("#707070"));
        view_nav_prv_plcy_1.setVisibility(View.GONE);

        mmenu_nav_logout.setTextColor(Color.parseColor("#707070"));
        view_nav_logout_1.setVisibility(View.GONE);

        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
        toolbar.setVisibility(View.VISIBLE);
        Intent intent = new Intent(DashboardActivity.this, InvitingStudentsActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    @OnClick({R.id.menu_nav_profile})
    public void profile() {

        mmenu_nav_home.setTextColor(Color.parseColor("#707070"));
        view_nav_home_1.setVisibility(View.GONE);

        mmenu_nav_notification.setTextColor(Color.parseColor("#707070"));
        view_nav_ntfcn_1.setVisibility(View.GONE);

        mmenu_nav_events.setTextColor(Color.parseColor("#707070"));
        view_nav_events_1.setVisibility(View.GONE);

        mmenu_nav_classes.setTextColor(Color.parseColor("#707070"));
        view_nav_classes_1.setVisibility(View.GONE);

        mmenu_nav_invite_prts.setTextColor(Color.parseColor("#707070"));
        view_nav_invt_prts_1.setVisibility(View.GONE);

        mmenu_nav_profile.setTextColor(Color.parseColor("#4E096C"));
        view_nav_profile_1.setVisibility(View.VISIBLE);

        mmenu_nav_help_sup.setTextColor(Color.parseColor("#707070"));
        view_nav_hlp_sup_1.setVisibility(View.GONE);

        mmenu_nav_terms_conds.setTextColor(Color.parseColor("#707070"));
        view_nav_terms_cnds_1.setVisibility(View.GONE);

        mmenu_nav_privacy_policy.setTextColor(Color.parseColor("#707070"));
        view_nav_prv_plcy_1.setVisibility(View.GONE);

        mmenu_nav_logout.setTextColor(Color.parseColor("#707070"));
        view_nav_logout_1.setVisibility(View.GONE);

        toolbar.setVisibility(View.GONE);
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
        toolbar.setVisibility(View.GONE);
        //loadFragment(new Profile_Fragment(), false);
        getBottomIconsChange("profile");
        bottomNavigationView.setSelectedItemId(R.id.btmnav_profile);
    }


    @OnClick(R.id.menu_nav_help_sup)
    public void help_sup() {
        mmenu_nav_home.setTextColor(Color.parseColor("#707070"));
        view_nav_home_1.setVisibility(View.GONE);

        mmenu_nav_notification.setTextColor(Color.parseColor("#707070"));
        view_nav_ntfcn_1.setVisibility(View.GONE);

        mmenu_nav_events.setTextColor(Color.parseColor("#707070"));
        view_nav_events_1.setVisibility(View.GONE);

        mmenu_nav_classes.setTextColor(Color.parseColor("#707070"));
        view_nav_classes_1.setVisibility(View.GONE);

        mmenu_nav_invite_prts.setTextColor(Color.parseColor("#707070"));
        view_nav_invt_prts_1.setVisibility(View.GONE);

        mmenu_nav_profile.setTextColor(Color.parseColor("#707070"));
        view_nav_profile_1.setVisibility(View.GONE);

        mmenu_nav_help_sup.setTextColor(Color.parseColor("#4E096C"));
        view_nav_hlp_sup_1.setVisibility(View.VISIBLE);

        mmenu_nav_terms_conds.setTextColor(Color.parseColor("#707070"));
        view_nav_terms_cnds_1.setVisibility(View.GONE);

        mmenu_nav_privacy_policy.setTextColor(Color.parseColor("#707070"));
        view_nav_prv_plcy_1.setVisibility(View.GONE);

        mmenu_nav_logout.setTextColor(Color.parseColor("#707070"));
        view_nav_logout_1.setVisibility(View.GONE);

        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
        toolbar.setVisibility(View.VISIBLE);
        Intent intent = new Intent(DashboardActivity.this, Help_SupportActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    @OnClick({R.id.menu_nav_terms_conds})
    public void terms_conditions_nav() {

        mmenu_nav_home.setTextColor(Color.parseColor("#707070"));
        view_nav_home_1.setVisibility(View.GONE);

        mmenu_nav_notification.setTextColor(Color.parseColor("#707070"));
        view_nav_ntfcn_1.setVisibility(View.GONE);

        mmenu_nav_events.setTextColor(Color.parseColor("#707070"));
        view_nav_events_1.setVisibility(View.GONE);

        mmenu_nav_classes.setTextColor(Color.parseColor("#707070"));
        view_nav_classes_1.setVisibility(View.GONE);

        mmenu_nav_invite_prts.setTextColor(Color.parseColor("#707070"));
        view_nav_invt_prts_1.setVisibility(View.GONE);

        mmenu_nav_profile.setTextColor(Color.parseColor("#707070"));
        view_nav_profile_1.setVisibility(View.GONE);

        mmenu_nav_help_sup.setTextColor(Color.parseColor("#707070"));
        view_nav_hlp_sup_1.setVisibility(View.GONE);

        mmenu_nav_terms_conds.setTextColor(Color.parseColor("#4E096C"));
        view_nav_terms_cnds_1.setVisibility(View.VISIBLE);

        mmenu_nav_privacy_policy.setTextColor(Color.parseColor("#707070"));
        view_nav_prv_plcy_1.setVisibility(View.GONE);

        mmenu_nav_logout.setTextColor(Color.parseColor("#707070"));
        view_nav_logout_1.setVisibility(View.GONE);

        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
        toolbar.setVisibility(View.VISIBLE);
        Intent intent = new Intent(DashboardActivity.this, TermsConditionsActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    @OnClick({R.id.menu_nav_privacy_policy})
    public void privacy_policy_nav() {

        mmenu_nav_home.setTextColor(Color.parseColor("#707070"));
        view_nav_home_1.setVisibility(View.GONE);

        mmenu_nav_notification.setTextColor(Color.parseColor("#707070"));
        view_nav_ntfcn_1.setVisibility(View.GONE);

        mmenu_nav_events.setTextColor(Color.parseColor("#707070"));
        view_nav_events_1.setVisibility(View.GONE);

        mmenu_nav_classes.setTextColor(Color.parseColor("#707070"));
        view_nav_classes_1.setVisibility(View.GONE);

        mmenu_nav_invite_prts.setTextColor(Color.parseColor("#707070"));
        view_nav_invt_prts_1.setVisibility(View.GONE);

        mmenu_nav_profile.setTextColor(Color.parseColor("#707070"));
        view_nav_profile_1.setVisibility(View.GONE);

        mmenu_nav_help_sup.setTextColor(Color.parseColor("#707070"));
        view_nav_hlp_sup_1.setVisibility(View.GONE);

        mmenu_nav_terms_conds.setTextColor(Color.parseColor("#707070"));
        view_nav_terms_cnds_1.setVisibility(View.GONE);

        mmenu_nav_privacy_policy.setTextColor(Color.parseColor("#4E096C"));
        view_nav_prv_plcy_1.setVisibility(View.VISIBLE);

        mmenu_nav_logout.setTextColor(Color.parseColor("#707070"));
        view_nav_logout_1.setVisibility(View.GONE);

        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
        toolbar.setVisibility(View.VISIBLE);
        Intent intent = new Intent(DashboardActivity.this, PrivacyPolicyActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    @OnClick({R.id.menu_nav_logout})
    public void logout_nav() {

        Dialog dialog_fp = new Dialog(this, R.style.customdialog_theme);
        dialog_fp.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog_fp.setContentView(R.layout.alert_dialogue);
        ((TextView)dialog_fp.findViewById(R.id.tv_alertmessage)).setText(R.string.logoff);
        dialog_fp.getWindow().getAttributes().windowAnimations = R.style.customdialog_theme;
        WindowManager.LayoutParams lp_fp = new WindowManager.LayoutParams();
        Window window_fp = dialog_fp.getWindow();
        lp_fp.copyFrom(window_fp.getAttributes());
        lp_fp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp_fp.height = WindowManager.LayoutParams.MATCH_PARENT;
        dialog_fp.setCancelable(false);
        dialog_fp.setCanceledOnTouchOutside(false);
        dialog_fp.getWindow().setLayout(lp_fp.width, lp_fp.height);


        Button dialogButton = dialog_fp.findViewById(R.id.btn_ok_alert);
        dialogButton.setOnClickListener(v -> {
            dialog_fp.dismiss();
            logoff();
            //finish();
        });

        Button dialogNo = dialog_fp.findViewById(R.id.btn_cancel_alert);
        dialogNo.setOnClickListener(view -> dialog_fp.dismiss());

        if (dialog_fp.isShowing()) {
            return;
        }
        dialog_fp.show();

    }

    private void logoff() {

        mmenu_nav_home.setTextColor(Color.parseColor("#707070"));
        view_nav_home_1.setVisibility(View.GONE);

        mmenu_nav_notification.setTextColor(Color.parseColor("#707070"));
        view_nav_ntfcn_1.setVisibility(View.GONE);

        mmenu_nav_events.setTextColor(Color.parseColor("#707070"));
        view_nav_events_1.setVisibility(View.GONE);

        mmenu_nav_classes.setTextColor(Color.parseColor("#707070"));
        view_nav_classes_1.setVisibility(View.GONE);

        mmenu_nav_invite_prts.setTextColor(Color.parseColor("#707070"));
        view_nav_invt_prts_1.setVisibility(View.GONE);

        mmenu_nav_profile.setTextColor(Color.parseColor("#707070"));
        view_nav_profile_1.setVisibility(View.GONE);

        mmenu_nav_help_sup.setTextColor(Color.parseColor("#707070"));
        view_nav_hlp_sup_1.setVisibility(View.GONE);

        mmenu_nav_terms_conds.setTextColor(Color.parseColor("#707070"));
        view_nav_terms_cnds_1.setVisibility(View.GONE);

        mmenu_nav_privacy_policy.setTextColor(Color.parseColor("#707070"));
        view_nav_prv_plcy_1.setVisibility(View.GONE);

        mmenu_nav_logout.setTextColor(Color.parseColor("#4E096C"));
        view_nav_logout_1.setVisibility(View.VISIBLE);

        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
        finish();
        //Landing on Login page
        Intent intent=new Intent(this,LoginActivity.class);
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
            return;
        } /*else {
            super.onBackPressed();
            toolbar.setVisibility(View.VISIBLE);
        }*/
        if (mIsHomeFragment) {
            Dialog dialog_fp = new Dialog(this, R.style.customdialog_theme);
            dialog_fp.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog_fp.setContentView(R.layout.alert_dialogue);
            dialog_fp.getWindow().getAttributes().windowAnimations = R.style.customdialog_theme;
            WindowManager.LayoutParams lp_fp = new WindowManager.LayoutParams();
            Window window_fp = dialog_fp.getWindow();
            lp_fp.copyFrom(window_fp.getAttributes());
            lp_fp.width = WindowManager.LayoutParams.MATCH_PARENT;
            lp_fp.height = WindowManager.LayoutParams.MATCH_PARENT;
            dialog_fp.setCancelable(false);
            dialog_fp.setCanceledOnTouchOutside(false);
            dialog_fp.getWindow().setLayout(lp_fp.width, lp_fp.height);


            Button dialogButton = dialog_fp.findViewById(R.id.btn_ok_alert);
            dialogButton.setOnClickListener(v -> {
                dialog_fp.dismiss();
                finish();
            });

            Button dialogNo = dialog_fp.findViewById(R.id.btn_cancel_alert);
            dialogNo.setOnClickListener(view -> dialog_fp.dismiss());

            if (dialog_fp.isShowing()) {
                return;
            }
            dialog_fp.show();
           /* if (SharedPreference.getStringPreference(this, "HOMEVALUE").equalsIgnoreCase("1")) {
                toolbar.setVisibility(View.GONE);
                if (bottomNavigationView.getSelectedItemId() != R.id.btmnav_home) {
                    super.onBackPressed();
                } else {
                    bottomNavigationView.setSelectedItemId(R.id.btmnav_home);
                }
                finish();
            }*/
        } else {
            getBottomIconsChange("home");
            bottomNavigationView.setSelectedItemId(R.id.btmnav_home);
        }
    }

    private void setHomeItem(DashboardActivity dashboardActivity) {
        BottomNavigationView bottomNavigationView = (BottomNavigationView)
                dashboardActivity.findViewById(R.id.bottom_navigation);
        bottomNavigationView.setSelectedItemId(R.id.btmnav_home);
    }

    public boolean loadFragment(Fragment fragment, boolean isHomeFragment) {
        mIsHomeFragment = isHomeFragment;
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction ft = fragmentManager.beginTransaction();
        ft.replace(R.id.frameLayout, fragment);
        ft.addToBackStack(null);
        ft.commit();
        return true;
    }

    @Override
    protected void onStart() {
        super.onStart();
       // Utility.hideKeyboard(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
       // Utility.hideKeyboard(this);

        //getBottomIconsChange("home");
       //bottomNavigationView.setSelectedItemId(R.id.btmnav_home);
    }

    @SuppressLint("ResourceType")
    public void getBottomIconsChange(String menuName) {

        Menu menu = bottomNavigationView.getMenu();
//        menu.findItem(R.id.btmnav_home).setIcon(R.drawable.ic_notification_menu);

        if (menuName.equalsIgnoreCase("home")) {
            mIsHomeFragment = true;
//            Toast.makeText(this, "h " + menuName, Toast.LENGTH_SHORT).show();

//            menu.add(Menu.NONE,menu.findItem(R.id.btmnav_home), Menu.NONE, ).setIcon(R.drawable.ic_action_one);

//            menu.findItem(R.id.btmnav_home).setIcon(R.drawable.home_active);
            Utility.tintMenuIcon(this, menu.findItem(R.id.btmnav_home), R.color.color_dark_violet);
            bottomNavigationView.setItemTextAppearanceActive(R.color.color_dark_violet);
//            menu.findItem(R.id.btmnav_notification).setIcon(R.drawable.notification);
//            menu.findItem(R.id.btmnav_events).setIcon(R.drawable.event);
//            menu.findItem(R.id.btmnav_profile).setIcon(R.drawable.profile);

//            bottomNavigationView.findViewById(R.id.btmnav_home).setIte
        } else if (menuName.equalsIgnoreCase("notification")) {
            mIsHomeFragment = false;
//            Toast.makeText(this, "n " + menuName, Toast.LENGTH_SHORT).show();
            bottomNavigationView.setItemTextAppearanceActive(R.color.color_dark_violet);
//            menu.findItem(R.id.btmnav_home).setIcon(R.drawable.home);
            Utility.tintMenuIcon(this, menu.findItem(R.id.btmnav_notification), R.color.color_dark_violet);
//            menu.findItem(R.id.btmnav_notification).setIcon(R.drawable.notification_active);
//            menu.findItem(R.id.btmnav_events).setIcon(R.drawable.event);
//            menu.findItem(R.id.btmnav_profile).setIcon(R.drawable.profile);
           // bottomNavigationView.setItemTextAppearanceActive(R.color.color_dark_violet);

        } else if (menuName.equalsIgnoreCase("event")) {
            mIsHomeFragment = false;
//            Toast.makeText(this, "e " + menuName, Toast.LENGTH_SHORT).show();
            bottomNavigationView.setItemTextAppearanceActive(R.color.color_dark_violet);
//            menu.findItem(R.id.btmnav_home).setIcon(R.drawable.home);
//            menu.findItem(R.id.btmnav_notification).setIcon(R.drawable.notification);
            Utility.tintMenuIcon(this, menu.findItem(R.id.btmnav_events), R.color.color_dark_violet);
//            menu.findItem(R.id.btmnav_events).setIcon(R.drawable.event_active);
//            menu.findItem(R.id.btmnav_profile).setIcon(R.drawable.profile);

        } else if (menuName.equalsIgnoreCase("profile")) {
            mIsHomeFragment = false;
//            Toast.makeText(this, "p " + menuName, Toast.LENGTH_SHORT).show();
            bottomNavigationView.setItemTextAppearanceActive(R.color.color_dark_violet);
//            menu.findItem(R.id.btmnav_home).setIcon(R.drawable.home);
//            menu.findItem(R.id.btmnav_notification).setIcon(R.drawable.notification);
//            menu.findItem(R.id.btmnav_events).setIcon(R.drawable.event);
            Utility.tintMenuIcon(this, menu.findItem(R.id.btmnav_profile), R.color.color_dark_violet);
//            menu.findItem(R.id.btmnav_profile).setIcon(R.drawable.profile_active);

        }

//        bottomNavigationView.setItemIconTintList(null);


    }

    @Override
    protected void onPause() {
        super.onPause();
//        Toast.makeText(this, "hi", Toast.LENGTH_SHORT).show();
//        onBackPass();

    }

    @Override
    protected void onRestart() {
        super.onRestart();

//        Toast.makeText(this, "hiiiiiiiiiiiii", Toast.LENGTH_SHORT).show();

    }

}
