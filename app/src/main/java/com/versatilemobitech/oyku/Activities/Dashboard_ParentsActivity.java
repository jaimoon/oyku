package com.versatilemobitech.oyku.Activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationView;
import com.versatilemobitech.oyku.Fragments.Events_Parents_Fragment;
import com.versatilemobitech.oyku.Fragments.Home_Fragment;
import com.versatilemobitech.oyku.Fragments.notification.Notification_Fragment;
import com.versatilemobitech.oyku.Fragments.Notification_Parents_Fragment;
import com.versatilemobitech.oyku.Fragments.Profile_Parents_Fragment;
import com.versatilemobitech.oyku.R;
import com.versatilemobitech.oyku.utilities.SharedPreference;
import com.versatilemobitech.oyku.utilities.Utility;

public class Dashboard_ParentsActivity extends AppCompatActivity {

    private DrawerLayout drawerLayout;
    private ActionBarDrawerToggle drawerToggle;
    private BottomNavigationView bottomNavigationView;
    private Toolbar toolbar;

    @BindView(R.id.view_nav_home)
    View view_nav_home;

    @BindView(R.id.view_nav_peter_mem)
    View view_nav_peter_mem;

    @BindView(R.id.view_nav_ntfcn)
    View view_nav_ntfcn;

    @BindView(R.id.view_nav_events)
    View view_nav_events;

    @BindView(R.id.view_nav_profile)
    View view_nav_profile;

    @BindView(R.id.view_nav_hlp_sup)
    View view_nav_hlp_sup;

    @BindView(R.id.view_nav_prv_plcy)
    View view_nav_prv_plcy;

    @BindView(R.id.view_nav_terms_cnds)
    View view_nav_terms_cnds;

    @BindView(R.id.view_nav_logout)
    View view_nav_logout;

    @BindView(R.id.menu_nav_home_parents)
    TextView mmenu_nav_home_parents;

    @BindView(R.id.menu_nav_peter_memories)
    TextView mmenu_nav_peter_memories;

    @BindView(R.id.menu_nav_notification_parents)
    TextView mmenu_nav_notification_parents;

    @BindView(R.id.menu_nav_events_parents)
    TextView mmenu_nav_events_parents;

    @BindView(R.id.menu_nav_help_sup_parents)
    TextView mmenu_nav_help_sup_parents;

    @BindView(R.id.menu_nav_profile_parents)
    TextView mmenu_nav_profile_parents;

    @BindView(R.id.menu_nav_privacy_policy_parents)
    TextView mmenu_nav_privacy_policy_parents;

    @BindView(R.id.menu_nav_terms_conds_parents)
    TextView mmenu_nav_terms_conds_parents;

    @BindView(R.id.menu_nav_logout_parents)
    TextView mmenu_nav_logout_parents;
    private boolean mIsHomeFragment;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard_parents);

        toolbar = findViewById(R.id.toolbar_dashboard_2);
        setSupportActionBar(toolbar);

        TextView tv_title = toolbar.findViewById(R.id.tv_title);

        tv_title.setText("Öykü");

        SearchView searchView=toolbar.findViewById(R.id.searchView);
        searchView.setQueryHint("Search");
        searchView.setMaxWidth(Integer.MAX_VALUE);
        EditText searchEditText = (EditText) searchView.findViewById(androidx.appcompat.R.id.search_src_text);
        searchEditText.setTextColor(getResources().getColor(R.color.black));
        searchEditText.setHintTextColor(getResources().getColor(R.color.color_dark_grey));
        Utility.setCloseSearchIcon(searchView);


        drawerLayout = findViewById(R.id.drawer_layout_2);
        NavigationView navigationView = findViewById(R.id.nav_view_2);
        bottomNavigationView = findViewById(R.id.bottom_navigation_2);

        drawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawerLayout.addDrawerListener(drawerToggle);
        drawerToggle.syncState();

        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Drawable leftDrawable = AppCompatResources.getDrawable(this, R.drawable.ic_menu);
        getSupportActionBar().setHomeAsUpIndicator(leftDrawable);

        Bottomnavigationview();

        ButterKnife.bind(this);

        loadFragment(new Home_Fragment(),true);

    }

    private void Bottomnavigationview() {
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                switch (item.getItemId()) {
                    case R.id.btmnav_home_parents:
                        toolbar.setVisibility(View.VISIBLE);
                        getSupportActionBar().setHomeButtonEnabled(true);
                        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                        Drawable leftDrawable = AppCompatResources.getDrawable(Dashboard_ParentsActivity.this, R.drawable.ic_menu);
                        getSupportActionBar().setHomeAsUpIndicator(leftDrawable);
                        loadFragment(new Home_Fragment(),true);
                        getBottomIconsChange("home");

                        break;

                    case R.id.btmnav_notification_parents:

                        loadFragment(new Notification_Parents_Fragment(),false);
                        toolbar.setVisibility(View.GONE);

                        break;

                    case R.id.btmnav_events_parents:

                        loadFragment(new Events_Parents_Fragment(),false);
                        toolbar.setVisibility(View.GONE);
                        break;

                    case R.id.btmnav_profile_parents:

                        loadFragment(new Profile_Parents_Fragment(),false);
                        toolbar.setVisibility(View.GONE);
                        break;

                    default:
                        break;
                }

                return true;
            }
        });
    }

    @Override
    public void onBackPressed() {
/*
        if (SharedPreference.getStringPreference(this, "HOMEVALUE").equalsIgnoreCase("1")) {
            finish();
        }
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }*/

        if (mIsHomeFragment) {
            Dialog dialog_fp = new Dialog(this, R.style.customdialog_theme);
            dialog_fp.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog_fp.setContentView(R.layout.alert_dialogue);
            dialog_fp.getWindow().getAttributes().windowAnimations = R.style.customdialog_theme;
            WindowManager.LayoutParams lp_fp = new WindowManager.LayoutParams();
            Window window_fp = dialog_fp.getWindow();
            lp_fp.copyFrom(window_fp.getAttributes());
            lp_fp.width = WindowManager.LayoutParams.MATCH_PARENT;
            lp_fp.height = WindowManager.LayoutParams.MATCH_PARENT;
            dialog_fp.setCancelable(false);
            dialog_fp.setCanceledOnTouchOutside(false);
            dialog_fp.getWindow().setLayout(lp_fp.width, lp_fp.height);


            Button dialogButton = dialog_fp.findViewById(R.id.btn_ok_alert);
            dialogButton.setOnClickListener(v -> {
                dialog_fp.dismiss();
                finish();
            });

            Button dialogNo = dialog_fp.findViewById(R.id.btn_cancel_alert);
            dialogNo.setOnClickListener(view -> dialog_fp.dismiss());

            if (dialog_fp.isShowing()) {
                return;
            }
            dialog_fp.show();

        } else {

            getBottomIconsChange("home");
            bottomNavigationView.setSelectedItemId(R.id.btmnav_home_parents);
        }

    }

    @OnClick(R.id.menu_nav_home_parents)
    public void home_nav() {

        mIsHomeFragment=true;
        mmenu_nav_home_parents.setTextColor(Color.parseColor("#4E096C"));
        view_nav_home.setVisibility(View.VISIBLE);

        mmenu_nav_peter_memories.setTextColor(Color.parseColor("#707070"));
        view_nav_peter_mem.setVisibility(View.GONE);

        mmenu_nav_notification_parents.setTextColor(Color.parseColor("#707070"));
        view_nav_ntfcn.setVisibility(View.GONE);

        mmenu_nav_events_parents.setTextColor(Color.parseColor("#707070"));
        view_nav_events.setVisibility(View.GONE);

        mmenu_nav_profile_parents.setTextColor(Color.parseColor("#707070"));
        view_nav_profile.setVisibility(View.GONE);

        mmenu_nav_help_sup_parents.setTextColor(Color.parseColor("#707070"));
        view_nav_hlp_sup.setVisibility(View.GONE);

        mmenu_nav_privacy_policy_parents.setTextColor(Color.parseColor("#707070"));
        view_nav_prv_plcy.setVisibility(View.GONE);

        mmenu_nav_terms_conds_parents.setTextColor(Color.parseColor("#707070"));
        view_nav_terms_cnds.setVisibility(View.GONE);

        mmenu_nav_logout_parents.setTextColor(Color.parseColor("#707070"));
        view_nav_logout.setVisibility(View.GONE);

        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
        getBottomIconsChange("home");
        toolbar.setVisibility(View.VISIBLE);
        startActivity(new Intent(Dashboard_ParentsActivity.this, Dashboard_ParentsActivity.class));
    }

    @OnClick(R.id.menu_nav_peter_memories)
    public void memories_nav() {

        mmenu_nav_home_parents.setTextColor(Color.parseColor("#707070"));
        view_nav_home.setVisibility(View.GONE);

        mmenu_nav_peter_memories.setTextColor(Color.parseColor("#4E096C"));
        view_nav_peter_mem.setVisibility(View.VISIBLE);

        mmenu_nav_notification_parents.setTextColor(Color.parseColor("#707070"));
        view_nav_ntfcn.setVisibility(View.GONE);

        mmenu_nav_events_parents.setTextColor(Color.parseColor("#707070"));
        view_nav_events.setVisibility(View.GONE);

        mmenu_nav_profile_parents.setTextColor(Color.parseColor("#707070"));
        view_nav_profile.setVisibility(View.GONE);

        mmenu_nav_help_sup_parents.setTextColor(Color.parseColor("#707070"));
        view_nav_hlp_sup.setVisibility(View.GONE);

        mmenu_nav_privacy_policy_parents.setTextColor(Color.parseColor("#707070"));
        view_nav_prv_plcy.setVisibility(View.GONE);

        mmenu_nav_terms_conds_parents.setTextColor(Color.parseColor("#707070"));
        view_nav_terms_cnds.setVisibility(View.GONE);

        mmenu_nav_logout_parents.setTextColor(Color.parseColor("#707070"));
        view_nav_logout.setVisibility(View.GONE);

        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
        startActivity(new Intent(Dashboard_ParentsActivity.this, MemoriesActivity.class));
    }

    @OnClick(R.id.menu_nav_notification_parents)
    public void notification_nav() {

        mmenu_nav_home_parents.setTextColor(Color.parseColor("#707070"));
        view_nav_home.setVisibility(View.GONE);

        mmenu_nav_peter_memories.setTextColor(Color.parseColor("#707070"));
        view_nav_peter_mem.setVisibility(View.GONE);

        mmenu_nav_notification_parents.setTextColor(Color.parseColor("#4E096C"));
        view_nav_ntfcn.setVisibility(View.VISIBLE);

        mmenu_nav_events_parents.setTextColor(Color.parseColor("#707070"));
        view_nav_events.setVisibility(View.GONE);

        mmenu_nav_profile_parents.setTextColor(Color.parseColor("#707070"));
        view_nav_profile.setVisibility(View.GONE);

        mmenu_nav_help_sup_parents.setTextColor(Color.parseColor("#707070"));
        view_nav_hlp_sup.setVisibility(View.GONE);

        mmenu_nav_privacy_policy_parents.setTextColor(Color.parseColor("#707070"));
        view_nav_prv_plcy.setVisibility(View.GONE);

        mmenu_nav_terms_conds_parents.setTextColor(Color.parseColor("#707070"));
        view_nav_terms_cnds.setVisibility(View.GONE);

        mmenu_nav_logout_parents.setTextColor(Color.parseColor("#707070"));
        view_nav_logout.setVisibility(View.GONE);


        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
        getBottomIconsChange("notification");
        toolbar.setVisibility(View.GONE);
        //loadFragment(new Notification_Parents_Fragment(),false);
        bottomNavigationView.setSelectedItemId(R.id.btmnav_notification_parents);
    }

    @OnClick(R.id.menu_nav_events_parents)
    public void events_nav() {

        mmenu_nav_home_parents.setTextColor(Color.parseColor("#707070"));
        view_nav_home.setVisibility(View.GONE);

        mmenu_nav_peter_memories.setTextColor(Color.parseColor("#707070"));
        view_nav_peter_mem.setVisibility(View.GONE);

        mmenu_nav_notification_parents.setTextColor(Color.parseColor("#707070"));
        view_nav_ntfcn.setVisibility(View.GONE);

        mmenu_nav_events_parents.setTextColor(Color.parseColor("#4E096C"));
        view_nav_events.setVisibility(View.VISIBLE);

        mmenu_nav_profile_parents.setTextColor(Color.parseColor("#707070"));
        view_nav_profile.setVisibility(View.GONE);

        mmenu_nav_help_sup_parents.setTextColor(Color.parseColor("#707070"));
        view_nav_hlp_sup.setVisibility(View.GONE);

        mmenu_nav_privacy_policy_parents.setTextColor(Color.parseColor("#707070"));
        view_nav_prv_plcy.setVisibility(View.GONE);

        mmenu_nav_terms_conds_parents.setTextColor(Color.parseColor("#707070"));
        view_nav_terms_cnds.setVisibility(View.GONE);

        mmenu_nav_logout_parents.setTextColor(Color.parseColor("#707070"));
        view_nav_logout.setVisibility(View.GONE);

        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
        getBottomIconsChange("event");
        toolbar.setVisibility(View.GONE);
        //loadFragment(new Events_Parents_Fragment(),false);
        bottomNavigationView.setSelectedItemId(R.id.btmnav_events_parents);
    }


    @OnClick({R.id.menu_nav_profile_parents})
    public void profile() {

        mmenu_nav_home_parents.setTextColor(Color.parseColor("#707070"));
        view_nav_home.setVisibility(View.GONE);

        mmenu_nav_peter_memories.setTextColor(Color.parseColor("#707070"));
        view_nav_peter_mem.setVisibility(View.GONE);

        mmenu_nav_notification_parents.setTextColor(Color.parseColor("#707070"));
        view_nav_ntfcn.setVisibility(View.GONE);

        mmenu_nav_events_parents.setTextColor(Color.parseColor("#707070"));
        view_nav_events.setVisibility(View.GONE);

        mmenu_nav_profile_parents.setTextColor(Color.parseColor("#4E096C"));
        view_nav_profile.setVisibility(View.VISIBLE);

        mmenu_nav_help_sup_parents.setTextColor(Color.parseColor("#707070"));
        view_nav_hlp_sup.setVisibility(View.GONE);

        mmenu_nav_privacy_policy_parents.setTextColor(Color.parseColor("#707070"));
        view_nav_prv_plcy.setVisibility(View.GONE);

        mmenu_nav_terms_conds_parents.setTextColor(Color.parseColor("#707070"));
        view_nav_terms_cnds.setVisibility(View.GONE);

        mmenu_nav_logout_parents.setTextColor(Color.parseColor("#707070"));
        view_nav_logout.setVisibility(View.GONE);

        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
        getBottomIconsChange("profile");
        toolbar.setVisibility(View.GONE);
        //loadFragment(new Profile_Parents_Fragment(),false);
        bottomNavigationView.setSelectedItemId(R.id.btmnav_profile_parents);
    }


    @OnClick(R.id.menu_nav_help_sup_parents)
    public void classes_nav() {

        mmenu_nav_home_parents.setTextColor(Color.parseColor("#707070"));
        view_nav_home.setVisibility(View.GONE);

        mmenu_nav_peter_memories.setTextColor(Color.parseColor("#707070"));
        view_nav_peter_mem.setVisibility(View.GONE);

        mmenu_nav_notification_parents.setTextColor(Color.parseColor("#707070"));
        view_nav_ntfcn.setVisibility(View.GONE);

        mmenu_nav_events_parents.setTextColor(Color.parseColor("#707070"));
        view_nav_events.setVisibility(View.GONE);

        mmenu_nav_profile_parents.setTextColor(Color.parseColor("#707070"));
        view_nav_profile.setVisibility(View.GONE);

        mmenu_nav_help_sup_parents.setTextColor(Color.parseColor("#4E096C"));
        view_nav_hlp_sup.setVisibility(View.VISIBLE);

        mmenu_nav_privacy_policy_parents.setTextColor(Color.parseColor("#707070"));
        view_nav_prv_plcy.setVisibility(View.GONE);

        mmenu_nav_terms_conds_parents.setTextColor(Color.parseColor("#707070"));
        view_nav_terms_cnds.setVisibility(View.GONE);

        mmenu_nav_logout_parents.setTextColor(Color.parseColor("#707070"));
        view_nav_logout.setVisibility(View.GONE);

        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
        startActivity(new Intent(Dashboard_ParentsActivity.this, Help_Support_2_Activity.class));
    }

    @OnClick({R.id.menu_nav_privacy_policy_parents})
    public void privacy_policy_nav() {

        mmenu_nav_home_parents.setTextColor(Color.parseColor("#707070"));
        view_nav_home.setVisibility(View.GONE);

        mmenu_nav_peter_memories.setTextColor(Color.parseColor("#707070"));
        view_nav_peter_mem.setVisibility(View.GONE);

        mmenu_nav_notification_parents.setTextColor(Color.parseColor("#707070"));
        view_nav_ntfcn.setVisibility(View.GONE);

        mmenu_nav_events_parents.setTextColor(Color.parseColor("#707070"));
        view_nav_events.setVisibility(View.GONE);

        mmenu_nav_profile_parents.setTextColor(Color.parseColor("#707070"));
        view_nav_profile.setVisibility(View.GONE);

        mmenu_nav_help_sup_parents.setTextColor(Color.parseColor("#707070"));
        view_nav_hlp_sup.setVisibility(View.GONE);

        mmenu_nav_privacy_policy_parents.setTextColor(Color.parseColor("#4E096C"));
        view_nav_prv_plcy.setVisibility(View.VISIBLE);

        mmenu_nav_terms_conds_parents.setTextColor(Color.parseColor("#707070"));
        view_nav_terms_cnds.setVisibility(View.GONE);

        mmenu_nav_logout_parents.setTextColor(Color.parseColor("#707070"));
        view_nav_logout.setVisibility(View.GONE);

        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
        startActivity(new Intent(Dashboard_ParentsActivity.this, Privacy_Policy_2_Activity.class));
    }

    @OnClick({R.id.menu_nav_terms_conds_parents})
    public void terms_conditions_nav() {

        mmenu_nav_home_parents.setTextColor(Color.parseColor("#707070"));
        view_nav_home.setVisibility(View.GONE);

        mmenu_nav_peter_memories.setTextColor(Color.parseColor("#707070"));
        view_nav_peter_mem.setVisibility(View.GONE);

        mmenu_nav_notification_parents.setTextColor(Color.parseColor("#707070"));
        view_nav_ntfcn.setVisibility(View.GONE);

        mmenu_nav_events_parents.setTextColor(Color.parseColor("#707070"));
        view_nav_events.setVisibility(View.GONE);

        mmenu_nav_profile_parents.setTextColor(Color.parseColor("#707070"));
        view_nav_profile.setVisibility(View.GONE);

        mmenu_nav_help_sup_parents.setTextColor(Color.parseColor("#707070"));
        view_nav_hlp_sup.setVisibility(View.GONE);

        mmenu_nav_privacy_policy_parents.setTextColor(Color.parseColor("#707070"));
        view_nav_prv_plcy.setVisibility(View.GONE);

        mmenu_nav_terms_conds_parents.setTextColor(Color.parseColor("#4E096C"));
        view_nav_terms_cnds.setVisibility(View.VISIBLE);

        mmenu_nav_logout_parents.setTextColor(Color.parseColor("#707070"));
        view_nav_logout.setVisibility(View.GONE);

        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
        startActivity(new Intent(Dashboard_ParentsActivity.this, Terms_Conds_2_Activity.class));
    }

    @OnClick({R.id.menu_nav_logout_parents})
    public void logout_nav() {

        Dialog dialog_fp = new Dialog(this, R.style.customdialog_theme);
        dialog_fp.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog_fp.setContentView(R.layout.alert_dialogue);
        ((TextView)dialog_fp.findViewById(R.id.tv_alertmessage)).setText(R.string.logoff);
        dialog_fp.getWindow().getAttributes().windowAnimations = R.style.customdialog_theme;
        WindowManager.LayoutParams lp_fp = new WindowManager.LayoutParams();
        Window window_fp = dialog_fp.getWindow();
        lp_fp.copyFrom(window_fp.getAttributes());
        lp_fp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp_fp.height = WindowManager.LayoutParams.MATCH_PARENT;
        dialog_fp.setCancelable(false);
        dialog_fp.setCanceledOnTouchOutside(false);
        dialog_fp.getWindow().setLayout(lp_fp.width, lp_fp.height);


        Button dialogButton = dialog_fp.findViewById(R.id.btn_ok_alert);
        dialogButton.setOnClickListener(v -> {
            dialog_fp.dismiss();
            logoff();
            //finish();
        });

        Button dialogNo = dialog_fp.findViewById(R.id.btn_cancel_alert);
        dialogNo.setOnClickListener(view -> dialog_fp.dismiss());

        if (dialog_fp.isShowing()) {
            return;
        }
        dialog_fp.show();
    }

    private void logoff() {

        mmenu_nav_home_parents.setTextColor(Color.parseColor("#707070"));
        view_nav_home.setVisibility(View.GONE);

        mmenu_nav_peter_memories.setTextColor(Color.parseColor("#707070"));
        view_nav_peter_mem.setVisibility(View.GONE);

        mmenu_nav_notification_parents.setTextColor(Color.parseColor("#707070"));
        view_nav_ntfcn.setVisibility(View.GONE);

        mmenu_nav_events_parents.setTextColor(Color.parseColor("#707070"));
        view_nav_events.setVisibility(View.GONE);

        mmenu_nav_profile_parents.setTextColor(Color.parseColor("#707070"));
        view_nav_profile.setVisibility(View.GONE);

        mmenu_nav_help_sup_parents.setTextColor(Color.parseColor("#707070"));
        view_nav_hlp_sup.setVisibility(View.GONE);

        mmenu_nav_privacy_policy_parents.setTextColor(Color.parseColor("#707070"));
        view_nav_prv_plcy.setVisibility(View.GONE);

        mmenu_nav_terms_conds_parents.setTextColor(Color.parseColor("#4E096C"));
        view_nav_terms_cnds.setVisibility(View.GONE);

        mmenu_nav_logout_parents.setTextColor(Color.parseColor("#4E096C"));
        view_nav_logout.setVisibility(View.VISIBLE);

        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }

        finish();
        startActivity(new Intent(Dashboard_ParentsActivity.this, LoginActivity.class));
    }


    public boolean loadFragment(Fragment fragment ,boolean isHomeFragment) {
        mIsHomeFragment = isHomeFragment;
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction ft = manager.beginTransaction();
        ft.replace(R.id.frameLayout_2, fragment);
        ft.addToBackStack(null);
        ft.commit();

        return true;
    }


    @SuppressLint("ResourceType")
    public void getBottomIconsChange(String menuName) {

        Menu menu = bottomNavigationView.getMenu();
//        menu.findItem(R.id.btmnav_home).setIcon(R.drawable.ic_notification_menu);

        if (menuName.equalsIgnoreCase("home")) {
            mIsHomeFragment = true;
//            Toast.makeText(this, "h " + menuName, Toast.LENGTH_SHORT).show();

//            menu.add(Menu.NONE,menu.findItem(R.id.btmnav_home), Menu.NONE, ).setIcon(R.drawable.ic_action_one);

//            menu.findItem(R.id.btmnav_home).setIcon(R.drawable.home_active);
            Utility.tintMenuIcon(this, menu.findItem(R.id.btmnav_home_parents), R.color.color_dark_violet);
            bottomNavigationView.setItemTextAppearanceActive(R.color.color_dark_violet);
//            menu.findItem(R.id.btmnav_notification).setIcon(R.drawable.notification);
//            menu.findItem(R.id.btmnav_events).setIcon(R.drawable.event);
//            menu.findItem(R.id.btmnav_profile).setIcon(R.drawable.profile);

//            bottomNavigationView.findViewById(R.id.btmnav_home).setIte
        } else if (menuName.equalsIgnoreCase("notification")) {
            mIsHomeFragment = false;
//            Toast.makeText(this, "n " + menuName, Toast.LENGTH_SHORT).show();
            bottomNavigationView.setItemTextAppearanceActive(R.color.colorPrimaryDark);
//            menu.findItem(R.id.btmnav_home).setIcon(R.drawable.home);
            Utility.tintMenuIcon(this, menu.findItem(R.id.btmnav_notification_parents), R.color.color_dark_violet);
//            menu.findItem(R.id.btmnav_notification).setIcon(R.drawable.notification_active);
//            menu.findItem(R.id.btmnav_events).setIcon(R.drawable.event);
//            menu.findItem(R.id.btmnav_profile).setIcon(R.drawable.profile);
            bottomNavigationView.setItemTextAppearanceActive(R.color.color_dark_violet);

        } else if (menuName.equalsIgnoreCase("event")) {
            mIsHomeFragment = false;
//            Toast.makeText(this, "e " + menuName, Toast.LENGTH_SHORT).show();
            bottomNavigationView.setItemTextAppearanceActive(R.color.color_dark_violet);
//            menu.findItem(R.id.btmnav_home).setIcon(R.drawable.home);
//            menu.findItem(R.id.btmnav_notification).setIcon(R.drawable.notification);
            Utility.tintMenuIcon(this, menu.findItem(R.id.btmnav_events_parents), R.color.color_dark_violet);
//            menu.findItem(R.id.btmnav_events).setIcon(R.drawable.event_active);
//            menu.findItem(R.id.btmnav_profile).setIcon(R.drawable.profile);

        } else if (menuName.equalsIgnoreCase("profile")) {
            mIsHomeFragment = false;
//            Toast.makeText(this, "p " + menuName, Toast.LENGTH_SHORT).show();
            bottomNavigationView.setItemTextAppearanceActive(R.color.color_dark_violet);
//            menu.findItem(R.id.btmnav_home).setIcon(R.drawable.home);
//            menu.findItem(R.id.btmnav_notification).setIcon(R.drawable.notification);
//            menu.findItem(R.id.btmnav_events).setIcon(R.drawable.event);
            Utility.tintMenuIcon(this, menu.findItem(R.id.btmnav_profile_parents), R.color.color_dark_violet);
//            menu.findItem(R.id.btmnav_profile).setIcon(R.drawable.profile_active);

        }

//        bottomNavigationView.setItemIconTintList(null);


    }


    @Override
    protected void onResume() {
        super.onResume();
        // Utility.hideKeyboard(this);

        getBottomIconsChange("home");
        bottomNavigationView.setSelectedItemId(R.id.btmnav_home_parents);
    }

}
