package com.versatilemobitech.oyku.Activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.versatilemobitech.oyku.R;
import com.versatilemobitech.oyku.interfaces.EActivity;
import com.versatilemobitech.oyku.utilities.Utility;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class Forgot_Password_Activity extends AppCompatActivity implements EActivity {

    @BindView(R.id.tvForgotPassword)
    TextView tvForgotPassword;

    @BindView(R.id.etEmail)
    EditText etEmail;

    @BindView(R.id.imv_back_arrow)
    ImageView imv_back_arrow;


    @BindView(R.id.coordinatorLayout)
    CoordinatorLayout coordinatorLayout;
    @BindView(R.id.iv_toolbar_menu)
    ImageView iv_toolbar_menu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN|WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        setContentView(R.layout.activity_forgot_password);

        ButterKnife.bind(this);

        Toolbar toolbar = findViewById(R.id.toolbar_title);
        TextView tv_title = toolbar.findViewById(R.id.tv_title);
        tv_title.setText("Forgot Password");
        imv_back_arrow.setVisibility(View.GONE);
        toolbar.setNavigationIcon(R.drawable.ic_back_arrow);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

    }

    @OnClick(R.id.tvForgotPassword)
    public void btn_submit() {

        String email = etEmail.getText().toString().trim();

        if (email.isEmpty())
        {
            showSnackBar("Please enter email",2);
        }
        else if (!Utility.isValidEmail(email))
        {
            showSnackBar("Please enter valid email",3);
        }
        else
        {
            startActivity(new Intent(getApplicationContext(), LoginActivity.class));
            Toast.makeText(this, "Password Sent Successfully To Your Email", Toast.LENGTH_SHORT).show();
            finish();
        }

    }

    @OnClick(R.id.etEmail)
    public void edittext_fp() {

    }

//    @OnClick(R.id.imv_back_arrow)
//    public void back_nav() {
//        startActivity(new Intent(getApplicationContext(), LoginActivity.class));
//    }

    @Override
    public void changeTitle(String title) {

    }

    @Override
        public void showSnackBar(String snackBarText, int type) {

        Utility.showSnackBar(this,coordinatorLayout,snackBarText,type);
    }

    @Override
    public Activity activity() {
        return this;
    }
}
