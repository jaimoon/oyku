package com.versatilemobitech.oyku.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.versatilemobitech.oyku.R;
import com.versatilemobitech.oyku.utilities.SharedPreference;

public class Help_SupportActivity extends AppCompatActivity {

    @BindView(R.id.tv_text_hs)
    TextView tv_text_hs;

    @BindView(R.id.tv_email_id)
    TextView tv_email_id;

    @BindView(R.id.tv_ph_no)
    TextView tv_ph_no;

    @BindView(R.id.tv_mail_id)
    TextView tv_mail_id;

    @BindView(R.id.tv_contact_no)
    TextView tv_contact_no;

    @BindView(R.id.imv_back_arrow)
    ImageView imv_back_arrow;

    @BindView(R.id.iv_toolbar_menu)
    ImageView iv_toolbar_menu;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_help_support);

        ButterKnife.bind(this);

        Toolbar toolbar = findViewById(R.id.toolbar_title);
        TextView tv_title = toolbar.findViewById(R.id.tv_title);
        tv_title.setText("Help & Support");
        imv_back_arrow.setVisibility(View.GONE);
        SharedPreference.setStringPreference(this,"HOMEVALUE","2");
        toolbar.setNavigationIcon(R.drawable.ic_back_arrow);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        iv_toolbar_menu.setVisibility(View.INVISIBLE);
    }


    @OnClick(R.id.imv_back_arrow)
    public void back_nav() {

        onBackPressed();
    }
}
