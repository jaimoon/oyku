package com.versatilemobitech.oyku.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.versatilemobitech.oyku.R;

public class Help_Support_2_Activity extends AppCompatActivity {

    @BindView(R.id.tv_text_hs_2)
    TextView tv_text_hs;

    @BindView(R.id.tv_email_id_2)
    TextView tv_email_id;

    @BindView(R.id.tv_ph_no_2)
    TextView tv_ph_no;

    @BindView(R.id.tv_mail_id_2)
    TextView tv_mail_id;

    @BindView(R.id.tv_contact_no_2)
    TextView tv_contact_no;

    @BindView(R.id.imv_back_arrow)
    ImageView imv_back_arrow;

    ImageView iv_toolbar_menu;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_help__support_2);

        ButterKnife.bind(this);

        Toolbar toolbar = findViewById(R.id.toolbar_title);
        TextView tv_title = toolbar.findViewById(R.id.tv_title);
        iv_toolbar_menu=findViewById(R.id.iv_toolbar_menu);
        iv_toolbar_menu.setVisibility(View.GONE);
        imv_back_arrow.setVisibility(View.VISIBLE);
        tv_title.setText("Help & Support");
    }

    @OnClick(R.id.imv_back_arrow)
    public void back_nav() {
        startActivity(new Intent(getApplicationContext(), Dashboard_ParentsActivity.class));
    }
}
