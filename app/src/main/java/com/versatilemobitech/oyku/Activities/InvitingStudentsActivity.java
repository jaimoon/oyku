package com.versatilemobitech.oyku.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.versatilemobitech.oyku.Adapters.InviteStudent_Adapter;
import com.versatilemobitech.oyku.Model.InviteStudent_Model;
import com.versatilemobitech.oyku.R;
import com.versatilemobitech.oyku.utilities.SharedPreference;

public class InvitingStudentsActivity extends AppCompatActivity {

    private InviteStudent_Model inviteStudent_model;


    @BindView(R.id.imv_back_arrow)
    ImageView mimv_back_arrow;

    @BindView(R.id.iv_toolbar_menu)
    ImageView iv_toolbar_menu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inviting_students);

        ButterKnife.bind(this);

        Toolbar toolbar = findViewById(R.id.toolbar_title);
        TextView tv_title = toolbar.findViewById(R.id.tv_title);
        tv_title.setText("INVITE PARENTS");
        SharedPreference.setStringPreference(this,"HOMEVALUE","2");
        mimv_back_arrow=findViewById(R.id.imv_back_arrow);
        mimv_back_arrow.setVisibility(View.GONE);
        iv_toolbar_menu.setVisibility(View.INVISIBLE);
        toolbar.setNavigationIcon(R.drawable.ic_back_arrow);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
//        mimv_back_arrow.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                onBackPressed();
//            }
//        });


        RecyclerView recyclerView = findViewById(R.id.recycler_view_invite_student);
        InviteStudent_Adapter inviteStudent_adapter = new InviteStudent_Adapter(this, inviteStudent_model);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(inviteStudent_adapter);
    }


//    @OnClick(R.id.tvAddGroupNew)
//    public void btn_add_group(){
//
//        finish();
//    }

}
