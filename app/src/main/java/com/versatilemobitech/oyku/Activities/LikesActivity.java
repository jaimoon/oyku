package com.versatilemobitech.oyku.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.versatilemobitech.oyku.R;
import com.versatilemobitech.oyku.customviews.CommentsAdpater;
import com.versatilemobitech.oyku.customviews.LikesAdpater;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LikesActivity extends AppCompatActivity {

    ImageView iv_toolbar_menu;

    @BindView(R.id.imv_back_arrow)
    ImageView mimv_back_arrow;

    @BindView(R.id.recycler_likes_list)
    RecyclerView recycler_likes_list;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_likes);



        ButterKnife.bind(this);
        Toolbar toolbar = findViewById(R.id.toolbar_title);
        TextView tv_title = toolbar.findViewById(R.id.tv_title);
        tv_title.setText("Likes");

        ImageView imgView= toolbar.findViewById(R.id.iv_toolbar_menu);
        mimv_back_arrow=toolbar.findViewById(R.id.imv_back_arrow);
        imgView.setVisibility(View.GONE);

        toolbar.setNavigationIcon(R.drawable.back_arrow);
        mimv_back_arrow.setVisibility(View.GONE);
        toolbar.setNavigationIcon(R.drawable.ic_back_arrow);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });


        iv_toolbar_menu = findViewById(R.id.iv_toolbar_menu);
        iv_toolbar_menu.setVisibility(View.GONE);

        showLiks();

    }

    private void showLiks() {

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false);
        recycler_likes_list.setLayoutManager(layoutManager);
        recycler_likes_list.setAdapter(new LikesAdpater(this));
    }

}