package com.versatilemobitech.oyku.Activities;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.InputType;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.versatilemobitech.oyku.R;
import com.versatilemobitech.oyku.interfaces.EActivity;
import com.versatilemobitech.oyku.utilities.Utility;

public class LoginActivity extends AppCompatActivity implements EActivity, AdapterView.OnItemSelectedListener {

    private static final String PARENT = "Parent";
    private static final String TEACHER = "Teacher";

    @BindView(R.id.spinner)
    Spinner mspinner;

    @BindView(R.id.tvLogin)
    TextView tvLogin;

    @BindView(R.id.edt_email)
    EditText etEmail;

    @BindView(R.id.edt_password)
    EditText etPassword;

    @BindView(R.id.tv_forgot_password)
    TextView mtv_forgot_password;

    @BindView(R.id.tv_terms_conds)
    TextView mtv_terms_conds;

    @BindView(R.id.coordinatorLayout)
    CoordinatorLayout coordinatorLayout;

    @BindView(R.id.tv_text)
    TextView tv_text;

    @BindView(R.id.ivShowPassword)
    ImageView ivShowPassword;


    @BindView(R.id.ivHidePassword)
    ImageView ivHidePassword;
    private int CAMERA_REQ_CODE;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        setContentView(R.layout.activity_login);

        ButterKnife.bind(this);

        setOnClickListeners();

        Spinner spinner = (Spinner) findViewById(R.id.spinner);
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, R.layout.custom_spinner, getResources().getStringArray(R.array.list));
        adapter.setDropDownViewResource(R.layout.custom_spinner_dropdown);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(this);
    }

    private void setOnClickListeners() {

        ivShowPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                etPassword.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD);
                ivHidePassword.setVisibility(View.VISIBLE);
                ivShowPassword.setVisibility(View.GONE);
                etPassword.setSelection(etPassword.length());
            }
        });

        ivHidePassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                etPassword.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                ivHidePassword.setVisibility(View.GONE);
                ivShowPassword.setVisibility(View.VISIBLE);
                etPassword.setSelection(etPassword.length());
            }
        });

        etPassword.setSelection(etPassword.length());


    }

    @OnClick(R.id.tvPrivacy)
    public void privacy_policy() {

        Intent intent = new Intent(LoginActivity.this, PrivacyPolicyActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);

    }


    @OnClick(R.id.tvLogin)
    public void btn_login() {

        // startActivity(new Intent(LoginActivity.this, DashboardActivity.class));
    }

    @OnClick(R.id.tv_terms_conds)
    public void terms_conds() {
        Intent intent = new Intent(LoginActivity.this, TermsConditionsActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    @OnClick(R.id.tv_forgot_password)
    public void forgot_password() {

        Intent intent = new Intent(LoginActivity.this, Forgot_Password_Activity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

        if (position == 0) {

            tvLogin.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String email = etEmail.getText().toString().trim();
                    String password = etPassword.getText().toString().trim();
                    if (email.isEmpty()) {
                        showSnackBar("Please enter email", 2);
                    } else if (!Utility.isValidEmail(email)) {
                        showSnackBar("Please enter valid email", 3);
                    } else if (password.isEmpty()) {
                        showSnackBar("Please enter password", 2);
                    } else {
                        navigateToDashboard(TEACHER);
                    }
                }
            });

        } else if (position == 1) {

            tvLogin.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    String email = etEmail.getText().toString().trim();
                    String password = etPassword.getText().toString().trim();


                    if (email.isEmpty()) {
                        showSnackBar("Please enter email", 2);
                    } else if (!Utility.isValidEmail(email)) {
                        showSnackBar("Please enter valid email", 3);
                    } else if (password.isEmpty()) {
                        showSnackBar("Please enter password", 2);
                    } else {

                        Dialog dialog_fp = new Dialog(LoginActivity.this, R.style.customdialog_theme);
                        dialog_fp.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        dialog_fp.setContentView(R.layout.dialog_face_rec);
                        dialog_fp.getWindow().getAttributes().windowAnimations = R.style.customdialog_theme;
                        WindowManager.LayoutParams lp_fp = new WindowManager.LayoutParams();
                        Window window_fp = dialog_fp.getWindow();
                        lp_fp.copyFrom(window_fp.getAttributes());
                        lp_fp.width = WindowManager.LayoutParams.MATCH_PARENT;
                        lp_fp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                        dialog_fp.setCancelable(false);
                        dialog_fp.setCanceledOnTouchOutside(false);
                        dialog_fp.getWindow().setLayout(lp_fp.width, lp_fp.height);
                        dialog_fp.show();

                        Button dialogButton_face_reg = dialog_fp.findViewById(R.id.btn_face_reg);
                        dialogButton_face_reg.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialogButton_face_reg.setBackgroundColor(R.drawable.bg_dialog_button_enable_clr);
                                dialogButton_face_reg.getResources().getColor(R.color.color_white);

                                dialog_fp.dismiss();

                                Toast.makeText(LoginActivity.this, "Coming Soon..!", Toast.LENGTH_SHORT).show();

                                // Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
                                //startActivityForResult(intent, CAMERA_REQ_CODE);

                                //Jay Changes
                                /*Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                                if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                                    startActivityForResult(takePictureIntent, CAMERA_REQ_CODE);
                                }*/

                            }
                        });

                        Button dialogButton_skip = dialog_fp.findViewById(R.id.btn_skip);
                        dialogButton_skip.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialogButton_skip.setBackgroundColor(R.drawable.bg_dialog_button_enable_clr);
                                dialogButton_skip.getResources().getColor(R.color.color_white);
                                dialog_fp.dismiss();
                                navigateToDashboard(PARENT);
                            }
                        });
                    }
                }
            });
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
    }

    @Override
    public void changeTitle(String title) {

    }

    @Override
    public void showSnackBar(String snackBarText, int type) {

        Utility.showSnackBar(this, coordinatorLayout, snackBarText, type);
    }

    @Override
    public Activity activity() {
        return this;
    }

    private void navigateToDashboard(String type){
        switch (type){
            case PARENT:
                startActivity(new Intent(LoginActivity.this,Dashboard_ParentsActivity.class));
                finish();
                break;
            case TEACHER:
                startActivity(new Intent(LoginActivity.this,DashboardActivity.class));
                finish();
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode ==CAMERA_REQ_CODE)
        {

            navigateToDashboard(PARENT);
        }
    }
}
