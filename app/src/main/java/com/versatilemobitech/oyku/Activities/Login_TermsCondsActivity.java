package com.versatilemobitech.oyku.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.versatilemobitech.oyku.R;

public class Login_TermsCondsActivity extends AppCompatActivity {


    @BindView(R.id.tv_text_tc_lgn)
    TextView tv_text_tc;

    @BindView(R.id.imv_back_arrow)
    ImageView imv_back_arrow;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_terms_conds);
        imv_back_arrow.setVisibility(View.GONE);

        ButterKnife.bind(this);

        Toolbar toolbar = findViewById(R.id.toolbar_title);
        TextView tv_title = toolbar.findViewById(R.id.tv_title);
        tv_title.setText("Terms & Conditions");
    }

    @OnClick(R.id.imv_back_arrow)
    public void back_nav() {
        startActivity(new Intent(getApplicationContext(), LoginActivity.class));

    }
}
