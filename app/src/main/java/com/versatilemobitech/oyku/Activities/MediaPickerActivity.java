package com.versatilemobitech.oyku.Activities;

import android.Manifest;
import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentActivity;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.versatilemobitech.oyku.Adapters.MediaPickerPagerAdapter;
import com.versatilemobitech.oyku.R;

import java.util.List;

public class MediaPickerActivity extends FragmentActivity {

    private ViewPager mViewPager;
    private TabLayout mTabsLayout;
    private MediaPickerPagerAdapter mMediaPickerPagerAdapter;
    public static MediaPickerActivity mMediaPickerActivityInstance;
    
//    private boolean isAllPermissionsChecked = false;
    private static final String TAG = MediaPickerActivity.class.getCanonicalName();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_media_picker);
        mMediaPickerActivityInstance = MediaPickerActivity.this;
        mViewPager = findViewById(R.id.viewPager);
        mTabsLayout = findViewById(R.id.tabsLayout);

        Dexter.withContext(mMediaPickerActivityInstance).
                withPermissions(new String[]{
                        Manifest.permission.CAMERA,
                        Manifest.permission.RECORD_AUDIO,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE
                }).withListener(new MultiplePermissionsListener() {
            @Override
            public void onPermissionsChecked(MultiplePermissionsReport multiplePermissionsReport) {
                if (multiplePermissionsReport.areAllPermissionsGranted()){
                    init();
                } else {
                    Toast.makeText(mMediaPickerActivityInstance, "Permission Denied", Toast.LENGTH_SHORT).show();
                    init();
                }
            }

            @Override
            public void onPermissionRationaleShouldBeShown(List<PermissionRequest> list, PermissionToken permissionToken) {

            }
        }).check();
    }

    private void init() {
        mViewPager.setOffscreenPageLimit(1);
        mMediaPickerPagerAdapter = new MediaPickerPagerAdapter(mMediaPickerActivityInstance, getSupportFragmentManager());
        mViewPager.setAdapter(mMediaPickerPagerAdapter);
        mTabsLayout.setupWithViewPager(mViewPager);
        mTabsLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                Log.d(TAG, "onTabSelected: "+tab.getPosition());
                Log.d(TAG, "onTabSelected: "+tab.getText());
                mViewPager.setCurrentItem(tab.getPosition(), true);
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }
}
