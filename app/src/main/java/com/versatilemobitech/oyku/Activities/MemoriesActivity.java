package com.versatilemobitech.oyku.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.versatilemobitech.oyku.Adapters.Memories_Adapter;
import com.versatilemobitech.oyku.Model.Memories_Model;
import com.versatilemobitech.oyku.R;

public class MemoriesActivity extends AppCompatActivity {

    private Memories_Model memories_model;

    @BindView(R.id.imv_back_arrow)
    ImageView imv_back_arrow;

    ImageView iv_toolbar_menu;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_memories);
        ButterKnife.bind(this);

        Toolbar toolbar = findViewById(R.id.toolbar_title);
        iv_toolbar_menu=findViewById(R.id.iv_toolbar_menu);
        iv_toolbar_menu.setVisibility(View.INVISIBLE);
        imv_back_arrow.setVisibility(View.VISIBLE);
        TextView tv_title = toolbar.findViewById(R.id.tv_title);
        tv_title.setText("Memories");


        RecyclerView recyclerView = findViewById(R.id.recycler_view_memories);
        Memories_Adapter memories_adapter = new Memories_Adapter(this, memories_model);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(memories_adapter);
    }

    @OnClick(R.id.imv_back_arrow)
    public void back_nav(){

        startActivity(new Intent(MemoriesActivity.this,Dashboard_ParentsActivity.class));
    }
}
