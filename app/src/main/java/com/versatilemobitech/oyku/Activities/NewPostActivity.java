package com.versatilemobitech.oyku.Activities;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.app.ActivityCompat;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.versatilemobitech.oyku.R;
import com.versatilemobitech.oyku.interfaces.EActivity;
import com.versatilemobitech.oyku.utilities.Constants;
import com.versatilemobitech.oyku.utilities.Utility;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;

public class NewPostActivity extends AppCompatActivity implements EActivity {

    private static final int RESULT_LOAD_IMAGE = 1000;
    private boolean isCheckedValue;
    private String mImagePath = "";
    private ArrayList<String> mImages = new ArrayList<>();

    @BindView(R.id.imv_back_arrow)
    ImageView mimv_back_arrow;

    @BindView(R.id.imv_image_post)
    ImageView mimv_image_post;

    @BindView(R.id.tvPost)
    TextView mbtn_new_post;

    @BindView(R.id.coordinatorLayout)
    CoordinatorLayout coordinatorLayout;

    @BindView(R.id.switch_private)
    Switch mswitch_private;

    @BindView(R.id.switch_section)
    Switch mswitch_section;

    @BindView(R.id.switch_class)
    Switch mswitch_class;

    @BindView(R.id.switch_school)
    Switch mswitch_school;

    @BindView(R.id.switch_group)
    Switch mswitch_group;

    @BindView(R.id.iv_toolbar_menu)
    ImageView iv_toolbar_menu;

    @BindView(R.id.edt_search_post)
    EditText edt_search_post;

    public static final int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 123;
    private static final int MY_CAMERA_PERMISSION_CODE = 100;
    private static final String IMAGE_DIRECTORY = "/demonuts";
    private int GALLERY = 1, CAMERA = 2;
    public static final int PICK_IMAGE_REQUEST = 1;
    String userChoosenTask;
    Uri imageUri;
    private int SECTION_ACTIVITY_CODE=234;
    private int CLASS_SELECTION=454;
    private int STUDENT_GROPY_REQUEST=88;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        setContentView(R.layout.activity_new_post);

        Toolbar toolbar = findViewById(R.id.toolbar_title);
        TextView tv_title = toolbar.findViewById(R.id.tv_title);
        ImageView imgView= toolbar.findViewById(R.id.iv_toolbar_menu);
        mimv_back_arrow=toolbar.findViewById(R.id.imv_back_arrow);
        imgView.setVisibility(View.GONE);
        tv_title.setText("New Post");
        toolbar.setNavigationIcon(R.drawable.back_arrow);
        mimv_back_arrow.setVisibility(View.GONE);
        toolbar.setNavigationIcon(R.drawable.ic_back_arrow);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });



        ButterKnife.bind(this);

        Utility.hideKeyboard(this);

        setOnClickListeners();

        mbtn_new_post.setVisibility(View.GONE);

        //switch selection private
        mswitch_private.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                isCheckedValue = isChecked;
                if (isChecked) {
                    mswitch_section.setChecked(false);
                    mswitch_class.setChecked(false);
                    mswitch_school.setChecked(false);
                    mswitch_group.setChecked(false);

                    startActivityForResult(new Intent(NewPostActivity.this, SelectStudent.class),786);

                } else {
                }
            }
        });

        //switch selection section
        mswitch_section.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                isCheckedValue = isChecked;
                if (isChecked) {
                    mswitch_private.setChecked(false);
                    mswitch_class.setChecked(false);
                    mswitch_school.setChecked(false);
                    mswitch_group.setChecked(false);
                    startActivityForResult(new Intent(NewPostActivity.this, Select_SectionsActivity.class),SECTION_ACTIVITY_CODE);
                } else {
                }
            }
        });

        //switch selection class
        mswitch_class.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                isCheckedValue = isChecked;
                if (isChecked) {
                    mswitch_private.setChecked(false);
                    mswitch_section.setChecked(false);
                    mswitch_school.setChecked(false);
                    mswitch_group.setChecked(false);
                    startActivityForResult(new Intent(NewPostActivity.this, SelectClassActivity.class),CLASS_SELECTION);
                } else {
                }
            }
        });

        //switch selection School
        mswitch_school.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                isCheckedValue = isChecked;
                if (isChecked) {
                    mbtn_new_post.setVisibility(View.VISIBLE);
                    mswitch_private.setChecked(false);
                    mswitch_section.setChecked(false);
                    mswitch_class.setChecked(false);
                    mswitch_group.setChecked(false);
                } else {
                    mbtn_new_post.setVisibility(View.GONE);
                }
            }
        });

        //switch selection Group
        mswitch_group.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                isCheckedValue = isChecked;
                if (isChecked) {
                    mswitch_private.setChecked(false);
                    mswitch_section.setChecked(false);
                    mswitch_class.setChecked(false);
                    mswitch_school.setChecked(false);
                    startActivityForResult(new Intent(NewPostActivity.this, StudentsActivity.class),STUDENT_GROPY_REQUEST);
                } else {
                }
            }
        });


        //
        edt_search_post.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    //performSearch();
Utility.hideKeyboardFrom(NewPostActivity.this,edt_search_post);

                    return true;
                }
                return false;
            }
        });
    }

    private void setOnClickListeners() {

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        //startActivity(new Intent(this,DashboardActivity.class));
        //finish();
    }

    /*private void selectImage() {

        try {
            if (ActivityCompat.checkSelfPermission(NewPostActivity.this,
                    Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(NewPostActivity.this,
                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,
                                Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        MY_CAMERA_PERMISSION_CODE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        final CharSequence[] options = {"Take Photo", "Choose from Gallery", "Cancel"};

        AlertDialog.Builder builder = new AlertDialog.Builder(NewPostActivity.this);
        builder.setTitle("Choose Picture");
        builder.setCancelable(false);
        builder.setItems(options, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int item) {
                switch (item) {
                    case 0:
                        takePhotoFromCamera();
                        break;
                    case 1:
                        choosePhotoFromGallary();
                        break;
                    case 2:
                        builder.setCancelable(true);
                        startActivity(new Intent(getApplicationContext(),NewPostActivity.class));
                        break;
                }
            }
        });
        builder.show();
    }*/


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                           int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (userChoosenTask.equals("Take Photo"))
                        takePhotoFromCamera();

                    else if (userChoosenTask.equals("Choose from Library"))
                        choosePhotoFromGallary();
                } else {
                    //code for deny
                }
                break;
        }
    }


    private void takePhotoFromCamera() {
        Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, CAMERA);
    }

    private void choosePhotoFromGallary() {
        Intent galleryIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        galleryIntent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
        startActivityForResult(galleryIntent, GALLERY);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == SECTION_ACTIVITY_CODE)
        {
            if(resultCode ==RESULT_CANCELED)
            mswitch_section.setChecked(false);
            else onBackPressed();
        }

        if(requestCode==CLASS_SELECTION)
        {
            if(resultCode ==RESULT_CANCELED)
                mswitch_class.setChecked(false);
            else onBackPressed();
        }
        if(requestCode==STUDENT_GROPY_REQUEST)
        {
            if(resultCode ==RESULT_CANCELED)
                mswitch_group.setChecked(false);
            else onBackPressed();
        }
        if(requestCode==786)
        {
            if(resultCode ==RESULT_CANCELED)
                mswitch_private.setChecked(false);
            else onBackPressed();
        }



        if (data != null) {
           /* imageUri = data.getData();
            if (resultCode == this.RESULT_CANCELED) {
                return;
            }
            if (requestCode == GALLERY) {
                if (data != null) {
                    Uri contentURI = data.getData();
                    try {
                        Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), contentURI);
                        mImagePath = saveImage(bitmap);
                        mImages.add(mImagePath);
                        mimv_image_post.setImageBitmap(bitmap);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            } else if (requestCode == CAMERA) {
                Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
                mimv_image_post.setImageBitmap(thumbnail);
            }*/
        }
    }

    public String saveImage(Bitmap myBitmap) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        myBitmap.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
        File wallpaperDirectory = new File(Environment.getExternalStorageDirectory() + IMAGE_DIRECTORY);
        // have the object build the directory structure, if needed.
        if (!wallpaperDirectory.exists()) {
            wallpaperDirectory.mkdirs();
        }

        try {
            File f = new File(wallpaperDirectory, Calendar.getInstance()
                    .getTimeInMillis() + ".jpg");
            f.createNewFile();
            FileOutputStream fo = new FileOutputStream(f);
            fo.write(bytes.toByteArray());
            MediaScannerConnection.scanFile(this,
                    new String[]{f.getPath()},
                    new String[]{"image/jpeg"}, null);
            fo.close();
            Log.d("TAG", "File Saved::---&gt;" + f.getAbsolutePath());

            return f.getAbsolutePath();
        } catch (IOException e1) {
            e1.printStackTrace();
        }
        return "";
    }

    public String saveImages(Bitmap myBitmap) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        myBitmap.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
        File wallpaperDirectory = new File(
                Environment.getExternalStorageDirectory() + IMAGE_DIRECTORY);
        // have the object build the directory structure, if needed.
        if (!wallpaperDirectory.exists()) {
            wallpaperDirectory.mkdirs();
        }

        try {
            File f = new File(wallpaperDirectory, Calendar.getInstance()
                    .getTimeInMillis() + ".jpg");
            f.createNewFile();
            FileOutputStream fo = new FileOutputStream(f);
            fo.write(bytes.toByteArray());
            MediaScannerConnection.scanFile(this,
                    new String[]{f.getPath()},
                    new String[]{"image/jpeg"}, null);
            fo.close();
            Log.d("TAG", "File Saved::---&gt;" + f.getAbsolutePath());

            return f.getAbsolutePath();
        } catch (IOException e1) {
            e1.printStackTrace();
        }
        return "";
    }

    @OnClick(R.id.imv_back_arrow)
    public void back_nav() {
        onBackPressed();
    }

    @OnClick(R.id.imv_image_post)
    public void choose_image() {
        startActivityForResult(new Intent(NewPostActivity.this,MediaPickerActivity.class), Constants.MEDIA_PICKER_REQUEST_CODE);
//        selectImage();
        /*
        Toast.makeText(getApplicationContext(), "Post is posted successfully", Toast.LENGTH_SHORT).show();
        startActivity(new Intent(getApplicationContext(), DashboardActivity.class));*/
    }

    @OnClick(R.id.tvPost)
    public void createPost(){
        if (mImages != null && mImages.size() > 0){
            Toast.makeText(getApplicationContext(), "Post is posted successfully", Toast.LENGTH_SHORT).show();
            finish();
            //startActivity(new Intent(getApplicationContext(), DashboardActivity.class));
        } else {
            Utility.showToast(this,"Please select image",false);
        }
    }

    @Override
    public void changeTitle(String title) {

    }

    @Override
    public void showSnackBar(String snackBarText, int type) {
        Utility.showSnackBar(this,coordinatorLayout,snackBarText,type);

    }


    @Override
    public Activity activity() {
        return null;
    }


}
