package com.versatilemobitech.oyku.Activities;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import com.versatilemobitech.oyku.Adapters.PostEvent_Adapter;
import com.versatilemobitech.oyku.Model.CreateEvent_Model;
import com.versatilemobitech.oyku.R;

public class PostEventActivity extends AppCompatActivity {

    @BindView(R.id.imv_back_arrow)
    ImageView mimv_back_arrow;

    @BindView(R.id.btn_post_event)
    TextView mbtn_post_event;

    @BindView(R.id.radioButton_checked)
    ImageView mradioButton_checked;

    @BindView(R.id.radioButton_unchecked)
    ImageView mradioButton_unchecked;

    @BindView(R.id.tv_select_class)
    TextView mtv_select_class;

    @BindView(R.id.tv_select_students)
    TextView mtv_select_students;

    @BindView(R.id.tv_view_students)
    TextView mtv_view_students;

    @BindView(R.id.tv_view_class)
    TextView mtv_view_class;

    private  ImageView iv_toolbar_menu;

    private CreateEvent_Model createEvent_model;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_event);
        ButterKnife.bind(this);

        Toolbar toolbar = findViewById(R.id.toolbar_title);
        TextView tv_title = toolbar.findViewById(R.id.tv_title);
        tv_title.setText("Post Event");
        iv_toolbar_menu= findViewById(R.id.iv_toolbar_menu);
        iv_toolbar_menu.setVisibility(View.INVISIBLE);
        mimv_back_arrow.setVisibility(View.GONE);
        toolbar.setNavigationIcon(R.drawable.ic_back_arrow);
        toolbar.setNavigationOnClickListener(v -> onBackPressed());

        RecyclerView recyclerView = findViewById(R.id.recycler_view_create_event);
        PostEvent_Adapter event_adapter = new PostEvent_Adapter(this, createEvent_model);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(event_adapter);

    }

//    @OnClick(R.id.imv_back_arrow)
//    public void back_nav() {
//
//        Intent showContent = new Intent(PostEventActivity.this,CreateEventActivity.class);
//        startActivity(showContent);
//    }

    @OnClick(R.id.btn_post_event)
    public void post_btn(){

        setResult(2);
        Toast.makeText(this, "Event Posted Successfully", Toast.LENGTH_SHORT).show();
        onBackPressed();
        //Intent intent=new Intent(PostEventActivity.this,CreateEventActivity.class);
        //startActivity(intent);
    }

    @OnClick(R.id.tv_view_class)
    public void view_class(){
        Intent students_intent = new Intent(PostEventActivity.this, SelectClassActivity.class);
        startActivityForResult(students_intent,123);
    }

    @OnClick(R.id.tv_view_students)
    public void view_students(){

        Intent students_intent = new Intent(PostEventActivity.this, SelectStudent.class);
        startActivityForResult(students_intent,321);
    }

    @OnClick(R.id.radioButton_checked)
    public void rdbtn_checked(){

        mradioButton_checked.setVisibility(View.GONE);
        mradioButton_unchecked.setVisibility(View.VISIBLE);
    }

    @OnClick(R.id.radioButton_unchecked)
    public void rdbtn_unchecked(){

        mradioButton_unchecked.setVisibility(View.GONE);
        mradioButton_checked.setVisibility(View.VISIBLE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode ==123) {
            if (resultCode != RESULT_CANCELED) {
                setResult(2);
                finish();
            }
        }

        if(requestCode ==321) {
            if (resultCode != RESULT_CANCELED) {
                setResult(2);
                finish();
            }
        }
    }
}
