package com.versatilemobitech.oyku.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.versatilemobitech.oyku.Adapters.Student_Adapter;
import com.versatilemobitech.oyku.Model.Student_Model;
import com.versatilemobitech.oyku.R;
import com.versatilemobitech.oyku.interfaces.SelectAllItems;
import com.versatilemobitech.oyku.utilities.Utility;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SelectStudent extends AppCompatActivity implements SelectAllItems{

    @BindView(R.id.btn_add)
    Button mbtn_add;

    private Student_Model student_model;

    @BindView(R.id.checkbox_student)
    public CheckBox mSelectAllCb;
    private Student_Adapter student_adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        setContentView(R.layout.activity_select_student);
        ButterKnife.bind(this);


        Toolbar toolbar = findViewById(R.id.toolbar_title);


       /* EditText edt_search = toolbar.findViewById(R.id.edt_search);
        edt_search.setHint("Search Students");*/
     TextView tv_title = toolbar.findViewById(R.id.tv_title);
      tv_title.setVisibility(View.VISIBLE);
        tv_title.setText("STUDENTS");

        SearchView searchView=toolbar.findViewById(R.id.searchView);
        searchView.setQueryHint("Search Students");
        searchView.setMaxWidth(Integer.MAX_VALUE);
        EditText searchEditText = (EditText) searchView.findViewById(androidx.appcompat.R.id.search_src_text);
        searchEditText.setTextColor(getResources().getColor(R.color.black));
        searchEditText.setHintTextColor(getResources().getColor(R.color.color_dark_grey));

        Utility.setCloseSearchIcon(searchView);

//        imv_back_arrow.setVisibility(View.GONE);
//        iv_toolbar_menu.setVisibility(View.GONE);
//        constraintLayout_menu.setVisibility(View.GONE);
        toolbar.setNavigationIcon(R.drawable.ic_back_arrow);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        RecyclerView recyclerView = findViewById(R.id.recycler_view_students);
          student_adapter = new Student_Adapter(this, student_model);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(student_adapter);

       /* mSelectAllCb.setOnCheckedChangeListener(
                (buttonView, isChecked) -> student_adapter.onSelectAll(isChecked)
        );*/

        mSelectAllCb.setOnClickListener(v -> {
           student_adapter.onSelectAll(((CheckBox)v).isChecked());
        });
        Utility.hideKeyboard(this);

    }

//    @OnClick(R.id.imv_back_arrow)
//    public void back_nav() {
//        startActivity(new Intent(SelectStudent.this, PostEventActivity.class));
//    }

    @OnClick(R.id.btn_add)
    public void btn_add() {

        if(student_adapter.isSelectedAnyStudent()){
        Toast.makeText(this, "Posted Successfully.", Toast.LENGTH_SHORT).show();
        //startActivity(new Intent(getApplicationContext(), DashboardActivity.class));
        setResult(2);
        finish();
        }else {
            Toast.makeText(this, "Please select the students.", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onSelectAll(boolean value) {
        mSelectAllCb.setChecked(value);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        //startActivity(new Intent(this,NewPostActivity.class));
       // finish();
    }
}
