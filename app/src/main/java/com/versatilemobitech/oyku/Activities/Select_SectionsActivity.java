package com.versatilemobitech.oyku.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.versatilemobitech.oyku.Adapters.Class_ExpandableListAdapter;
import com.versatilemobitech.oyku.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Select_SectionsActivity extends AppCompatActivity {

    private Class_ExpandableListAdapter listAdapter;
    private ExpandableListView expListView;
    private List<String> listDataHeader;
    private HashMap<String, List<SelectionObj>> listDataChild;

    @BindView(R.id.imv_back_arrow)
    ImageView mimv_back_arrow;

    @BindView(R.id.btn_add_class)
    Button mbtn_add_class;

    @BindView(R.id.iv_toolbar_menu)
    ImageView iv_toolbar_menu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_sections);

        ButterKnife.bind(this);

        Toolbar toolbar = findViewById(R.id.toolbar_title);
        TextView tv_title = toolbar.findViewById(R.id.tv_title);
        tv_title.setText("SELECT SECTIONS");
        iv_toolbar_menu.setVisibility(View.INVISIBLE);

        ImageView imageView = findViewById(R.id.imv_back_arrow);
        imageView.setVisibility(View.GONE);

        toolbar.setNavigationIcon(R.drawable.ic_back_arrow);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });


        expListView = (ExpandableListView) findViewById(R.id.expandable_listview);
        // preparing list data
        prepareListData();
        listAdapter = new Class_ExpandableListAdapter(this, listDataHeader, listDataChild);
        expListView.setAdapter(listAdapter);
        group_listener();
    }

    @OnClick(R.id.btn_add_class)
    public void add_class() {

        boolean isSelected=listAdapter.isSelectedAnyItem();
        if(isSelected)
        {
            setResult(2);
            Toast.makeText(this,"Posted successfully",Toast.LENGTH_LONG).show();
            finish();
        }
        else {
            Toast.makeText(this,"Please select the sections",Toast.LENGTH_LONG).show();
        }

        //startActivity(new Intent(Select_SectionsActivity.this, NewPostActivity.class));
    }


    private void group_listener() {

        // Listview Group click listener
        expListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {

            @Override
            public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
                // Toast.makeText(getApplicationContext(),
                // "Group Clicked " + listDataHeader.get(groupPosition),
                // Toast.LENGTH_SHORT).show();
                return false;
            }
        });

        // Listview Group expanded listener
        expListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {

            @Override
            public void onGroupExpand(int groupPosition) {
                Toast.makeText(getApplicationContext(), listDataHeader.get(groupPosition) + " Expanded", Toast.LENGTH_SHORT).show();
            }
        });

        // Listview Group collasped listener
        expListView.setOnGroupCollapseListener(new ExpandableListView.OnGroupCollapseListener() {

            @Override
            public void onGroupCollapse(int groupPosition) {
                Toast.makeText(getApplicationContext(), listDataHeader.get(groupPosition) + " Collapsed", Toast.LENGTH_SHORT).show();
            }
        });

        expListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                Toast.makeText(getApplicationContext(), listDataHeader.get(groupPosition) + " Collapsed", Toast.LENGTH_SHORT).show();
                return false;
            }
        });

        // Listview on child click listener
       /* expListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {

            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                // TODO Auto-generated method stub
                Toast.makeText(getApplicationContext(), listDataHeader.get(groupPosition) + " : " + listDataChild.get(listDataHeader.get(groupPosition)).get(childPosition), Toast.LENGTH_SHORT).show();
                return true;
            }
        });*/
    }


    private void prepareListData() {
        listDataHeader = new ArrayList<String>();
        listDataChild = new HashMap<String, List<SelectionObj>>();

        // Adding header data
        listDataHeader.add("I - Class");
        listDataHeader.add("II - Class");
        listDataHeader.add("III - Class");
        listDataHeader.add("IV - Class");
        listDataHeader.add("V - Class");
        listDataHeader.add("VI - Class");
        listDataHeader.add("VII - Class");

        //Adding Child data
        List<SelectionObj> class1 = new ArrayList<SelectionObj>();
        class1.add(new SelectionObj("Section 1A",false));
        class1.add(new SelectionObj("Section 2A",false));
        class1.add(new SelectionObj("Section 3A",false));

        List<SelectionObj> class2 = new ArrayList<SelectionObj>();
        class2.add(new SelectionObj("Section 1Ac",false));
        class2.add(new SelectionObj("Section 2Ac",false));
        class2.add(new SelectionObj("Section 3Ac",false));

        List<SelectionObj> class3 = new ArrayList<SelectionObj>();
        class3.add(new SelectionObj("Section 1A",false));
        class3.add(new SelectionObj("Section 2A",false));
        class3.add(new SelectionObj("Section 3A",false));


        List<SelectionObj> class4 = new ArrayList<SelectionObj>();
        class4.add(new SelectionObj("Section 1A",false));
        class4.add(new SelectionObj("Section 2A",false));
        class4.add(new SelectionObj("Section 3A",false));


        List<SelectionObj> class5 = new ArrayList<SelectionObj>();
        class5.add(new SelectionObj("Section 1A",false));
        class5.add(new SelectionObj("Section 2A",false));
        class5.add(new SelectionObj("Section 3A",false));


        List<SelectionObj> class6 = new ArrayList<SelectionObj>();
        class6.add(new SelectionObj("Section 1A",false));
        class6.add(new SelectionObj("Section 2A",false));
        class6.add(new SelectionObj("Section 3A",false));


        List<SelectionObj> class7 = new ArrayList<SelectionObj>();
        class7.add(new SelectionObj("Section 1A",false));
        class7.add(new SelectionObj("Section 2A",false));
        class7.add(new SelectionObj("Section 3A",false));

        listDataChild.put(listDataHeader.get(0), class1); // Header, Child data
        listDataChild.put(listDataHeader.get(1), class2);
        listDataChild.put(listDataHeader.get(2), class3);
        listDataChild.put(listDataHeader.get(3), class4);
        listDataChild.put(listDataHeader.get(4), class5);
        listDataChild.put(listDataHeader.get(5), class6);
        listDataChild.put(listDataHeader.get(6), class7);
    }
}


