package com.versatilemobitech.oyku.Activities;

public class SelectionObj {


    private String sectionName="";
    private boolean isSelected=false;
    public SelectionObj(String s, boolean b) {
        this.sectionName=s;
        this.isSelected=b;
    }

    public String getSectionName() {
        return sectionName;
    }

    public void setSectionName(String sectionName) {
        this.sectionName = sectionName;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

}
