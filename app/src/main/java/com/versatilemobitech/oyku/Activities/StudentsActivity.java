package com.versatilemobitech.oyku.Activities;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.DialogCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.versatilemobitech.oyku.Adapters.Student_Adapter;
import com.versatilemobitech.oyku.Model.Student_Model;
import com.versatilemobitech.oyku.R;
import com.versatilemobitech.oyku.utilities.Utility;

public class StudentsActivity extends AppCompatActivity {

    @BindView(R.id.imv_back_arrow)
    ImageView mimv_back_arrow_stu;

    @BindView(R.id.iv_toolbar_menu)
    ImageView iv_toolbar_menu;

    @BindView(R.id.btn_add_group_2)
    TextView btn_add_group_2;
    EditText edt_create_group;
    private Button dialogButton;
    private String edit_create;

//    @BindView(R.id.imv_sel_stu_checked)
//    ImageView mimv_sel_stu_checked;
//
//    @BindView(R.id.imv_sel_stu_unchecked)
//    ImageView mimv_sel_stu_unchecked;

//    @BindView(R.id.btn_add_group_2)
//    Button mbtn_add;


    private Student_Model student_model;
   private Student_Adapter student_adapter;
    private int GROP_ADDING_REQUEST=986;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_students);
        ButterKnife.bind(this);
        Toolbar toolbar = findViewById(R.id.toolbar_title);
        TextView tv_title = toolbar.findViewById(R.id.tv_title);
        tv_title.setText("STUDENTS");
        setSupportActionBar(toolbar);

        iv_toolbar_menu.setVisibility(View.GONE);

        toolbar.setNavigationIcon(R.drawable.ic_back_arrow);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        btn_add_group_2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //Condition to check
                if (student_adapter.isSelectedAnyOne()) {
                    Dialog dialog_fp = new Dialog(StudentsActivity.this, R.style.customdialog_theme);
                    dialog_fp.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog_fp.setContentView(R.layout.create_group_dialog);
                    dialog_fp.getWindow().getAttributes().windowAnimations = R.style.customdialog_theme;
                    WindowManager.LayoutParams lp_fp = new WindowManager.LayoutParams();
                    Window window_fp = dialog_fp.getWindow();
                    lp_fp.copyFrom(window_fp.getAttributes());
                    lp_fp.width = WindowManager.LayoutParams.MATCH_PARENT;
                    lp_fp.height = WindowManager.LayoutParams.MATCH_PARENT;
                    dialog_fp.setCancelable(true);
                    dialog_fp.setCanceledOnTouchOutside(true);
                    dialog_fp.getWindow().setLayout(lp_fp.width, lp_fp.height);
                    dialog_fp.show();

                    edt_create_group = dialog_fp.findViewById(R.id.edt_create_group);

                    edt_create_group.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                        @Override
                        public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                            if (actionId == EditorInfo.IME_ACTION_DONE) {
                                //performSearch();

                                Utility.hideKeyboardFrom(StudentsActivity.this,edt_create_group);
                                //dialogButton.performClick();
                                return true;
                            }
                            return false;
                        }
                    });
                    dialogButton = dialog_fp.findViewById(R.id.btn_submit_groupname);
                    dialogButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            edit_create = edt_create_group.getText().toString().trim();
                            if (edit_create.isEmpty() || edit_create.equals("")) {
                                edt_create_group.setError("Enter Group Name");
                                edt_create_group.requestFocus();
//                            Toast.makeText(StudentsActivity.this,"Enter Group Name", Toast.LENGTH_SHORT).show();
                            } else {
                                dialog_fp.dismiss();
                                Intent intent = new Intent(StudentsActivity.this, Students_AddgroupActivity.class);
                                startActivityForResult(intent,GROP_ADDING_REQUEST);
                            }
//                        Toast.makeText(getApplicationContext(), "Work in Progress", Toast.LENGTH_SHORT).show();

                            //Toast.makeText(getApplicationContext(), "Submitted", Toast.LENGTH_SHORT).show();
                        }
                    });

                    EditText edt_email = dialog_fp.findViewById(R.id.edt_create_group);
                    edt_email.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                        }
                    });
                }
                else {
                    Toast.makeText(getApplicationContext(), R.string.please_select_student, Toast.LENGTH_SHORT).show();
                }
            }
        });
//        iv_toolbar_menu.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//
//            }
//        });
//        toolbar.setNavigationIcon(R.drawable.ic_back_arrow);
//        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//            }
//        });
        RecyclerView recyclerView = findViewById(R.id.recycler_student_sel_class);
          student_adapter = new Student_Adapter(this, student_model);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(student_adapter);

    }

   /* @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.create_event_menu, menu);
        return true;
    }*/


    @OnClick(R.id.imv_back_arrow)
    public void back_nav() {
        onBackPressed();
    }

//    @OnClick(R.id.btn_add_group_2)
//    public void btn_add() {
//        startActivity(new Intent(getApplicationContext(), NewPostActivity.class));
//
//    }

//    @OnClick(R.id.imv_sel_stu_checked)
//    public void cb_checked() {
//        mimv_sel_stu_checked.setVisibility(View.GONE);
//        mimv_sel_stu_unchecked.setVisibility(View.VISIBLE);
//    }
//
//
//    @OnClick(R.id.imv_sel_stu_unchecked)
//    public void cb_unchecked() {
//        mimv_sel_stu_unchecked.setVisibility(View.GONE);
//        mimv_sel_stu_checked.setVisibility(View.VISIBLE);
//    }

    @OnClick(R.id.tv_filter)
    public void filter() {

        Dialog dialog_fp = new Dialog(this, R.style.customdialog_theme);
        //dialog_fp.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog_fp.setContentView(R.layout.filter_sections);
        //dialog_fp.getWindow().getAttributes().windowAnimations = R.style.customdialog_theme;
        WindowManager.LayoutParams lp_fp = new WindowManager.LayoutParams();
        Window window_fp = dialog_fp.getWindow();
        //lp_fp.copyFrom(window_fp.getAttributes());
        lp_fp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp_fp.height = WindowManager.LayoutParams.MATCH_PARENT;
        dialog_fp.setCancelable(true);
        dialog_fp.setCanceledOnTouchOutside(true);

        dialog_fp.getWindow().setLayout(lp_fp.width, lp_fp.height);
        dialog_fp.show();

        TextView dlg_tv_sec_a = dialog_fp.findViewById(R.id.tv_sec_a);
        dlg_tv_sec_a.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog_fp.dismiss();
                Toast.makeText(getApplicationContext(), "Section 5A Selected", Toast.LENGTH_SHORT).show();
            }
        });



        TextView dlg_tv_sec_b = dialog_fp.findViewById(R.id.tv_sec_b);
        dlg_tv_sec_b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog_fp.dismiss();
                Toast.makeText(getApplicationContext(), "Section 5B Selected", Toast.LENGTH_SHORT).show();
            }
        });

        TextView dlg_tv_sec_c = dialog_fp.findViewById(R.id.tv_sec_c);
        dlg_tv_sec_c.setOnClickListener(v -> {
            dialog_fp.dismiss();
            Toast.makeText(getApplicationContext(), "Section 5C Selected", Toast.LENGTH_SHORT).show();
        });
        TextView dlg_tv_sec_d = dialog_fp.findViewById(R.id.tv_sec_d);
        dlg_tv_sec_d.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog_fp.dismiss();
                Toast.makeText(getApplicationContext(), "Section 5D Selected", Toast.LENGTH_SHORT).show();
            }
        });
        TextView dlg_tv_sec_e = dialog_fp.findViewById(R.id.tv_sec_e);
        dlg_tv_sec_e.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog_fp.dismiss();
                Toast.makeText(getApplicationContext(), "Section 5E Selected", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode ==GROP_ADDING_REQUEST)
        {
            if(resultCode != RESULT_CANCELED)
            {
                setResult(2);
                finish();
            }
        }
    }

    /* @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.nav_create_event:
                Intent intent = new Intent(StudentsActivity.this, CreateEventActivity.class);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }*/
}
