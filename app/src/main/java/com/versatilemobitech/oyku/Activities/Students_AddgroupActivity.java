package com.versatilemobitech.oyku.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.versatilemobitech.oyku.Adapters.StudentClassListAdapter;
import com.versatilemobitech.oyku.Adapters.Student_Addgroup_Adapter;
import com.versatilemobitech.oyku.Model.Student_sel_class_Model;
import com.versatilemobitech.oyku.R;

public class Students_AddgroupActivity extends AppCompatActivity {

    private Student_sel_class_Model student_sel_class_model;
    private StudentClassListAdapter studentClassListAdapter;

    @BindView(R.id.btn_add_group_2)
    TextView mbtn_add_group_2;

    @BindView(R.id.imv_back_arrow)
    ImageView mimv_back_arrow;

    @BindView(R.id.tv_filter)
    TextView mtv_filter;
    @BindView(R.id.recycler_student_sel_class)
    RecyclerView recycler_student_class;

//    @BindView(R.id.recycler_student_class)
//    RecyclerView recycler_student_class;

    @BindView(R.id.checkbox1)
    CheckBox checkbox1;
    @BindView(R.id.checkbox2)
    CheckBox checkbox2;

    @BindView(R.id.iv_toolbar_menu)
    ImageView iv_toolbar_menu;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_students_addgroup);

        ButterKnife.bind(this);

        Toolbar toolbar = findViewById(R.id.toolbar_title);
        TextView tv_title = toolbar.findViewById(R.id.tv_title);
        mimv_back_arrow = toolbar.findViewById(R.id.imv_back_arrow);
        tv_title.setText("SELECT GROUP");
        setSupportActionBar(toolbar);
        iv_toolbar_menu.setVisibility(View.GONE);
        mimv_back_arrow.setVisibility(View.GONE);
        toolbar.setNavigationIcon(R.drawable.ic_back_arrow);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        RecyclerView recyclerView = findViewById(R.id.recycler_student_sel_class);
        Student_Addgroup_Adapter student_addgroup_adapter = new Student_Addgroup_Adapter(this, student_sel_class_model);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(student_addgroup_adapter);

//        recycler_student_class= findViewById(R.id.recycler_student_class);
//        studentClassListAdapter = new StudentClassListAdapter(Students_AddgroupActivity.this);
//        recyclerView.setHasFixedSize(true);
//        recyclerView.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL,true));
//        recyclerView.setAdapter(student_addgroup_adapter);

//        if(checkbox1.isChecked()==true)
//        {
//            Toast.makeText(this, "hello", Toast.LENGTH_SHORT).show();
////            recycler_student_class.setVisibility(View.VISIBLE);
//        }
//        if(checkbox2.isChecked()==true)
//        {
//            Toast.makeText(this, "hello", Toast.LENGTH_SHORT).show();
////            recycler_student_class.setVisibility(View.VISIBLE);
//        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.create_event_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.nav_create_event:
                Intent intent = new Intent(Students_AddgroupActivity.this, StudentsActivity.class);
                //intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @OnClick(R.id.imv_back_arrow)
    public void back_nav() {
        startActivity(new Intent(Students_AddgroupActivity.this, NewPostActivity.class));
    }

    @OnClick(R.id.btn_add_group_2)
    public void btn_addgroup() {

        if(checkbox1.isChecked() || checkbox2.isChecked()) {
            setResult(2);
            finish();
            //startActivity(new Intent(Students_AddgroupActivity.this, NewPostActivity.class));
            Toast.makeText(this, "Posted successfully", Toast.LENGTH_SHORT).show();
        }else {
            Toast.makeText(this, "Please select the group", Toast.LENGTH_SHORT).show();
        }
    }

    @OnClick(R.id.tv_filter)
    public void filter() {
        Dialog dialog_fp = new Dialog(this, R.style.customdialog_theme);
        dialog_fp.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog_fp.setContentView(R.layout.filter_sections);
        dialog_fp.getWindow().getAttributes().windowAnimations = R.style.customdialog_theme;
        WindowManager.LayoutParams lp_fp = new WindowManager.LayoutParams();
        Window window_fp = dialog_fp.getWindow();
        lp_fp.copyFrom(window_fp.getAttributes());
        lp_fp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp_fp.height = WindowManager.LayoutParams.MATCH_PARENT;
        dialog_fp.setCancelable(true);
        dialog_fp.setCanceledOnTouchOutside(true);
        dialog_fp.getWindow().setLayout(lp_fp.width, lp_fp.height);
        dialog_fp.show();

        TextView dlg_tv_sec_a = dialog_fp.findViewById(R.id.tv_sec_a);
        dlg_tv_sec_a.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog_fp.dismiss();
                Toast.makeText(getApplicationContext(), "Section 5A Selected", Toast.LENGTH_SHORT).show();
            }
        });

        TextView dlg_tv_sec_b = dialog_fp.findViewById(R.id.tv_sec_b);
        dlg_tv_sec_b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog_fp.dismiss();
                Toast.makeText(getApplicationContext(), "Section 5B Selected", Toast.LENGTH_SHORT).show();
            }
        });

        TextView dlg_tv_sec_c = dialog_fp.findViewById(R.id.tv_sec_c);
        dlg_tv_sec_c.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog_fp.dismiss();
                Toast.makeText(getApplicationContext(), "Section 5C Selected", Toast.LENGTH_SHORT).show();
            }
        });
        TextView dlg_tv_sec_d = dialog_fp.findViewById(R.id.tv_sec_d);
        dlg_tv_sec_d.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog_fp.dismiss();
                Toast.makeText(getApplicationContext(), "Section 5D Selected", Toast.LENGTH_SHORT).show();
            }
        });
        TextView dlg_tv_sec_e = dialog_fp.findViewById(R.id.tv_sec_e);
        dlg_tv_sec_e.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog_fp.dismiss();
                Toast.makeText(getApplicationContext(), "Section 5E Selected", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
