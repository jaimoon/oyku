package com.versatilemobitech.oyku.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.versatilemobitech.oyku.R;
import com.versatilemobitech.oyku.utilities.SharedPreference;

public class TermsConditionsActivity extends AppCompatActivity {

    @BindView(R.id.tv_text_tc)
    TextView tv_text_tc;

    @BindView(R.id.imv_back_arrow)
    ImageView imv_back_arrow;

    @BindView(R.id.iv_toolbar_menu)
    ImageView iv_toolbar_menu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_terms_conditions);

        ButterKnife.bind(this);

        Toolbar toolbar = findViewById(R.id.toolbar_title);
        TextView tv_title = toolbar.findViewById(R.id.tv_title);
        tv_title.setText("Terms & Conditions");
        imv_back_arrow.setVisibility(View.GONE);
        SharedPreference.setStringPreference(this,"HOMEVALUE","2");
        iv_toolbar_menu.setVisibility(View.INVISIBLE);

        toolbar.setNavigationIcon(R.drawable.ic_back_arrow);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    @OnClick(R.id.imv_back_arrow)
    public void back_nav() {

        onBackPressed();


    }
}
