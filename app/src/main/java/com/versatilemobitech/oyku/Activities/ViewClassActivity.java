package com.versatilemobitech.oyku.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import butterknife.BindView;
import butterknife.ButterKnife;

import android.os.Bundle;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.versatilemobitech.oyku.Adapters.ViewClass_ExpandableListAdapter;
import com.versatilemobitech.oyku.R;
import com.versatilemobitech.oyku.utilities.SharedPreference;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ViewClassActivity extends AppCompatActivity {

    private ViewClass_ExpandableListAdapter listAdapter;
    private ExpandableListView expListView;
    private List<String> listDataHeader;
    private HashMap<String, List<String>> listDataChild;

    @BindView(R.id.imv_back_arrow)
    ImageView mimv_back_arrow;
    ImageView iv_toolbar_menu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_class);

        Toolbar toolbar = findViewById(R.id.toolbar_title);
        TextView tv_title = toolbar.findViewById(R.id.tv_title);
        tv_title.setText("VIEW CLASSES");
        SharedPreference.setStringPreference(this, "HOMEVALUE", "2");
        iv_toolbar_menu = findViewById(R.id.iv_toolbar_menu);
        iv_toolbar_menu.setVisibility(View.INVISIBLE);
        ImageView imageView = findViewById(R.id.imv_back_arrow);
        imageView.setVisibility(View.GONE);
        toolbar.setNavigationIcon(R.drawable.ic_back_arrow);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
//        imageView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                Toast.makeText(ViewClassActivity.this, "Hello", Toast.LENGTH_SHORT).show();
//                onBackPressed();
//            }
//        });
//        imageView.setVisibility(View.GONE);

        ButterKnife.bind(this);

        expListView = (ExpandableListView) findViewById(R.id.expandable_listview_2);

        // preparing list data
        prepareListData();

        listAdapter = new ViewClass_ExpandableListAdapter(this, listDataHeader, listDataChild);
        expListView.setAdapter(listAdapter);


        // Listview Group click listener
        expListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {

            @Override
            public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
                // Toast.makeText(getApplicationContext(),
                // "Group Clicked " + listDataHeader.get(groupPosition),
                // Toast.LENGTH_SHORT).show();
                return false;
            }
        });

        // Listview Group expanded listener
        expListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {

            @Override
            public void onGroupExpand(int groupPosition) {
                Toast.makeText(getApplicationContext(), listDataHeader.get(groupPosition) + " Expanded", Toast.LENGTH_SHORT).show();
            }
        });

        // Listview Group collasped listener
        expListView.setOnGroupCollapseListener(new ExpandableListView.OnGroupCollapseListener() {

            @Override
            public void onGroupCollapse(int groupPosition) {
                Toast.makeText(getApplicationContext(), listDataHeader.get(groupPosition) + " Collapsed", Toast.LENGTH_SHORT).show();
            }
        });

        // Listview on child click listener
        expListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {

            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                // TODO Auto-generated method stub
                Toast.makeText(getApplicationContext(), listDataHeader.get(groupPosition) + " : " + listDataChild.get(listDataHeader.get(groupPosition)).get(childPosition), Toast.LENGTH_SHORT).show();
                return false;
            }
        });
    }

    private void prepareListData() {
        listDataHeader = new ArrayList<String>();
        listDataChild = new HashMap<String, List<String>>();

        // Adding header data
        listDataHeader.add("I - Class");
        listDataHeader.add("II - Class");
        listDataHeader.add("III - Class");
        listDataHeader.add("IV - Class");
        listDataHeader.add("V - Class");
        listDataHeader.add("VI - Class");
        listDataHeader.add("VII - Class");

        //Adding Child data
        List<String> class1 = new ArrayList<String>();
        class1.add("Section 1A");
        class1.add("Section 2A");
        class1.add("Section 3A");

        List<String> class2 = new ArrayList<String>();
        class2.add("Section 1A");
        class2.add("Section 2A");
        class2.add("Section 3A");

        List<String> class3 = new ArrayList<String>();
        class3.add("Section 1A");
        class3.add("Section 2A");
        class3.add("Section 3A");

        List<String> class4 = new ArrayList<String>();
        class4.add("Section 1A");
        class4.add("Section 2A");
        class4.add("Section 3A");

        List<String> class5 = new ArrayList<String>();
        class5.add("Section 1A");
        class5.add("Section 2A");
        class5.add("Section 3A");

        List<String> class6 = new ArrayList<String>();
        class6.add("Section 1A");
        class6.add("Section 2A");
        class6.add("Section 3A");

        List<String> class7 = new ArrayList<String>();
        class7.add("Section 1A");
        class7.add("Section 2A");
        class7.add("Section 3A");

        listDataChild.put(listDataHeader.get(0), class1); // Header, Child data
        listDataChild.put(listDataHeader.get(1), class2);
        listDataChild.put(listDataHeader.get(2), class3);
        listDataChild.put(listDataHeader.get(3), class4);
        listDataChild.put(listDataHeader.get(4), class5);
        listDataChild.put(listDataHeader.get(5), class6);
        listDataChild.put(listDataHeader.get(6), class7);

    }

}
