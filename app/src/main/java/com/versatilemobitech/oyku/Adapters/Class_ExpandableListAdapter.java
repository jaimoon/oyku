package com.versatilemobitech.oyku.Adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.widget.AppCompatCheckBox;

import com.versatilemobitech.oyku.Activities.SelectionObj;
import com.versatilemobitech.oyku.R;

import java.util.HashMap;
import java.util.List;

public class Class_ExpandableListAdapter extends BaseExpandableListAdapter {

    private Context _context;

    private List<String> _listDataHeader; // header titles

    // child data in format of header title, child title
    private HashMap<String, List<SelectionObj>> _listDataChild;

    public Class_ExpandableListAdapter(Context context, List<String> listDataHeader, HashMap<String, List<SelectionObj>> listChildData) {
        this._context = context;
        this._listDataHeader = listDataHeader;
        this._listDataChild = listChildData;
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return this._listDataChild.get(this._listDataHeader.get(groupPosition))
                .get(childPosition).getSectionName();
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(final int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        final String childText = (String) getChild(groupPosition, childPosition) +_listDataHeader.get(groupPosition);

        final String headerId = _listDataHeader.get(groupPosition);
        ViewHolder viewHolder;
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.exp_list_item, null,false);

            viewHolder=new ViewHolder();
            viewHolder.checkBox=convertView.findViewById(R.id.check_section);
            viewHolder.txtListChild= (TextView) convertView.findViewById(R.id.el_list_item);
            viewHolder.checkBox.setTag(groupPosition);
           // Log.d("TEST","Group name set :"+groupPosition);

           /* viewHolder.checkBox.setOnCheckedChangeListener((buttonView, isChecked) -> {
                _listDataChild.get( _listDataHeader.get(2))
                        .get(childPosition).setSelected(isChecked);

                finalViewHolder.txtListChild.setText( finalViewHolder.txtListChild.getText().toString() +""+isChecked);

                Log.d("TEST","Actual chaild Group id" + finalViewHolder.checkBox.getTag() + " Name :"+ _listDataHeader.size());

                Log.d("TEST","child name"+_listDataHeader.get(groupPosition) + "; "+_listDataChild.get( _listDataHeader.get(groupPosition)).get(childPosition).getSectionName());
            });*/


            convertView.setTag(viewHolder);
        }
        else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

       /* viewHolder.checkBox.setOnClickListener(v -> {

            Log.d("TEST","TAG"+v.getTag() +"value :"+headerId);
            _listDataChild.get( headerId)
                    .get(1).setSelected(true);
        });*/

        viewHolder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                //Log.d("TEST","TAG"+"value :"+headerId);
                _listDataChild.get( headerId)
                        .get(childPosition).setSelected(isChecked);
            }
        });

if(this._listDataChild.get(this._listDataHeader.get(groupPosition))
        .get(childPosition).isSelected()) {
    viewHolder.checkBox.setChecked(true);

}else {
    viewHolder.checkBox.setChecked(false);

}


      /*  ImageView imv_checked = convertView.findViewById(R.id.checkbox_checked);
        ImageView imv_unchecked = convertView.findViewById(R.id.checkbox_unchecked);

        imv_checked.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imv_checked.setVisibility(View.GONE);
                imv_unchecked.setVisibility(View.VISIBLE);
            }
        });

        imv_unchecked.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imv_unchecked.setVisibility(View.GONE);
                imv_checked.setVisibility(View.VISIBLE);
            }
        });*/

        viewHolder.txtListChild.setText(childText);
        return convertView;
    }

    class ViewHolder {
        public  TextView txtListChild;
        public AppCompatCheckBox  checkBox;
    }


    @Override
    public int getGroupCount() {
        return this._listDataHeader.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return this._listDataChild.get(this._listDataHeader.get(groupPosition)).size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return this._listDataHeader.get(groupPosition);
    }


    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }


    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(final int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        String headerTitle = (String) getGroup(groupPosition);

        ViewHolderGroup viewHolderGroup=null;
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.exp_list_header, null);

            viewHolderGroup=new ViewHolderGroup();

            viewHolderGroup. lblListHeader = (TextView) convertView
                    .findViewById(R.id.el_header_item);
            viewHolderGroup.lblListHeader.setTypeface(null, Typeface.BOLD);
            convertView.setTag(viewHolderGroup);
        }
        else {
            viewHolderGroup= (ViewHolderGroup) convertView.getTag();
        }


        viewHolderGroup.lblListHeader.setText(headerTitle);

        return convertView;
    }

    class ViewHolderGroup
    {
        public  TextView lblListHeader;

    }

    public boolean isSelectedAnyItem()
    {
        boolean result=false;


        Log.d("TEST","result "+_listDataChild.size());

        for(int i=0;i<_listDataHeader.size();i++)
        {
            List<SelectionObj> selectionList = _listDataChild.get(_listDataHeader.get(i));
            for(int j=0;j<selectionList.size();j++) {

                if(selectionList.get(j).isSelected())
                {
                    return true;
                }
            }


        }
        return result;
    }



    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}
