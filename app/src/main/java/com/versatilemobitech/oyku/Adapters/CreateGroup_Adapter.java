package com.versatilemobitech.oyku.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.versatilemobitech.oyku.Activities.CreateGroupActivity;
import com.versatilemobitech.oyku.Model.CreateGroup_Model;
import com.versatilemobitech.oyku.R;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import de.hdodenhof.circleimageview.CircleImageView;

public class CreateGroup_Adapter extends RecyclerView.Adapter<CreateGroup_Adapter.ViewHolder> {

    private Context context;
    private CreateGroup_Model createGroup_model;

    public CreateGroup_Adapter(Context context, CreateGroup_Model createEvent_model) {
        this.context = context;
        this.createGroup_model = createEvent_model;
    }

    @NonNull
    @Override
    public CreateGroup_Adapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater mlayoutInflater = LayoutInflater.from(parent.getContext());
        View view = mlayoutInflater.inflate(R.layout.recycler_create_group, parent, false);
        CreateGroup_Adapter.ViewHolder mviewHolder = new CreateGroup_Adapter.ViewHolder(view);
        return mviewHolder;

    }

    @Override
    public void onBindViewHolder(@NonNull CreateGroup_Adapter.ViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return 4;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ImageView mcb_checked,mcb_unchecked;
        TextView mtv_tit,mtv_sub_tit;
        CircleImageView mciv_image;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            mcb_checked = itemView.findViewById(R.id.cb_checked);
            mcb_unchecked = itemView.findViewById(R.id.cb_unchecked);
            mtv_tit = itemView.findViewById(R.id.tv_tit);
            mtv_sub_tit = itemView.findViewById(R.id.tv_sub_tit);
            mciv_image = itemView.findViewById(R.id.civ_image_student);

            mcb_unchecked.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mcb_unchecked.setVisibility(View.GONE);
                    mcb_checked.setVisibility(View.VISIBLE);
                }
            });
            mcb_checked.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mcb_checked.setVisibility(View.GONE);
                    mcb_unchecked.setVisibility(View.VISIBLE);
                }
            });
        }
    }
}
