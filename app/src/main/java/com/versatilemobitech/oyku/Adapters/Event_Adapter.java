package com.versatilemobitech.oyku.Adapters;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.versatilemobitech.oyku.Activities.NewPostActivity;
import com.versatilemobitech.oyku.Model.Event_Model;
import com.versatilemobitech.oyku.R;
import com.versatilemobitech.oyku.interfaces.EventInterface;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import me.relex.circleindicator.CircleIndicator;

import static com.facebook.FacebookSdk.getApplicationContext;

public class Event_Adapter extends RecyclerView.Adapter<Event_Adapter.ViewHolder> {

    Event_Model event_model;
    Context context;
    EventInterface eventInterface;
    private ViewPager viewPager;
    private CircleIndicator circleIndicator;
    private MyPager myPager;
    public Event_Adapter(Context context,Event_Model event_model,EventInterface eventInterface1){
        this.context = context;
        this.event_model = event_model;
        eventInterface=eventInterface1;

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater mlayoutInflater = LayoutInflater.from(parent.getContext());
        View view = mlayoutInflater.inflate(R.layout.recycler_events, parent, false);
        Event_Adapter.ViewHolder mviewHolder = new Event_Adapter.ViewHolder(view);
        return mviewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return 8;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView tv_tv_annual_day_event, tv_date_time, tv_text_event;
        private ImageView imv_pen, imv_event_pic,imv_event_pic_2,imageView_edit;
        private TextView tv_tv_annual_day_event_2, tv_date_time_2, tv_text_event_2;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            tv_tv_annual_day_event = itemView.findViewById(R.id.tv_annual_day_event);
            tv_date_time = itemView.findViewById(R.id.tv_time_date);
            tv_text_event = itemView.findViewById(R.id.tv_text_event);
            imv_event_pic = itemView.findViewById(R.id.imv_event_pic);
            imageView_edit=itemView.findViewById(R.id.imageView_edit);

            myPager = new MyPager(context);
            viewPager = itemView.findViewById(R.id.view_pager);
            viewPager.setAdapter(myPager);
            circleIndicator = itemView.findViewById(R.id.circle);
            circleIndicator.setViewPager(viewPager);

            /*imv_pen.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, CreateEvent_2Activity.class);
                    context.startActivity(intent);
                }
            });*/

            imageView_edit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    eventInterface.eventInterface();

                }
            });
        }
    }


}
