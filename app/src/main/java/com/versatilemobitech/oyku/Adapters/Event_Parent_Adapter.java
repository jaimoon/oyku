package com.versatilemobitech.oyku.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;

import com.versatilemobitech.oyku.Model.Event_Parent_Model;
import com.versatilemobitech.oyku.R;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class Event_Parent_Adapter extends RecyclerView.Adapter<Event_Parent_Adapter.ViewHolder> {

    Event_Parent_Model event_parent_model;
    Context context;

    public Event_Parent_Adapter(Context context, Event_Parent_Model event_parent_model){
        this.context = context;
        this.event_parent_model = event_parent_model;

    }

    @NonNull
    @Override
    public Event_Parent_Adapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        LayoutInflater mlayoutInflater = LayoutInflater.from(parent.getContext());
        View view = mlayoutInflater.inflate(R.layout.recycler_events_parents, parent, false);
        Event_Parent_Adapter.ViewHolder mviewHolder = new Event_Parent_Adapter.ViewHolder(view);
        return mviewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull Event_Parent_Adapter.ViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return 5;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView tv_annual_day_event, tv_date_time, tv_text_event;
        private ImageView imv_pen, imv_event_pic,imv_event_pic_2;
        private TextView tv_independence_day_event_2, tv_date_time_2, tv_text_event_2;
        private ScrollView scrl_view_events;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            scrl_view_events = itemView.findViewById(R.id.scrlvw_events_parents);
            tv_annual_day_event = itemView.findViewById(R.id.tv_annual_day_event_prt);
            tv_date_time = itemView.findViewById(R.id.tv_time_date_prt);
            tv_text_event = itemView.findViewById(R.id.tv_text_event_prt);
            tv_independence_day_event_2 = itemView.findViewById(R.id.tv_independence_day_event_prt_2);
            tv_text_event_2 = itemView.findViewById(R.id.tv_text_event_prt_2);
            imv_event_pic = itemView.findViewById(R.id.imv_event_prt);
            imv_event_pic_2 = itemView.findViewById(R.id.imv_event_prt_2);

            /*imv_pen.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, CreateEvent_2Activity.class);
                    context.startActivity(intent);
                }
            });*/
        }
    }
}
