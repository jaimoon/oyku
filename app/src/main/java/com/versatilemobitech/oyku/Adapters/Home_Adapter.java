package com.versatilemobitech.oyku.Adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.versatilemobitech.oyku.Activities.CommentsActivity;
import com.versatilemobitech.oyku.Activities.LikesActivity;
import com.versatilemobitech.oyku.Model.Home_Model;
import com.versatilemobitech.oyku.R;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import de.hdodenhof.circleimageview.CircleImageView;

public class Home_Adapter extends RecyclerView.Adapter<Home_Adapter.ViewHolder> {

    Home_Model homeModel;
    Context context;

    public Home_Adapter(Context context, Home_Model homeModel) {
        this.context = context;
        this.homeModel = homeModel;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        LayoutInflater mlayoutInflater = LayoutInflater.from(parent.getContext());
        View view = mlayoutInflater.inflate(R.layout.recycler_home, parent, false);
        Home_Adapter.ViewHolder mviewHolder = new Home_Adapter.ViewHolder(view);
        return mviewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {


    }

    @Override
    public int getItemCount() {
        return 8;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private ScrollView scrollview_homefragment;

        private CircleImageView civ_profle_1;
        private CircleImageView civ_profle_2;
        private ImageView imv_image, imv_likes, imv_comments;
        private ImageView imv_image_2, imv_likes_2, imv_comments_2;
        private TextView tv_person_name, tv_person_subject, tv_posted_day, tv_likes, tv_comments, tv_schl_memories;
      // private TextView tv_person_name_2, tv_person_subject_2, tv_posted_day_2, tv_likes_2, tv_comments_2, tv_schl_memories_2;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            civ_profle_1 = itemView.findViewById(R.id.fab_profile_pic);


            imv_image = itemView.findViewById(R.id.imv_image);


            imv_likes = itemView.findViewById(R.id.imv_likes);


            imv_comments = itemView.findViewById(R.id.imv_comments);


            tv_person_name = itemView.findViewById(R.id.tv_person_name);

            tv_person_subject = itemView.findViewById(R.id.tv_person_subject);

            tv_posted_day = itemView.findViewById(R.id.tv_posted_day);

            tv_likes = itemView.findViewById(R.id.tv_likes);

            tv_comments = itemView.findViewById(R.id.tv_comments);

            tv_comments.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    context.startActivity(new Intent(context, CommentsActivity.class));
                }
            });

            tv_likes. setOnClickListener(v->{
                Intent intent=new Intent(context, LikesActivity.class);
                context.startActivity(intent);
            });

        }
    }
}
