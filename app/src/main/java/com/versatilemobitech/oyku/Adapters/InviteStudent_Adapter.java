package com.versatilemobitech.oyku.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ScrollView;

import com.versatilemobitech.oyku.Model.InviteStudent_Model;
import com.versatilemobitech.oyku.R;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class InviteStudent_Adapter extends RecyclerView.Adapter<InviteStudent_Adapter.ViewHolder> {

    private InviteStudent_Model inviteStudent_model;
    Context context;

    public InviteStudent_Adapter(Context context, InviteStudent_Model inviteStudent_model) {
        this.context = context;
        this.inviteStudent_model = inviteStudent_model;
    }

    @NonNull
    @Override
    public InviteStudent_Adapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        LayoutInflater mlayoutInflater = LayoutInflater.from(parent.getContext());
        View view = mlayoutInflater.inflate(R.layout.recycler_invite_student, parent, false);
        InviteStudent_Adapter.ViewHolder mviewHolder = new InviteStudent_Adapter.ViewHolder(view);
        return mviewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull InviteStudent_Adapter.ViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return 16;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private ScrollView mscrollView_inv_stud;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            mscrollView_inv_stud = itemView.findViewById(R.id.scrollview_invite_student);
        }
    }
}
