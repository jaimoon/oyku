package com.versatilemobitech.oyku.Adapters;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.ThumbnailUtils;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.versatilemobitech.oyku.Model.MediaDetailsModel;
import com.versatilemobitech.oyku.R;
import com.versatilemobitech.oyku.interfaces.OnSelectMediaItem;
import com.versatilemobitech.oyku.utilities.Constants;

import java.io.File;
import java.util.ArrayList;

public class MediaListAdapter extends RecyclerView.Adapter<MediaListAdapter.MyViewHolder> {

    private Activity mActivity;
    private ArrayList<MediaDetailsModel> mMediaDetailsModelArrayList;
    private String mMediaType;
    private LayoutInflater mLayoutInflater;
    private OnSelectMediaItem mOnSelectMediaItem;

    public MediaListAdapter(Activity activity, OnSelectMediaItem onSelectMediaItem, ArrayList<MediaDetailsModel> mediaDetailsModelArrayList, String mediaType) {
        mActivity = activity;
        mOnSelectMediaItem = onSelectMediaItem;
        mMediaDetailsModelArrayList = mediaDetailsModelArrayList;
        mMediaType = mediaType;
        mLayoutInflater = LayoutInflater.from(mActivity);
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = mLayoutInflater.inflate(R.layout.item_media_list, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        final MediaDetailsModel mediaDetailsModel = mMediaDetailsModelArrayList.get(position);
        holder.imageName.setText(mediaDetailsModel.getName());
        Bitmap bitmap = null;
        if (mMediaType.equalsIgnoreCase(Constants.VIDEO)) {
            holder.videoThumb.setVisibility(View.VISIBLE);
            bitmap = ThumbnailUtils.createVideoThumbnail(mediaDetailsModel.getPath(), MediaStore.Video.Thumbnails.MICRO_KIND);
        } else {
            holder.videoThumb.setVisibility(View.GONE);
            File imgFile = new File(mediaDetailsModel.getPath());
            if (imgFile.exists()) {
                bitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
            }
        }

        holder.imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mOnSelectMediaItem.selectedItem(mediaDetailsModel,mMediaType);
            }
        });

        Glide.with(mActivity).load(bitmap).error(R.drawable.ic_error).into(holder.imageView);
    }

    @Override
    public int getItemCount() {
        return mMediaDetailsModelArrayList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView,videoThumb;
        TextView imageName;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.imagePreview);
            videoThumb = itemView.findViewById(R.id.videoThumb);
            imageName = itemView.findViewById(R.id.imageName);
        }
    }

}
