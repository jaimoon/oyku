package com.versatilemobitech.oyku.Adapters;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.versatilemobitech.oyku.Fragments.mediapicker.CameraFragment;
import com.versatilemobitech.oyku.Fragments.mediapicker.GalleryFragment;
import com.versatilemobitech.oyku.Fragments.mediapicker.VideoFragment;
import com.versatilemobitech.oyku.R;

public class MediaPickerPagerAdapter extends FragmentPagerAdapter {

    @StringRes
    private static final int[] TAB_TITLES =
            new int[]{R.string.str_gallery, R.string.str_photo, R.string.str_video};

    private final Context mContext;

    public MediaPickerPagerAdapter(Context mContext, FragmentManager fragmentManager) {
        super(fragmentManager);
        this.mContext = mContext;
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return GalleryFragment.newInstance();
            case 1:
                return CameraFragment.newInstance();
            case 2:
                return VideoFragment.newInstance();
        }
        return null;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return mContext.getResources().getString(TAB_TITLES[position]);
    }

    @Override
    public int getCount() {
        return TAB_TITLES.length;
    }
}
