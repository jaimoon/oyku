package com.versatilemobitech.oyku.Adapters;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.versatilemobitech.oyku.Model.MediaModel;
import com.versatilemobitech.oyku.R;
import com.versatilemobitech.oyku.utilities.Constants;

import java.util.List;

public class MediaSpinnerAdapter extends ArrayAdapter<MediaModel> {

    LayoutInflater mLayoutInflater;

    public MediaSpinnerAdapter(@NonNull Activity context, int resource, int textViewResourceId, @NonNull List<MediaModel> objects) {
        super(context, resource, textViewResourceId, objects);
        mLayoutInflater = context.getLayoutInflater();
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return getView(position,convertView,parent);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        MediaModel rowItem = getItem(position);

        View rowview = mLayoutInflater.inflate(R.layout.item_albums,null,true);

        TextView txtTitle = (TextView) rowview.findViewById(R.id.albumName);
        txtTitle.setText(rowItem.getFolderName());

        ImageView mediaTypeIv = rowview.findViewById(R.id.mediaTypeIv);
        if (rowItem.getType().equalsIgnoreCase(Constants.VIDEO)){
            mediaTypeIv.setImageDrawable(getContext().getDrawable(R.drawable.ic_video));
        } else {
            mediaTypeIv.setImageDrawable(getContext().getDrawable(R.drawable.ic_image));
        }

        return rowview;
    }
}
