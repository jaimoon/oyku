package com.versatilemobitech.oyku.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ScrollView;

import com.versatilemobitech.oyku.Model.Memories_Model;
import com.versatilemobitech.oyku.R;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class Memories_Adapter extends RecyclerView.Adapter<Memories_Adapter.ViewHolder> {

    Context context;
    private Memories_Model memories_model;

    public Memories_Adapter(Context context , Memories_Model memories_model) {
        this.context = context;
        this.memories_model = memories_model;
    }

    @NonNull
    @Override
    public Memories_Adapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        LayoutInflater mlayoutInflater = LayoutInflater.from(parent.getContext());
        View view = mlayoutInflater.inflate(R.layout.recycler_memories, parent, false);
        Memories_Adapter.ViewHolder mviewHolder = new Memories_Adapter.ViewHolder(view);
        return mviewHolder;

    }

    @Override
    public void onBindViewHolder(@NonNull Memories_Adapter.ViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return 2;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ScrollView scrollView_mem;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            scrollView_mem = itemView.findViewById(R.id.scrollview_memories);
        }
    }
}
