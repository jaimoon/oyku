package com.versatilemobitech.oyku.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;

import com.versatilemobitech.oyku.Model.CreateEvent_Model;
import com.versatilemobitech.oyku.R;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import de.hdodenhof.circleimageview.CircleImageView;

public class PostEvent_Adapter extends RecyclerView.Adapter<PostEvent_Adapter.ViewHolder> {

    private CreateEvent_Model createEvent_model;
    private Context context;

    public PostEvent_Adapter(Context context, CreateEvent_Model createEvent_model) {

        this.context = context;
        this.createEvent_model = createEvent_model;
    }

    @NonNull
    @Override
    public PostEvent_Adapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        LayoutInflater mlayoutInflater = LayoutInflater.from(parent.getContext());
        View view = mlayoutInflater.inflate(R.layout.recycler_postevent, parent, false);
        PostEvent_Adapter.ViewHolder mviewHolder = new PostEvent_Adapter.ViewHolder(view);
        return mviewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull PostEvent_Adapter.ViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return 5;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView mtv_parents_ce, mtv_section_ce;
        private CircleImageView mciv_profile_pic_ce;
        private ImageView mimv_rb_checked,mimv_rb_unchecked;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            mtv_parents_ce = itemView.findViewById(R.id.tv_parent_ce);
            mtv_section_ce = itemView.findViewById(R.id.tv_section_ce);
            mciv_profile_pic_ce = itemView.findViewById(R.id.civ_profile_pic_ce);
            mimv_rb_checked = itemView.findViewById(R.id.imv_rb_checked);
            mimv_rb_unchecked = itemView.findViewById(R.id.imv_rb_unchecked);

            mimv_rb_checked.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mimv_rb_checked.setVisibility(View.GONE);
                    mimv_rb_unchecked.setVisibility(View.VISIBLE);
                }
            });

            mimv_rb_unchecked.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mimv_rb_unchecked.setVisibility(View.GONE);
                    mimv_rb_checked.setVisibility(View.VISIBLE);
                }
            });
        }

    }
}
