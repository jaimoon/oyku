package com.versatilemobitech.oyku.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.versatilemobitech.oyku.Activities.Students_AddgroupActivity;
import com.versatilemobitech.oyku.R;

import de.hdodenhof.circleimageview.CircleImageView;

public class StudentClassListAdapter extends RecyclerView.Adapter<StudentClassListAdapter.ViewHolder> {

    private Context ctx;

    public StudentClassListAdapter(Context context) {


        ctx=context;
    }

    @NonNull
    @Override
    public StudentClassListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        LayoutInflater mlayoutInflater = LayoutInflater.from(parent.getContext());
        View view = mlayoutInflater.inflate(R.layout.class_type_list, parent, false);
        StudentClassListAdapter.ViewHolder mviewHolder = new StudentClassListAdapter.ViewHolder(view);
        return mviewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return 1;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView mtv_1, mtv_2, mtv_3;
        private CircleImageView circleImageView;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

//            circleImageView = itemView.findViewById(R.id.civ_prfle_pic);
//            mtv_1 = itemView.findViewById(R.id.tv_1);
//            mtv_2 = itemView.findViewById(R.id.tv_2);
//            mtv_3 = itemView.findViewById(R.id.tv_3);
        }
    }
}
