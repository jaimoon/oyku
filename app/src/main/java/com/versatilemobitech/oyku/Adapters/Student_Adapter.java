package com.versatilemobitech.oyku.Adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;

import com.versatilemobitech.oyku.Activities.SelectStudent;
import com.versatilemobitech.oyku.Activities.StudentsActivity;
import com.versatilemobitech.oyku.Model.Student_Model;
import com.versatilemobitech.oyku.R;
import com.versatilemobitech.oyku.interfaces.SelectAllItems;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class Student_Adapter extends RecyclerView.Adapter<Student_Adapter.ViewHolder> implements SelectAllItems {

    private Student_Model student_model;
    private Context context;
    private ViewHolder mViewHolder;
    private boolean isSelectedAll;
    private int mSelectedItemsSize;
    private int listCount = 5;

    List<ViewHolder> listHolder=new ArrayList<ViewHolder>();

    public Student_Adapter(Context context, Student_Model student_model) {
        this.context = context;
        this.student_model = student_model;
        Log.d("TEST","Student_Adapter " + listHolder.size());
    }

    @NonNull
    @Override
    public Student_Adapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        LayoutInflater mlayoutInflater = LayoutInflater.from(parent.getContext());
        View view = mlayoutInflater.inflate(R.layout.recycler_students, parent, false);
        Student_Adapter.ViewHolder mviewHolder = new Student_Adapter.ViewHolder(view);
        return mviewHolder;

    }


    @Override
    public void onBindViewHolder(@NonNull Student_Adapter.ViewHolder holder, int position) {
        mViewHolder = holder;
        listHolder.add(mViewHolder);
        mViewHolder.student_cb.setChecked(isSelectedAll);
        mViewHolder.student_cb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                Log.d("TEST","selected "+isChecked + listHolder.size());


                    buttonView.post(new Runnable() {
                        @Override
                        public void run() {

                            boolean allAreChecked=false;
                            for (int i=0;i<listHolder.size();i++)
                            {
                                if(listHolder.get(i).student_cb.isChecked())
                                {
                                    allAreChecked = true;
                                }else{
                                    allAreChecked = false;
                                    break;
                                }

                            }

                            if (context instanceof SelectStudent) {
   ((SelectStudent) context).mSelectAllCb.setChecked(allAreChecked);
                            }
                            if (context instanceof StudentsActivity) {
                               // ((StudentsActivity) context).mSelectAllCb.setChecked(allAreChecked);
                            }

                        }
                    });


               // ( (SelectStudent)  context ).mSelectAllCb.invalidate();
            }
        });
    }

    @Override
    public int getItemCount() {
        return listCount;
    }

    @Override
    public void onSelectAll(boolean isChecked) {
        isSelectAll(isChecked);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        CheckBox student_cb;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            student_cb = itemView.findViewById(R.id.student_cb);

        }
    }

    public boolean isSelectedAnyOne()
    {
        boolean allAreChecked=false;
        if (context instanceof StudentsActivity) {


            for (int i=0;i<listHolder.size();i++)
            {
                if(listHolder.get(i).student_cb.isChecked())
                {
                    allAreChecked = true;
                    break;
                }else{
                    allAreChecked = false;

                }

            }

        }
        return allAreChecked;
    }

    public boolean isSelectedAnyStudent()
    {
        boolean allAreChecked=false;



            for (int i=0;i<listHolder.size();i++)
            {
                if(listHolder.get(i).student_cb.isChecked())
                {
                    allAreChecked = true;
                    break;
                }else{
                    allAreChecked = false;

                }

            }


        return allAreChecked;
    }

    public void isSelectAll(boolean isCheckedAll) {
        isSelectedAll = isCheckedAll;
        mSelectedItemsSize = listCount;
        listHolder.clear();
        notifyDataSetChanged();
    }

}
