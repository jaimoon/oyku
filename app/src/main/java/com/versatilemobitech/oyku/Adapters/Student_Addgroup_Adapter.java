package com.versatilemobitech.oyku.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.versatilemobitech.oyku.Model.Student_sel_class_Model;
import com.versatilemobitech.oyku.R;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import de.hdodenhof.circleimageview.CircleImageView;

public class Student_Addgroup_Adapter extends RecyclerView.Adapter<Student_Addgroup_Adapter.ViewHolder> {

    private Student_sel_class_Model student_sel_class_model;
    Context context;

    public Student_Addgroup_Adapter(Context context, Student_sel_class_Model student_sel_class_model) {
        this.context = context;
        this.student_sel_class_model = student_sel_class_model;
    }

    @NonNull
    @Override
    public Student_Addgroup_Adapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        LayoutInflater mlayoutInflater = LayoutInflater.from(parent.getContext());
        View view = mlayoutInflater.inflate(R.layout.recycler_students_sel_class, parent, false);
        Student_Addgroup_Adapter.ViewHolder mviewHolder = new Student_Addgroup_Adapter.ViewHolder(view);
        return mviewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull Student_Addgroup_Adapter.ViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return 5;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView mtv_1, mtv_2, mtv_3;
        private CircleImageView circleImageView;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            circleImageView = itemView.findViewById(R.id.civ_prfle_pic);
            mtv_1 = itemView.findViewById(R.id.tv_1);
            mtv_2 = itemView.findViewById(R.id.tv_2);
            mtv_3 = itemView.findViewById(R.id.tv_3);
        }
    }
}
