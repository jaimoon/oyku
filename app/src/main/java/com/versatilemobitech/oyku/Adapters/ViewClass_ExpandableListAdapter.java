package com.versatilemobitech.oyku.Adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.versatilemobitech.oyku.R;

import java.util.HashMap;
import java.util.List;

public class ViewClass_ExpandableListAdapter extends BaseExpandableListAdapter {

    private Context _context;

    private List<String> _listDataHeader; // header titles

    // child data in format of header title, child title
    private HashMap<String, List<String>> _listDataChild;

    public ViewClass_ExpandableListAdapter(Context context, List<String> listDataHeader, HashMap<String, List<String>> listChildData) {
        this._context = context;
        this._listDataHeader = listDataHeader;
        this._listDataChild = listChildData;
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return this._listDataChild.get(this._listDataHeader.get(groupPosition))
                .get(childPosition);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        final String childText = (String) getChild(groupPosition, childPosition);

        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.exp_list_item_2, null);
        }

        TextView txtListChild = (TextView) convertView
                .findViewById(R.id.el_list_item_2);

        txtListChild.setText(childText);
        return convertView;
    }

    @Override
    public int getGroupCount() {
        return this._listDataHeader.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return this._listDataChild.get(this._listDataHeader.get(groupPosition)).size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return this._listDataHeader.get(groupPosition);
    }


    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }


    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        String headerTitle = (String) getGroup(groupPosition);
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.exp_list_header_2, null);
        }

        TextView lblListHeader = (TextView) convertView.findViewById(R.id.el_header_item_2);

        ImageView mimv_cb_check = convertView.findViewById(R.id.imv_cb_check);
        ImageView mimv_cb_uncheck = convertView.findViewById(R.id.imv_cb_uncheck);
        mimv_cb_check.setVisibility(View.GONE);
        mimv_cb_check.setVisibility(View.GONE);
        mimv_cb_check.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mimv_cb_check.setVisibility(View.GONE);
                mimv_cb_uncheck.setVisibility(View.VISIBLE);
            }
        }); mimv_cb_uncheck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mimv_cb_uncheck.setVisibility(View.GONE);
                mimv_cb_check.setVisibility(View.VISIBLE);
            }
        });

        lblListHeader.setTypeface(null, Typeface.BOLD);
        lblListHeader.setText(headerTitle);

        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}

