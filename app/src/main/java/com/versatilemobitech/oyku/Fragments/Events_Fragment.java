package com.versatilemobitech.oyku.Fragments;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.media.Image;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.versatilemobitech.oyku.Activities.CreateEventActivity;
import com.versatilemobitech.oyku.Activities.DashboardActivity;
import com.versatilemobitech.oyku.Activities.NewPostActivity;
import com.versatilemobitech.oyku.Adapters.Event_Adapter;
import com.versatilemobitech.oyku.Model.Event_Model;
import com.versatilemobitech.oyku.R;
import com.versatilemobitech.oyku.interfaces.EventInterface;
import com.versatilemobitech.oyku.utilities.Constants;
import com.versatilemobitech.oyku.utilities.SharedPreference;
import com.versatilemobitech.oyku.utilities.Utility;

import java.lang.reflect.Field;

public class Events_Fragment extends Fragment implements EventInterface{

    private Event_Model event_model;
    private ImageView mimv_back_arrow;
    private CardView mcd_event_imv;
    private ImageView iv_toolbar_menu;
    private EventInterface eventInterface;

    public static final int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 123;
    private static final int MY_CAMERA_PERMISSION_CODE = 100;
    private static final String IMAGE_DIRECTORY = "/demonuts";
    private int GALLERY = 1, CAMERA = 2;
    public static final int PICK_IMAGE_REQUEST = 1;
    String userChoosenTask;
    Uri imageUri;


    public Events_Fragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_events, container, false);

        eventInterface=this::eventInterface;

        ((DashboardActivity)getActivity()).getBottomIconsChange("event");

        SharedPreference.setStringPreference(getActivity(),"HOMEVALUE","2");
        Toolbar toolbar = view.findViewById(R.id.toolbar_title);
        TextView tv_title = toolbar.findViewById(R.id.tv_title);
        tv_title.setText("Events");

        RecyclerView recyclerView = view.findViewById(R.id.recycler_view_event);
        Event_Adapter event_adapter = new Event_Adapter(getContext(), event_model,eventInterface);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(event_adapter);
        mimv_back_arrow = view.findViewById(R.id.imv_back_arrow);
        mcd_event_imv = view.findViewById(R.id.cd_event_imv);
        iv_toolbar_menu=view.findViewById(R.id.iv_toolbar_menu);
        iv_toolbar_menu.setVisibility(View.INVISIBLE);
//        SharedPreference.setStringPreference(getActivity(),"HOMEVALUE","2");

        toolbar.setNavigationIcon(R.drawable.ic_back_arrow);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });
        mimv_back_arrow = view.findViewById(R.id.imv_back_arrow);
        mimv_back_arrow.setVisibility(View.GONE);
//        mimv_back_arrow.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                getActivity().onBackPressed();
//            }
//        });
        mcd_event_imv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openCreateEditEvent(true);
            }
        });

        SearchView searchView=view.findViewById(R.id.searchView);
        searchView.setMaxWidth(Integer.MAX_VALUE);
        EditText searchEditText = (EditText) searchView.findViewById(androidx.appcompat.R.id.search_src_text);
        searchEditText.setTextColor(getResources().getColor(R.color.black));
        searchEditText.setHintTextColor(getResources().getColor(R.color.color_dark_grey));

        Utility.setCloseSearchIcon(searchView);
        return view;
    }



    private void openCreateEditEvent(boolean isNew){
        Intent eventsIntent = new Intent(getActivity(), CreateEventActivity.class);
        eventsIntent.putExtra(Constants.IS_NEW,isNew);
        startActivityForResult(eventsIntent,77);
    }

    @Override
    public void eventInterface() {

        openCreateEditEvent(false);
//        selectImage();
    }

    private void selectImage() {

        try {
            if (ActivityCompat.checkSelfPermission(getActivity(),
                    Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(getActivity(),
                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,
                                Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        MY_CAMERA_PERMISSION_CODE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        final CharSequence[] options = {"Take Photo", "Choose from Gallery", "Cancel"};

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Choose Picture");
        builder.setCancelable(false);
        builder.setItems(options, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int item) {
                switch (item) {
                    case 0:
                        takePhotoFromCamera();
                        break;
                    case 1:
                        choosePhotoFromGallary();
                        break;
                    case 2:
                        builder.setCancelable(true);
                        startActivity(new Intent(getActivity(),NewPostActivity.class));
                        break;
                }
            }
        });
        builder.show();
    }

    private void takePhotoFromCamera() {
        Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, CAMERA);

    }

    private void choosePhotoFromGallary() {
        Intent galleryIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(galleryIntent, GALLERY);

        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("image/*");
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                           int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (userChoosenTask.equals("Take Photo"))
                        takePhotoFromCamera();

                    else if (userChoosenTask.equals("Choose from Library"))
                        choosePhotoFromGallary();
                } else {
                    //code for deny
                }
                break;
        }
    }


}
