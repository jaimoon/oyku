package com.versatilemobitech.oyku.Fragments;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.versatilemobitech.oyku.Activities.CreateEventActivity;
import com.versatilemobitech.oyku.Activities.DashboardActivity;
import com.versatilemobitech.oyku.Activities.Dashboard_ParentsActivity;
import com.versatilemobitech.oyku.Adapters.Event_Adapter;
import com.versatilemobitech.oyku.Adapters.Event_Parent_Adapter;
import com.versatilemobitech.oyku.Model.Event_Model;
import com.versatilemobitech.oyku.Model.Event_Parent_Model;
import com.versatilemobitech.oyku.R;
import com.versatilemobitech.oyku.utilities.Utility;

public class Events_Parents_Fragment extends Fragment {

    private Event_Parent_Model event_parent_model;
    private ImageView mimv_back_arrow;;

    public Events_Parents_Fragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_events_parents, container, false);

        Toolbar toolbar = view.findViewById(R.id.toolbar_title);
        TextView tv_title = toolbar.findViewById(R.id.tv_title);

        tv_title.setText("Events");

        SearchView searchView=toolbar.findViewById(R.id.searchView);
        searchView.setQueryHint("Search");
        searchView.setMaxWidth(Integer.MAX_VALUE);
        EditText searchEditText = (EditText) searchView.findViewById(androidx.appcompat.R.id.search_src_text);
        searchEditText.setTextColor(getResources().getColor(R.color.black));
        searchEditText.setHintTextColor(getResources().getColor(R.color.color_dark_grey));
        Utility.setCloseSearchIcon(searchView);


        RecyclerView recyclerView = view.findViewById(R.id.recycler_view_event_parent);
        Event_Parent_Adapter event_parent_adapter = new Event_Parent_Adapter(getContext(), event_parent_model);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(event_parent_adapter);
        mimv_back_arrow = view.findViewById(R.id.imv_back_arrow);
        mimv_back_arrow.setVisibility(View.VISIBLE);

        mimv_back_arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //startActivity(new Intent(getActivity(), Dashboard_ParentsActivity.class));
                getActivity().onBackPressed();
            }
        });

        return view;

    }

}
