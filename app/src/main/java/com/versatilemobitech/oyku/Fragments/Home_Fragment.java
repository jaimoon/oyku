package com.versatilemobitech.oyku.Fragments;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.versatilemobitech.oyku.Activities.LikesActivity;
import com.versatilemobitech.oyku.Adapters.Home_Adapter;
import com.versatilemobitech.oyku.Model.Home_Model;
import com.versatilemobitech.oyku.R;
import com.versatilemobitech.oyku.utilities.SharedPreference;
import com.versatilemobitech.oyku.utilities.Utility;


public class Home_Fragment extends Fragment {

    Home_Model homeModel;



    public Home_Fragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home, container, false);

        SharedPreference.setStringPreference(getActivity(),"HOMEVALUE","1");

        RecyclerView recyclerView = view.findViewById(R.id.recycler_view_home);
        Home_Adapter homeAdapter = new Home_Adapter(getContext(),homeModel);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(homeAdapter);


        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
       // Utility.hideKeyboard(getActivity());
    }
}
