package com.versatilemobitech.oyku.Fragments;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.versatilemobitech.oyku.Activities.DashboardActivity;
import com.versatilemobitech.oyku.Activities.Dashboard_ParentsActivity;
import com.versatilemobitech.oyku.Fragments.notification.NotificationAdpater;
import com.versatilemobitech.oyku.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class Notification_Parents_Fragment extends Fragment {

    TextView tv_tittle;
    ImageView mimv_backarrow,iv_toolbar_menu;

    @BindView(R.id.rcvNotifications)
    RecyclerView rcvNotifications;

    @BindView(R.id.rcvNotifications1)
    RecyclerView rcvNotifications1;

    @BindView(R.id.rcvNotifications2)
    RecyclerView rcvNotifications2;

    public Notification_Parents_Fragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        //View view = inflater.inflate(R.layout.fragment_notification_parents, container, false);

        View view = inflater.inflate(R.layout.fragment_notification, container, false);

        Toolbar toolbar = view.findViewById(R.id.toolbar_title);

        TextView tv_tittle = toolbar.findViewById(R.id.tv_title);
        tv_tittle.setText("Notifications");


        mimv_backarrow = view.findViewById(R.id.imv_back_arrow);
        mimv_backarrow.setVisibility(View.VISIBLE);
        mimv_backarrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //startActivity(new Intent(getActivity(), Dashboard_ParentsActivity.class));
                getActivity().onBackPressed();
            }
        });



        ButterKnife.bind(this,view);
        setUp();
        return view;

    }
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        switch (keyCode) {
            case KeyEvent.KEYCODE_BACK:
                String cameback="CameBack";
                Intent intent = new Intent(getActivity(), DashboardActivity.class);
                intent.putExtra("Comingback", cameback);
                startActivity(intent);
                return true;
        }
        return false;
    }

    private void setUp() {

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity(),LinearLayoutManager.VERTICAL,false);
        rcvNotifications.setLayoutManager(layoutManager);
        rcvNotifications.setAdapter(new NotificationAdpater(getActivity()));

        RecyclerView.LayoutManager layoutManager1 = new LinearLayoutManager(getActivity(),LinearLayoutManager.VERTICAL,false);
        rcvNotifications1.setLayoutManager(layoutManager1);
        rcvNotifications1.setAdapter(new NotificationAdpater(getActivity()));


        RecyclerView.LayoutManager layoutManager2 = new LinearLayoutManager(getActivity(),LinearLayoutManager.VERTICAL,false);
        rcvNotifications2.setLayoutManager(layoutManager2);
        rcvNotifications2.setAdapter(new NotificationAdpater(getActivity()));
    }
}



