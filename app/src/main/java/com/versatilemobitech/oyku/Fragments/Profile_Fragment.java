package com.versatilemobitech.oyku.Fragments;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.versatilemobitech.oyku.Activities.
        ChangePasswordActivity;
import com.versatilemobitech.oyku.Activities.
        DashboardActivity;
import com.versatilemobitech.oyku.R;
import com.versatilemobitech.oyku.utilities.SharedPreference;

public class Profile_Fragment extends Fragment {

    private ImageView mimv_backarrow;
    private TextView tvChangePasswprd;
    private ImageView iv_toolbar_menu;

    public Profile_Fragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_profile, container, false);

        SharedPreference.setStringPreference(getActivity(), "HOMEVALUE", "2");
        ((DashboardActivity) getActivity()).getBottomIconsChange("profile");
        Toolbar toolbar = view.findViewById(R.id.toolbar_title);
        TextView tv_title = toolbar.findViewById(R.id.tv_title);
        tv_title.setText("View Profile");
        SharedPreference.setStringPreference(getActivity(), "HOMEVALUE", "2");

        tvChangePasswprd = view.findViewById(R.id.tvChangePasswprd);
        mimv_backarrow = view.findViewById(R.id.imv_back_arrow);
        iv_toolbar_menu = view.findViewById(R.id.iv_toolbar_menu);
        iv_toolbar_menu.setVisibility(View.INVISIBLE);
        mimv_backarrow.setVisibility(View.GONE);
        toolbar.setNavigationIcon(R.drawable.ic_back_arrow);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });
//        mimv_backarrow.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                getActivity().onBackPressed();
//            }
//        });

        tvChangePasswprd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getContext(), ChangePasswordActivity.class));

            }
        });

        return view;


    }


}
