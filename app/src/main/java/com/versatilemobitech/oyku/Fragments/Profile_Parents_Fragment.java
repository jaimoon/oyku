package com.versatilemobitech.oyku.Fragments;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.versatilemobitech.oyku.Activities.ChangePasswordActivity;
import com.versatilemobitech.oyku.Activities.Dashboard_ParentsActivity;
import com.versatilemobitech.oyku.R;

public class Profile_Parents_Fragment extends Fragment {

    private ImageView mimv_back,iv_toolbar_menu;
    private View mbtn_chnge_pswrd_prt;

    public Profile_Parents_Fragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profile_parents, container, false);

        Toolbar toolbar = view.findViewById(R.id.toolbar_title);
        TextView tv_title = toolbar.findViewById(R.id.tv_title);

        tv_title.setText("View Profile");
        toolbar.findViewById(R.id.searchView).setVisibility(View.GONE);

        mimv_back = view.findViewById(R.id.imv_back_arrow);
        mimv_back.setVisibility(View.VISIBLE);
        mbtn_chnge_pswrd_prt = view.findViewById(R.id.btn_chnge_pswrd_prt);

        mimv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //startActivity(new Intent(getContext(), Dashboard_ParentsActivity.class));
                getActivity().onBackPressed();
            }
        });

        mbtn_chnge_pswrd_prt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getContext(), ChangePasswordActivity.class));

            }
        });

        return view;
    }
}
