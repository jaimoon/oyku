package com.versatilemobitech.oyku.Fragments.mediapicker;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.PixelFormat;
import android.graphics.SurfaceTexture;
import android.hardware.Camera;
import android.hardware.camera2.CameraManager;
import android.opengl.GLSurfaceView;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.jackandphantom.instagramvideobutton.InstagramVideoButton;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.versatilemobitech.oyku.Activities.MediaPickerActivity;
import com.versatilemobitech.oyku.R;
import com.versatilemobitech.oyku.utilities.Constants;
import com.versatilemobitech.oyku.utilities.PermissionChecker;
import com.versatilemobitech.oyku.utilities.ShowCamera;

import java.io.File;
import java.io.FileOutputStream;
import java.util.List;

@SuppressWarnings("ALL")
public class CameraFragment extends Fragment {

    private static final String TAG = CameraFragment.class.getCanonicalName();
    private FrameLayout mFrameLayout;
    private InstagramVideoButton mCapture;
    private Camera mCamera;
    private ShowCamera mShowCamera;
    private Activity mActivity;
    private PermissionChecker mPermissionChecker;
    private AppCompatImageView mSwitchCamera;
    private Camera.Parameters mParams;
    private boolean isPreviewing;

    public CameraFragment() {
    }

    public static CameraFragment newInstance() {
        return new CameraFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "onCreate: ");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.d(TAG, "onCreateView: ");
        mActivity = MediaPickerActivity.mMediaPickerActivityInstance;
        View root = inflater.inflate(R.layout.fragment_camera, container, false);
        initBinding(root);
        return root;
    }

    private void initBinding(View root){
        mFrameLayout = root.findViewById(R.id.cameraLayout);
        mCapture = root.findViewById(R.id.capture);
        mSwitchCamera = root.findViewById(R.id.switchCamera);
        mCapture.setActionListener(mActionListener);
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d(TAG, "onResume: ");
        Dexter.withContext(mActivity).withPermissions(new String[]{Manifest.permission.CAMERA}).withListener(new MultiplePermissionsListener() {
            @Override
            public void onPermissionsChecked(MultiplePermissionsReport multiplePermissionsReport) {
                if (multiplePermissionsReport.areAllPermissionsGranted()){
                    initCamera();
                }
            }

            @Override
            public void onPermissionRationaleShouldBeShown(List<PermissionRequest> list, PermissionToken permissionToken) {

            }
        }).check();
    }

    @Override
    public void onPause() {
        super.onPause();
        mCamera.stopPreview();
    }

    private InstagramVideoButton.ActionListener mActionListener = new InstagramVideoButton.ActionListener() {
        @Override
        public void onStartRecord() {

        }

        @Override
        public void onEndRecord() {

        }

        @Override
        public void onSingleTap() {
            if (mCamera != null) {
                mCamera.takePicture(null, null, mPictureCallback);
            }
        }

        @Override
        public void onDurationTooShortError() {

        }

        @Override
        public void onCancelled() {

        }
    };

    private Camera.PictureCallback mPictureCallback = new Camera.PictureCallback() {
        @Override
        public void onPictureTaken(byte[] data, Camera camera) {
            File pictureFile = getOutputMedia();
            if (pictureFile != null) {
                try {
                    FileOutputStream fileOutputStream = new FileOutputStream(pictureFile);
                    fileOutputStream.write(data);
                    fileOutputStream.close();
                    mCamera.startPreview();
                } catch (Exception e) {
                    Log.e(TAG, "onPictureTaken: " + e);
                }
            }
        }
    };

    private File getOutputMedia() {
        String state = Environment.getExternalStorageState();
        if (!state.equalsIgnoreCase(Environment.MEDIA_MOUNTED)) {
            return null;
        } else {
            File folderGui = new File(Environment.getExternalStorageDirectory() + File.separator + "GUI");
            if (!folderGui.exists()) {
                folderGui.mkdirs();
            }
            File outputFile = new File(folderGui, "temp.jpg");
            return outputFile;
        }
    }

    private void initCamera(){
        mCamera = Camera.open();
        mShowCamera = new ShowCamera(mActivity,mCamera);
        mFrameLayout.addView(mShowCamera);
    }

}
