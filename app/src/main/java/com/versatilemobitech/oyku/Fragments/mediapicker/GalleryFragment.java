package com.versatilemobitech.oyku.Fragments.mediapicker;

import android.Manifest;
import android.app.Activity;
import android.database.Cursor;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Spinner;
import android.widget.Toast;
import android.widget.VideoView;

import androidx.appcompat.widget.AppCompatImageView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.google.android.material.snackbar.Snackbar;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.versatilemobitech.oyku.Adapters.MediaListAdapter;
import com.versatilemobitech.oyku.Adapters.MediaSpinnerAdapter;
import com.versatilemobitech.oyku.Model.MediaDetailsModel;
import com.versatilemobitech.oyku.Model.MediaModel;
import com.versatilemobitech.oyku.R;
import com.versatilemobitech.oyku.interfaces.OnSelectMediaItem;
import com.versatilemobitech.oyku.utilities.Constants;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class GalleryFragment extends Fragment implements OnSelectMediaItem {

    private static final String TAG = GalleryFragment.class.getCanonicalName();
    private ArrayList<MediaModel> mImagesFolderNames;
    private ArrayList<MediaModel> mVideosFolderNames;
    private ArrayList<MediaModel> mAllMedia;
    private Spinner mAlbums;
    private RecyclerView mMediaList;
    private ArrayList<MediaDetailsModel> mImageDetailsList;
    private AppCompatImageView mImagePreview;
    private VideoView mVideoPreview;
    private Activity mActivity;
    private File mPreviewFile;

    public GalleryFragment() {
    }

    public static GalleryFragment newInstance() {
        return new GalleryFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
         Log.d(TAG, "onCreate: ");
        super.onCreate(savedInstanceState);
        mActivity = getActivity();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
          Log.d(TAG, "onCreateView: ");
        View root = inflater.inflate(R.layout.fragment_gallery, container, false);
        mAlbums = root.findViewById(R.id.albums);
        mMediaList = root.findViewById(R.id.mediaList);
        mImagePreview = root.findViewById(R.id.imagePreview);
        mVideoPreview = root.findViewById(R.id.videoPreview);

        Dexter.withContext(mActivity).withPermissions(new String[]{Manifest.permission.CAMERA,Manifest.permission.WRITE_EXTERNAL_STORAGE,Manifest.permission.READ_EXTERNAL_STORAGE}).withListener(new MultiplePermissionsListener() {
            @Override
            public void onPermissionsChecked(MultiplePermissionsReport multiplePermissionsReport) {
                if (multiplePermissionsReport.areAllPermissionsGranted()){
                    init();
                } else {
                    Toast.makeText(mActivity, "Please accept all permissions to show", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onPermissionRationaleShouldBeShown(List<PermissionRequest> list, PermissionToken permissionToken) {

            }
        }).check();

        mAllMedia = new ArrayList<>();
        mImageDetailsList = new ArrayList<>();

        if (mImagesFolderNames != null && mImagesFolderNames.size() > 0) {
            mAllMedia.addAll(mImagesFolderNames);
        }

        if (mVideosFolderNames != null && mVideosFolderNames.size() > 0) {
            mAllMedia.addAll(mVideosFolderNames);
        }

        List<MediaModel> mediaModelList = new ArrayList<>();
        mediaModelList.addAll(mAllMedia);

        MediaSpinnerAdapter customSpinnerAdapter = new MediaSpinnerAdapter(mActivity, R.layout.item_albums, R.id.albumName, mediaModelList);
        mAlbums.setAdapter(customSpinnerAdapter);

        mAlbums.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                MediaModel mediaModel = mAllMedia.get(position);
                setMediaRecyclerList(mediaModel);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                MediaModel mediaModel = mAllMedia.get(0);
                setMediaRecyclerList(mediaModel);
            }
        });

        return root;
    }

    @Override
    public void onResume() {
          Log.d(TAG, "onResume: ");
        super.onResume();
    }

    private void init() {
        mImagesFolderNames = getPicturePaths();
        mVideosFolderNames = getAllVideoPath();
    }

    private ArrayList<MediaModel> getPicturePaths() {
        ArrayList<MediaModel> picFolders = new ArrayList<>();
        ArrayList<String> picPaths = new ArrayList<>();
        Uri allImagesuri = android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
        String[] projection = {MediaStore.Images.ImageColumns.DATA, MediaStore.Images.Media.DISPLAY_NAME,
                MediaStore.Images.Media.BUCKET_DISPLAY_NAME, MediaStore.Images.Media.BUCKET_ID};
        Cursor cursor = mActivity.getContentResolver().query(allImagesuri, projection, null, null, null);
        try {
            if (cursor != null) {
                cursor.moveToFirst();
            }
            do {
                MediaModel mediaModel = new MediaModel();
                String folder = cursor.getString(cursor.getColumnIndexOrThrow(MediaStore.Images.Media.BUCKET_DISPLAY_NAME));
                String datapath = cursor.getString(cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA));
                String folderpaths = datapath.substring(0, datapath.lastIndexOf(folder + "/"));
                folderpaths = folderpaths + folder + "/";
                if (!picPaths.contains(folderpaths)) {
                    picPaths.add(folderpaths);
                    mediaModel.setPath(folderpaths);
                    mediaModel.setFolderName(folder);
                    mediaModel.setType(Constants.IMAGE);
                    picFolders.add(mediaModel);
                }
            } while (cursor.moveToNext());
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return picFolders;
    }

    private ArrayList<MediaModel> getAllVideoPath() {
        ArrayList<MediaModel> vidFolders = new ArrayList<>();
        ArrayList<String> vidPaths = new ArrayList<>();
        Uri allImagesuri = android.provider.MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
        String[] projection = {MediaStore.Video.VideoColumns.DATA, MediaStore.Video.Media.DISPLAY_NAME,
                MediaStore.Video.Media.BUCKET_DISPLAY_NAME, MediaStore.Video.Media.BUCKET_ID};
        Cursor cursor = mActivity.getContentResolver().query(allImagesuri, projection, null, null, null);
        try {
            if (cursor != null) {
                cursor.moveToFirst();
            }
            do {
                MediaModel mediaModel = new MediaModel();
                String folder = cursor.getString(cursor.getColumnIndexOrThrow(MediaStore.Video.Media.BUCKET_DISPLAY_NAME));
                String datapath = cursor.getString(cursor.getColumnIndexOrThrow(MediaStore.Video.Media.DATA));
                String folderpaths = datapath.substring(0, datapath.lastIndexOf(folder + "/"));
                folderpaths = folderpaths + folder + "/";
                if (!vidPaths.contains(folderpaths)) {
                    vidPaths.add(folderpaths);
                    mediaModel.setPath(folderpaths);
                    mediaModel.setFolderName(folder);
                    mediaModel.setType(Constants.VIDEO);
                    vidFolders.add(mediaModel);
                }
            } while (cursor.moveToNext());
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return vidFolders;
    }

    public ArrayList<MediaDetailsModel> getAllImagesByFolder(String path) {
        ArrayList<MediaDetailsModel> images = new ArrayList<>();
        Uri allVideosuri = android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
        String[] projection = {MediaStore.Images.ImageColumns.DATA, MediaStore.Images.Media.DISPLAY_NAME,
                MediaStore.Images.Media.SIZE};
        Cursor cursor = mActivity.getContentResolver().query(allVideosuri, projection, MediaStore.Images.Media.DATA + " like ? ", new String[]{"%" + path + "%"}, null);
        try {
            cursor.moveToFirst();
            do {
                MediaDetailsModel mediaDetailsModel = new MediaDetailsModel();
                mediaDetailsModel.setName(cursor.getString(cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DISPLAY_NAME)));
                mediaDetailsModel.setPath(cursor.getString(cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA)));
                mediaDetailsModel.setSize(cursor.getString(cursor.getColumnIndexOrThrow(MediaStore.Images.Media.SIZE)));
                images.add(mediaDetailsModel);
            } while (cursor.moveToNext());
            cursor.close();
            ArrayList<MediaDetailsModel> reSelection = new ArrayList<>();
            for (int i = images.size() - 1; i > -1; i--) {
                reSelection.add(images.get(i));
            }
            images = reSelection;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return images;
    }

    public ArrayList<MediaDetailsModel> getAllVideosByFolder(String path) {
        ArrayList<MediaDetailsModel> images = new ArrayList<>();
        Uri allVideosuri = android.provider.MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
        String[] projection = {MediaStore.Video.VideoColumns.DATA, MediaStore.Video.Media.DISPLAY_NAME,
                MediaStore.Video.Media.SIZE};
        Cursor cursor = mActivity.getContentResolver().query(allVideosuri, projection, MediaStore.Images.Media.DATA + " like ? ", new String[]{"%" + path + "%"}, null);
        try {
            cursor.moveToFirst();
            do {
                MediaDetailsModel mediaDetailsModel = new MediaDetailsModel();
                mediaDetailsModel.setName(cursor.getString(cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DISPLAY_NAME)));
                mediaDetailsModel.setPath(cursor.getString(cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA)));
                mediaDetailsModel.setSize(cursor.getString(cursor.getColumnIndexOrThrow(MediaStore.Images.Media.SIZE)));
                images.add(mediaDetailsModel);
            } while (cursor.moveToNext());
            cursor.close();
            ArrayList<MediaDetailsModel> reSelection = new ArrayList<>();
            for (int i = images.size() - 1; i > -1; i--) {
                reSelection.add(images.get(i));
            }
            images = reSelection;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return images;
    }


    @Override
    public void selectedItem(MediaDetailsModel mediaDetailsModel, String mediaType) {
        loadPreview(mediaDetailsModel,mediaType);
    }

    private void loadPreview(MediaDetailsModel mediaDetailsModel, String mediaType){
        switch (mediaType){
            case Constants.IMAGE:
                mImagePreview.setVisibility(View.VISIBLE);
                mVideoPreview.setVisibility(View.GONE);
                Glide.with(mActivity).load(new File(mediaDetailsModel.getPath())).error(R.drawable.ic_error).into(mImagePreview);
                break;
            case Constants.VIDEO:
                mImagePreview.setVisibility(View.GONE);
                mVideoPreview.setVisibility(View.VISIBLE);
                mVideoPreview.setVideoPath(mediaDetailsModel.getPath());
                mVideoPreview.setOnPreparedListener(mPreparedListener);
                mVideoPreview.start();
                break;
        }
    }

    private void setMediaRecyclerList(MediaModel mediaModel){
        mImageDetailsList.clear();
        if (mediaModel.getType().equalsIgnoreCase(Constants.IMAGE)){
            mImageDetailsList = getAllImagesByFolder(mediaModel.getPath());
            MediaListAdapter mediaListAdapter = new MediaListAdapter(mActivity,this,mImageDetailsList,Constants.IMAGE);
            mMediaList.setLayoutManager(new GridLayoutManager(mActivity, 3));
            mMediaList.setAdapter(mediaListAdapter);
            loadPreview(mImageDetailsList.get(0),Constants.IMAGE);
        } else {
            mImageDetailsList = getAllVideosByFolder(mediaModel.getPath());
            MediaListAdapter mediaListAdapter = new MediaListAdapter(mActivity,this,mImageDetailsList,Constants.VIDEO);
            mMediaList.setLayoutManager(new GridLayoutManager(mActivity, 3));
            mMediaList.setAdapter(mediaListAdapter);
            loadPreview(mImageDetailsList.get(0),Constants.VIDEO);
        }
    }

    MediaPlayer.OnPreparedListener mPreparedListener = m -> {
        try {
            if (m.isPlaying()) {
                m.stop();
                m.release();
                m = new MediaPlayer();
            }
            m.setVolume(0f, 0f);
            m.setLooping(false);
            m.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    };
}
