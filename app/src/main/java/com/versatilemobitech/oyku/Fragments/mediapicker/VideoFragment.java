package com.versatilemobitech.oyku.Fragments.mediapicker;

import android.app.Activity;
import android.hardware.camera2.CameraCharacteristics;
import android.opengl.GLSurfaceView;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.Toast;

import androidx.appcompat.widget.AppCompatImageView;
import androidx.fragment.app.Fragment;

import com.daasuu.camerarecorder.CameraRecorder;
import com.daasuu.camerarecorder.CameraRecorderBuilder;
import com.daasuu.camerarecorder.LensFacing;
import com.daasuu.camerarecorder.egl.filter.GlFilter;
import com.jackandphantom.instagramvideobutton.InstagramVideoButton;
import com.versatilemobitech.oyku.R;
import com.versatilemobitech.oyku.utilities.Constants;

import java.io.File;

public class VideoFragment extends Fragment {

    private static final String TAG = VideoFragment.class.getCanonicalName();
    private FrameLayout mFrameLayout;
    private InstagramVideoButton mInstagramVideoButton;
    private AppCompatImageView mSwitchCamera;
    private GLSurfaceView mGlSurfaceView;
    private CameraRecorder mCameraRecorder;
    private CameraRecorderBuilder mCameraRecorderBuilder;
    private Activity mActivity;
    private boolean isCameraFacingBack;

    public VideoFragment() {
        // Required empty public constructor
    }

    /**
     * @return A new instance of fragment SpeedDialFragment.
     */
    public static VideoFragment newInstance() {
        return new VideoFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
         Log.d(TAG, "onCreate: ");
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
          Log.d(TAG, "onCreateView: ");
        View root = inflater.inflate(R.layout.fragment_video, container, false);
        mFrameLayout = root.findViewById(R.id.videoPreview);
        mInstagramVideoButton = root.findViewById(R.id.recordVideo);
        mSwitchCamera = root.findViewById(R.id.switchCamera);
        mActivity = getActivity();
        mSwitchCamera.setOnClickListener(mSwitchCameraClicked);
        return root;
    }

    @Override
    public void onResume() {
          Log.d(TAG, "onResume: ");
        super.onResume();
        attachCamera(Constants.CAMERA_FACING_BACK);
        mInstagramVideoButton.enableVideoRecording(true);
        mInstagramVideoButton.setActionListener(mActionListener);
    }

    private void attachCamera(String cameraFacing){
        switch (cameraFacing){
            case Constants.CAMERA_FACING_BACK:
                mGlSurfaceView = new GLSurfaceView(mActivity);
                mFrameLayout.addView(mGlSurfaceView);
                mCameraRecorderBuilder = new CameraRecorderBuilder(mActivity, mGlSurfaceView).lensFacing(LensFacing.BACK);
                mCameraRecorder = mCameraRecorderBuilder.build();
                isCameraFacingBack = true;
                break;
            case Constants.CAMERA_FACING_FRONT:
                mGlSurfaceView = new GLSurfaceView(mActivity);
                mFrameLayout.addView(mGlSurfaceView);
                mCameraRecorderBuilder = new CameraRecorderBuilder(mActivity, mGlSurfaceView).lensFacing(LensFacing.FRONT);
                mCameraRecorder = mCameraRecorderBuilder.build();
                isCameraFacingBack = false;
                break;
        }
    }

    private View.OnClickListener mSwitchCameraClicked = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (isCameraFacingBack){
                mFrameLayout.removeAllViews();
                attachCamera(Constants.CAMERA_FACING_FRONT);
            } else {
                mFrameLayout.removeAllViews();
                attachCamera(Constants.CAMERA_FACING_BACK);
            }
        }
    };

    private InstagramVideoButton.ActionListener mActionListener = new InstagramVideoButton.ActionListener() {
        @Override
        public void onStartRecord() {
            recordVideo();
        }

        @Override
        public void onEndRecord() {
            stopRecord();
        }

        @Override
        public void onSingleTap() {

        }

        @Override
        public void onDurationTooShortError() {

        }

        @Override
        public void onCancelled() {

        }
    };

    private void recordVideo() {
        if (!mCameraRecorder.isStarted()) {
            Toast.makeText(mActivity, "Recording", Toast.LENGTH_SHORT).show();
            mCameraRecorder.start(getOutputMedia().getAbsolutePath());
        } else {
            Toast.makeText(mActivity, "Already Recording", Toast.LENGTH_SHORT).show();
        }
    }

    private void stopRecord() {
        if (mCameraRecorder.isStarted()) {
            mCameraRecorder.stop();
            File file = getOutputMedia();
            if (file != null) {
                Log.d(TAG, "onClick: " + file.getAbsolutePath());
            }
            Toast.makeText(mActivity, "Stopped", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(mActivity, "Video recording not started", Toast.LENGTH_SHORT).show();
        }
    }

    private File getOutputMedia() {
        String state = Environment.getExternalStorageState();
        if (!state.equalsIgnoreCase(Environment.MEDIA_MOUNTED)) {
            return null;
        } else {
            File folderGui = new File(Environment.getExternalStorageDirectory() + File.separator + "GUI");
            if (!folderGui.exists()) {
                folderGui.mkdirs();
            }
            File outputFile = new File(folderGui, "temp.mp4");
            return outputFile;
        }
    }
}
