package com.versatilemobitech.oyku.Fragments.notification;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.versatilemobitech.oyku.R;

import butterknife.ButterKnife;

public class NotificationAdpater extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    Context context;

    public NotificationAdpater(Context context) {
        this.context = context;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
       int layout = R.layout.notification_design;
       return new NotificationAdpater.NotificationViewHolder(LayoutInflater.from(parent.getContext()).inflate(layout,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        if (holder instanceof NotificationViewHolder)
        {
            NotificationAdpater.NotificationViewHolder notificationViewHolder = (NotificationViewHolder)holder;
            notificationViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Toast.makeText(context, "Work in progress", Toast.LENGTH_SHORT).show();

                }
            });
        }

    }

    @Override
    public int getItemCount() {
        return 4;
    }
    public class NotificationViewHolder extends RecyclerView.ViewHolder {
        public NotificationViewHolder(@NonNull View itemView) {
            super(itemView);

            ButterKnife.bind(this,itemView);

        }
    }

}
