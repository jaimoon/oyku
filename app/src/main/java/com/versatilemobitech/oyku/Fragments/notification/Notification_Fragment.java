package com.versatilemobitech.oyku.Fragments.notification;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.versatilemobitech.oyku.Activities.DashboardActivity;
import com.versatilemobitech.oyku.R;
import com.versatilemobitech.oyku.utilities.SharedPreference;

import butterknife.BindView;
import butterknife.ButterKnife;

public class Notification_Fragment extends Fragment {

    ImageView mimv_backarrow,iv_toolbar_menu;

    @BindView(R.id.rcvNotifications)
    RecyclerView rcvNotifications;

    @BindView(R.id.rcvNotifications1)
    RecyclerView rcvNotifications1;

    @BindView(R.id.rcvNotifications2)
    RecyclerView rcvNotifications2;


    public Notification_Fragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_notification, container, false);
        SharedPreference.setStringPreference(getActivity(),"HOMEVALUE","2");


        ((DashboardActivity)getActivity()).getBottomIconsChange("notification");


        mimv_backarrow = view.findViewById(R.id.imv_back_arrow);
        mimv_backarrow.setVisibility(View.GONE);
        Toolbar toolbar = view.findViewById(R.id.toolbar_title);
        TextView tv_title = toolbar.findViewById(R.id.tv_title);
        tv_title.setText("Notifications");
        toolbar.setNavigationIcon(R.drawable.ic_back_arrow);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });

        ButterKnife.bind(this,view);
        setUp();
        iv_toolbar_menu = view.findViewById(R.id.iv_toolbar_menu);
        iv_toolbar_menu.setVisibility(View.INVISIBLE);

        return view;
    }

    private void setUp() {

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity(),LinearLayoutManager.VERTICAL,false);
        rcvNotifications.setLayoutManager(layoutManager);
        rcvNotifications.setAdapter(new NotificationAdpater(getActivity()));

        RecyclerView.LayoutManager layoutManager1 = new LinearLayoutManager(getActivity(),LinearLayoutManager.VERTICAL,false);
        rcvNotifications1.setLayoutManager(layoutManager1);
        rcvNotifications1.setAdapter(new NotificationAdpater(getActivity()));


        RecyclerView.LayoutManager layoutManager2 = new LinearLayoutManager(getActivity(),LinearLayoutManager.VERTICAL,false);
        rcvNotifications2.setLayoutManager(layoutManager2);
        rcvNotifications2.setAdapter(new NotificationAdpater(getActivity()));
    }


}
