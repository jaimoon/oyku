package com.versatilemobitech.oyku.Model;

import android.widget.ImageView;

public class CreateEvent_Model {

    private String parent;
    private String section;
    private ImageView person;
    private boolean button;

    public CreateEvent_Model(String parent, String section, ImageView person, boolean button) {
        this.parent = parent;
        this.section = section;
        this.person = person;
        this.button = button;
    }

    public String getParent() {
        return parent;
    }

    public void setParent(String parent) {
        this.parent = parent;
    }

    public String getSection() {
        return section;
    }

    public void setSection(String section) {
        this.section = section;
    }

    public ImageView getPerson() {
        return person;
    }

    public void setPerson(ImageView person) {
        this.person = person;
    }

    public boolean isButton() {
        return button;
    }

    public void setButton(boolean button) {
        this.button = button;
    }
}
