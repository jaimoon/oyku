package com.versatilemobitech.oyku.Model;

import android.widget.ImageView;

import de.hdodenhof.circleimageview.CircleImageView;

public class CreateGroup_Model {

    private String title, sub_title;
    private ImageView cb_checked, cb_unchecked;
    private CircleImageView circleImageView;

    public CreateGroup_Model(String title, String sub_title, ImageView cb_checked, ImageView cb_unchecked, CircleImageView circleImageView) {
        this.title = title;
        this.sub_title = sub_title;
        this.cb_checked = cb_checked;
        this.cb_unchecked = cb_unchecked;
        this.circleImageView = circleImageView;
    }

    public String getSub_title() {
        return sub_title;
    }

    public void setSub_title(String sub_title) {
        this.sub_title = sub_title;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public ImageView getCb_checked() {
        return cb_checked;
    }

    public void setCb_checked(ImageView cb_checked) {
        this.cb_checked = cb_checked;
    }

    public ImageView getCb_unchecked() {
        return cb_unchecked;
    }

    public void setCb_unchecked(ImageView cb_unchecked) {
        this.cb_unchecked = cb_unchecked;
    }

    public CircleImageView getCircleImageView() {
        return circleImageView;
    }

    public void setCircleImageView(CircleImageView circleImageView) {
        this.circleImageView = circleImageView;
    }
}
