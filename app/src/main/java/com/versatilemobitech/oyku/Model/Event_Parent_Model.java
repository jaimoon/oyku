package com.versatilemobitech.oyku.Model;

import android.widget.ImageView;

public class Event_Parent_Model {
    private String  annualday,independenceday;
    private String datetime;
    private String text;
    private ImageView image;

    public Event_Parent_Model(String annualday, String independenceday, String datetime, String text, ImageView image) {
        this.annualday = annualday;
        this.independenceday = independenceday;
        this.datetime = datetime;
        this.text = text;
        this.image = image;
    }

    public String getAnnualday() {
        return annualday;
    }

    public void setAnnualday(String annualday) {
        this.annualday = annualday;
    }

    public String getDatetime() {
        return datetime;
    }

    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public ImageView getImage() {
        return image;
    }

    public void setImage(ImageView image) {
        this.image = image;
    }

    public String getIndependenceday() {
        return independenceday;
    }

    public void setIndependenceday(String independenceday) {
        this.independenceday = independenceday;
    }
}
