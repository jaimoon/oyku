package com.versatilemobitech.oyku.Model;

import android.widget.ImageView;

public class Home_Model {

    private String posted_day;
    private String name;
    private String subject;
    private ImageView imv_profile;
    private ImageView imv_activity;
    private ImageView imv_likes;
    private ImageView imv_comments;
    private String likes;
    private String comments;
    private String memories;

    public Home_Model(String posted_day, String name, String subject, ImageView imv_profile, ImageView imv_activity, ImageView imv_likes, ImageView imv_comments, String likes, String comments, String memories) {
        this.posted_day = posted_day;
        this.name = name;
        this.subject = subject;
        this.imv_profile = imv_profile;
        this.imv_activity = imv_activity;
        this.imv_likes = imv_likes;
        this.imv_comments = imv_comments;
        this.likes = likes;
        this.comments = comments;
        this.memories = memories;
    }

    public String getPosted_day() {
        return posted_day;
    }

    public void setPosted_day(String posted_day) {
        this.posted_day = posted_day;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public ImageView getImv_profile() {
        return imv_profile;
    }

    public void setImv_profile(ImageView imv_profile) {
        this.imv_profile = imv_profile;
    }

    public ImageView getImv_activity() {
        return imv_activity;
    }

    public void setImv_activity(ImageView imv_activity) {
        this.imv_activity = imv_activity;
    }

    public ImageView getImv_likes() {
        return imv_likes;
    }

    public void setImv_likes(ImageView imv_likes) {
        this.imv_likes = imv_likes;
    }

    public ImageView getImv_comments() {
        return imv_comments;
    }

    public void setImv_comments(ImageView imv_comments) {
        this.imv_comments = imv_comments;
    }

    public String getLikes() {
        return likes;
    }

    public void setLikes(String likes) {
        this.likes = likes;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getMemories() {
        return memories;
    }

    public void setMemories(String memories) {
        this.memories = memories;
    }

}
