package com.versatilemobitech.oyku.Model;

import de.hdodenhof.circleimageview.CircleImageView;

public class InviteStudent_Model {

    private String stu_name,stu_fat_name,invite;
    private CircleImageView circleImageView;

    public InviteStudent_Model(String stu_name, String stu_fat_name, String invite, CircleImageView circleImageView) {
        this.stu_name = stu_name;
        this.stu_fat_name = stu_fat_name;
        this.invite = invite;
        this.circleImageView = circleImageView;
    }

    public String getStu_name() {
        return stu_name;
    }

    public void setStu_name(String stu_name) {
        this.stu_name = stu_name;
    }

    public String getStu_fat_name() {
        return stu_fat_name;
    }

    public void setStu_fat_name(String stu_fat_name) {
        this.stu_fat_name = stu_fat_name;
    }

    public String getInvite() {
        return invite;
    }

    public void setInvite(String invite) {
        this.invite = invite;
    }

    public CircleImageView getCircleImageView() {
        return circleImageView;
    }

    public void setCircleImageView(CircleImageView circleImageView) {
        this.circleImageView = circleImageView;
    }
}
