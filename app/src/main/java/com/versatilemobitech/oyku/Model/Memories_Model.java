package com.versatilemobitech.oyku.Model;

import android.widget.ImageView;

import de.hdodenhof.circleimageview.CircleImageView;

public class Memories_Model {

    private String name,subject,postedday,eventname,timedate,text;
    private ImageView imageView1,imageView2;
    private CircleImageView circleImageView;

    public Memories_Model(String name, String subject, String postedday, String eventname, String timedate, String text, ImageView imageView1, ImageView imageView2, CircleImageView circleImageView) {
        this.name = name;
        this.subject = subject;
        this.postedday = postedday;
        this.eventname = eventname;
        this.timedate = timedate;
        this.text = text;
        this.imageView1 = imageView1;
        this.imageView2 = imageView2;
        this.circleImageView = circleImageView;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getPostedday() {
        return postedday;
    }

    public void setPostedday(String postedday) {
        this.postedday = postedday;
    }

    public String getEventname() {
        return eventname;
    }

    public void setEventname(String eventname) {
        this.eventname = eventname;
    }

    public String getTimedate() {
        return timedate;
    }

    public void setTimedate(String timedate) {
        this.timedate = timedate;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public ImageView getImageView1() {
        return imageView1;
    }

    public void setImageView1(ImageView imageView1) {
        this.imageView1 = imageView1;
    }

    public ImageView getImageView2() {
        return imageView2;
    }

    public void setImageView2(ImageView imageView2) {
        this.imageView2 = imageView2;
    }

    public CircleImageView getCircleImageView() {
        return circleImageView;
    }

    public void setCircleImageView(CircleImageView circleImageView) {
        this.circleImageView = circleImageView;
    }
}
