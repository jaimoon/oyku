package com.versatilemobitech.oyku.Model;

import android.widget.ImageView;

public class Student_Model {

    private String stu_name;
    private String stu_father_name;
    private ImageView imageView;
    private ImageView imv_img;

    public Student_Model(String stu_name, String stu_father_name, ImageView imageView, ImageView imv_img) {
        this.stu_name = stu_name;
        this.stu_father_name = stu_father_name;
        this.imageView = imageView;
        this.imv_img = imv_img;
    }

    public String getStu_name() {
        return stu_name;
    }

    public void setStu_name(String stu_name) {
        this.stu_name = stu_name;
    }

    public String getStu_father_name() {
        return stu_father_name;
    }

    public void setStu_father_name(String stu_father_name) {
        this.stu_father_name = stu_father_name;
    }

    public ImageView getImageView() {
        return imageView;
    }

    public void setImageView(ImageView imageView) {
        this.imageView = imageView;
    }

    public ImageView getImv_img() {
        return imv_img;
    }

    public void setImv_img(ImageView imv_img) {
        this.imv_img = imv_img;
    }
}
