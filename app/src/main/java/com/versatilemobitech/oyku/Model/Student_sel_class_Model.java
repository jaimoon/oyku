package com.versatilemobitech.oyku.Model;

import de.hdodenhof.circleimageview.CircleImageView;

public class Student_sel_class_Model {

    private String name;
    private String father_name;
    private CircleImageView circleImageView;

    public Student_sel_class_Model(String name, String father_name, CircleImageView circleImageView) {
        this.name = name;
        this.father_name = father_name;
        this.circleImageView = circleImageView;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFather_name() {
        return father_name;
    }

    public void setFather_name(String father_name) {
        this.father_name = father_name;
    }

    public CircleImageView getCircleImageView() {
        return circleImageView;
    }

    public void setCircleImageView(CircleImageView circleImageView) {
        this.circleImageView = circleImageView;
    }
}
