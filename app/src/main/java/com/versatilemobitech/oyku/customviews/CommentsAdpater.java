package com.versatilemobitech.oyku.customviews;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.versatilemobitech.oyku.R;

import butterknife.ButterKnife;

public class CommentsAdpater extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    Context context;

    public CommentsAdpater(Context context) {
        this.context = context;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
       int layout = R.layout.commants_item_design;
       return new CommentsAdpater.NotificationViewHolder(LayoutInflater.from(parent.getContext()).inflate(layout,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        if (holder instanceof NotificationViewHolder)
        {
            CommentsAdpater.NotificationViewHolder notificationViewHolder = (NotificationViewHolder)holder;
            notificationViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Toast.makeText(context, "Work in progress", Toast.LENGTH_SHORT).show();

                }
            });
        }

    }

    @Override
    public int getItemCount() {
        return 4;
    }
    public class NotificationViewHolder extends RecyclerView.ViewHolder {
        public NotificationViewHolder(@NonNull View itemView) {
            super(itemView);

            ButterKnife.bind(this,itemView);

        }
    }

}
