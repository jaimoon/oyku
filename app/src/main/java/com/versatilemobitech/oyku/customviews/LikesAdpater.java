package com.versatilemobitech.oyku.customviews;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.versatilemobitech.oyku.R;

import butterknife.ButterKnife;

public class LikesAdpater extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    Context context;

    public LikesAdpater(Context context) {
        this.context = context;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
       int layout = R.layout.likes_item_design;
       return new LikesAdpater.NotificationViewHolder(LayoutInflater.from(parent.getContext()).inflate(layout,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        if (holder instanceof NotificationViewHolder)
        {
            LikesAdpater.NotificationViewHolder notificationViewHolder = (NotificationViewHolder)holder;
            notificationViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {



                }
            });
        }

    }

    @Override
    public int getItemCount() {
        return 14;
    }
    public class NotificationViewHolder extends RecyclerView.ViewHolder {
        public NotificationViewHolder(@NonNull View itemView) {
            super(itemView);

            ButterKnife.bind(this,itemView);

        }
    }

}
