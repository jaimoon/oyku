package com.versatilemobitech.oyku.customviews.edittexts;

import android.content.Context;
import android.util.AttributeSet;

import androidx.appcompat.widget.AppCompatEditText;

import com.versatilemobitech.oyku.utilities.Utility;

/**
 * Created by Nagabhushan on 11-07-2018.
 **/

public class RBMediumEditText extends AppCompatEditText {
    public RBMediumEditText(Context context) {
        super(context);
        init();
    }

    public RBMediumEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public RBMediumEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }
    private void init() {
        setTypeface(Utility.getTypeface(6,getContext()));
    }

}