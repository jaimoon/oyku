package com.versatilemobitech.oyku.customviews.edittexts;

import android.content.Context;
import android.util.AttributeSet;

import androidx.appcompat.widget.AppCompatEditText;

import com.versatilemobitech.oyku.utilities.Utility;

/**
 * Created by Nagabhushan on 11-05-2020.
 **/

public class RBRegularEditText extends AppCompatEditText {
    public RBRegularEditText(Context context) {
        super(context);
        init();
    }

    public RBRegularEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public RBRegularEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }
    private void init() {
        setTypeface(Utility.getTypeface(5,getContext()));
    }

}