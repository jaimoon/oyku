package com.versatilemobitech.oyku.customviews.textviews;

import android.content.Context;
import android.util.AttributeSet;

import androidx.appcompat.widget.AppCompatEditText;

import com.versatilemobitech.oyku.utilities.Utility;

/**
 * Created by Nagabhushan on 11-07-2018.
 **/

public class OSLightTextView extends AppCompatEditText
{
    public OSLightTextView(Context context) {
        super(context);
        init();
    }

    public OSLightTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public OSLightTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }
    private void init() {

        setTypeface(Utility.getTypeface(0,getContext()));
    }

}