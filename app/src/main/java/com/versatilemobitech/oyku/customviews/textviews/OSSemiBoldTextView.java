package com.versatilemobitech.oyku.customviews.textviews;

import android.content.Context;
import android.util.AttributeSet;

import androidx.appcompat.widget.AppCompatEditText;

import com.versatilemobitech.oyku.utilities.Utility;

/**
 * Created by Nagabhushan on 11-05-2020.
 **/

public class OSSemiBoldTextView extends AppCompatEditText
{
    public OSSemiBoldTextView(Context context) {
        super(context);
        init();
    }

    public OSSemiBoldTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public OSSemiBoldTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }
    private void init() {
        setTypeface(Utility.getTypeface(2,getContext()));
    }

}