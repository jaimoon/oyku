package com.versatilemobitech.oyku.interfaces;

import android.app.Activity;

public interface EActivity {
    public void changeTitle(String title);
    public void showSnackBar(String snackBarText, int type);
    public Activity activity();
}
