package com.versatilemobitech.oyku.interfaces;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public interface EFragment {

    View onCreateView(LayoutInflater inflater, ViewGroup container,
                      Bundle savedInstanceState);

    public void changeTitle(String title);
    public void showSnackBar(String snackBarText, int type);
    public Activity activity();

}
