package com.versatilemobitech.oyku.interfaces;

import com.versatilemobitech.oyku.Model.MediaDetailsModel;

public interface OnSelectMediaItem {
    void selectedItem(MediaDetailsModel mediaDetailsModel, String mediaType);
}
