package com.versatilemobitech.oyku.interfaces;

public interface SelectAllItems {
    void onSelectAll(boolean value);
}
