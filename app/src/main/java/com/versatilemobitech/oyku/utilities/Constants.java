package com.versatilemobitech.oyku.utilities;

public class Constants {
    public static final String IS_NEW = "IsNew";
    public static final String VIDEO = "Video";
    public static final String IMAGE = "Image";
    public static final int CAMERA_PERMISSION_CODE = 001;
    public static final int READ_STORAGE_PERMISSION_CODE = 002;
    public static final int WRITE_STORAGE_PERMISSION_CODE = 003;
    public static final int RECORD_AUDIO_PERMISSION_CODE = 004;
    public static final int MEDIA_PICKER_REQUEST_CODE = 005;
    public static final String CAMERA_FACING_BACK = "CameraFacingBack";
    public static final String CAMERA_FACING_FRONT = "CameraFacingFront";
}
