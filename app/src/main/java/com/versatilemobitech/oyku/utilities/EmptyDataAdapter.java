package com.versatilemobitech.oyku.utilities;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.versatilemobitech.oyku.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class EmptyDataAdapter extends RecyclerView.Adapter<EmptyDataAdapter.EmptyViewHolder>
{

    Context context;
    String content;
    int imageId;
    int type;

    public EmptyDataAdapter(Context context, String content, int imageId, int type)
    {
        this.context = context;
        this.content = content;
        this.imageId = imageId;
        this.type = type;
    }

    @Override
    public EmptyViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        return new EmptyViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.empty_data_item,parent,false));
    }

    @Override
    public void onBindViewHolder(EmptyViewHolder holder, int position)
    {
        holder.tvNote.setVisibility(type == 0 ? View.VISIBLE  : View.GONE);
        holder.tvError.setText(content);
        holder.imgError.setImageResource(imageId);

    }

    @Override
    public int getItemCount()
    {
        return 1;
    }

    public class EmptyViewHolder extends RecyclerView.ViewHolder
    {
        @BindView(R.id.llEmpty)
        LinearLayout llEmpty;

        @BindView(R.id.tvError)
        TextView tvError;

        @BindView(R.id.imgError)
        ImageView imgError;

        @BindView(R.id.tvNote)
        TextView tvNote;

        public EmptyViewHolder(View itemView)
        {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }
}
