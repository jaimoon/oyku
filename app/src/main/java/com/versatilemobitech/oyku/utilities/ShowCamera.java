package com.versatilemobitech.oyku.utilities;

import android.content.Context;
import android.content.res.Configuration;
import android.hardware.Camera;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import java.util.List;

@SuppressWarnings("ALL")
public class ShowCamera extends SurfaceView implements SurfaceHolder.Callback {

    public static final String TAG = ShowCamera.class.getCanonicalName();
    private Camera.Size mSize = null;
    private Camera mCamera;
    private SurfaceHolder mSurfaceHolder;

    public ShowCamera(Context context, Camera camera) {
        super(context);
        mCamera = camera;
        mSurfaceHolder = getHolder();
        mSurfaceHolder.addCallback(this);
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        Camera.Parameters parameters = mCamera.getParameters();
        List<Camera.Size> sizeList = parameters.getSupportedPictureSizes();
        for (Camera.Size size : sizeList){
            mSize = size;
        }
        parameters.setPictureSize(mSize.width, mSize.height);
        if (this.getResources().getConfiguration().orientation != Configuration.ORIENTATION_LANDSCAPE){
            parameters.set("orientation","potrait");
            mCamera.setDisplayOrientation(90);
            parameters.setRotation(90);
        } else {
            parameters.set("orientation","landscape");
            mCamera.setDisplayOrientation(0);
            parameters.setRotation(0);
        }

        mCamera.setParameters(parameters);
        try {
            mCamera.setPreviewDisplay(holder);
            mCamera.startPreview();
        }catch (Exception e){
            Log.e(TAG, "surfaceCreated: "+e );
        }
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {

    }
}
